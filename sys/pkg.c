/*
 * if.c
 *
 *  Created on: Aug 7, 2019
 *      Author: vchumakov
 */

#include "hal/hw_defs.h"

#include "device.h"

#include "bsp.h"
#include "hw_rtc.h"
#include "hw_gpio.h"
#include "eefs.h"
#include "sys/validators.h"
#include "calc.h"
#include "sys/syslog.h"
#include "hal/hw_crc.h"
#include "hal/hw_wdt.h"
#include "hal/hw_tim.h"
#include "events.h"
#include "def/default.h"

const char dev_sw_id[] = "redpower v0.01 (c) netcat";

void pkg_config_handler(if_msg_t *rcv, handler_stage_e stage);
void pkg_acc_handler(if_msg_t *rcv, handler_stage_e stage);
void pkg_tarif_handler(if_msg_t *rcv, handler_stage_e stage);
void pkg_limits_handler(if_msg_t *rcv, handler_stage_e stage);

// supported cmd objects
const char *objs_names[] = { IF_CMD_STRS };
const if_obj_t objs[] = {
  { CMD_SESSION,          { OBJ_RW, OBJ_RW }, DEVICE_PWD_LEN + DEVICE_SESSION_TIME_LEN },
  { CMD_SW_ID,            { OBJ_RD, OBJ_RD }, sizeof(dev_sw_id),       (uint8_t *)dev_sw_id },
  { CMD_CONFIG,           { OBJ_RW, OBJ_RD }, sizeof(device_config_t), (uint8_t *)&db.config, PT_CONFIG, validate_config, pkg_config_handler },
  { CMD_RESET,            { OBJ_WR, OBJ_NO }, sizeof(uint32_t) },
  { CMD_CLOCK,            { OBJ_RW, OBJ_RD }, sizeof(rtc_dt_t) },
  { CMD_INSTANT,          { OBJ_RD, OBJ_RD }, sizeof(instant_data_t) * PHASE_NUM, (uint8_t *)db.instant_data },
  { CMD_ENERGY,           { OBJ_RD, OBJ_RD }, sizeof(enengy_t),        (uint8_t *)&db.energy_data, PT_ENERGY },
  { CMD_REGULAR_HOLIDAYS, { OBJ_RW, OBJ_RD }, sizeof(db.schedule->regular_holiday), (uint8_t *)db.schedule->regular_holiday, PT_SHEDULE0, validate_regular_holidays, pkg_tarif_handler, PKG_NO_AUTO },
  { CMD_FIXED_HOLIDAYS,   { OBJ_RW, OBJ_RD }, sizeof(db.schedule->fixed_holiday), (uint8_t *)db.schedule->fixed_holiday, PT_SHEDULE0, validate_fixed_holidays, pkg_tarif_handler, PKG_NO_AUTO },
  { CMD_SEASONS,          { OBJ_RW, OBJ_RD }, sizeof(db.schedule->season), (uint8_t *)db.schedule->season, PT_SHEDULE0, validate_seasons, pkg_tarif_handler, PKG_NO_AUTO },
  { CMD_TARIF_PROGRAMS,   { OBJ_RW, OBJ_RD }, sizeof(db.schedule->tarif_program), (uint8_t *)db.schedule->tarif_program, PT_SHEDULE0, validate_tarif_programs, pkg_tarif_handler, PKG_NO_AUTO },
  { CMD_STATUS,           { OBJ_RD, OBJ_NO }, sizeof(dev_st_t),        (uint8_t *)&db.st },
  { CMD_COUNTERS,         { OBJ_RD, OBJ_RD }, sizeof(counters_t),      (uint8_t *)&db.cnt, PT_COUNTERS },
  { CMD_ACC,              { OBJ_RD, OBJ_NO }, sizeof(acc_data_t),      (uint8_t *)&db.acc, PT_ACC, 0, pkg_acc_handler },
  { CMD_SHEDULE0,         { OBJ_RD, OBJ_NO }, sizeof(schedule_t),      (uint8_t *)&db.schedule[0], PT_SHEDULE0 },
  { CMD_SHEDULE1,         { OBJ_RD, OBJ_NO }, sizeof(schedule_t),      (uint8_t *)&db.schedule[1], PT_SHEDULE1 },
  { CMD_LIMITS,           { OBJ_RW, OBJ_RD }, sizeof(limits_t),        (uint8_t *)&db.lim, PT_LIMITS, validate_limits, pkg_limits_handler },
  { CMD_REBOOT,           { OBJ_WR, OBJ_NO } },
  { CMD_EEFS_PT_SEARCH,   { OBJ_WR, OBJ_WR }, sizeof(eefs_search_t) },
  { CMD_EEFS_PT_RESET,    { OBJ_WR, OBJ_NO }, sizeof(uint32_t) },
  { CMD_LIM_SHEDULE,      { OBJ_RW, OBJ_RD }, sizeof(lim_shedule_t),   (uint8_t *)&db.lim_shedule, PT_LIM_SHEDULE, validate_lim_shedule },
};

#ifdef DEBUG
void pkg_printf_obj_table(void)
{
  for(int i = 0; i < SIZE_OF_ARRAY(objs); i++)
    slog("%s[%02x]: %d", objs_names[i], i, objs[i].size);
}
#endif

uint32_t pkg_reset_data(if_cmd_e cmd)
{
  slog("%s: %s", __FUNCTION__, objs_names[cmd]);

  // default value init objects
  if(cmd == CMD_RESET)
  {
    // RESET cmd + RESET obj code -> full eeprom erase
    eefs_format();
    pkg_auto_load_objs();
  }
  else if(cmd == CMD_CONFIG)
  {
    db.config = def_config;
    eefs_save(PT_CONFIG, &db.config);
  }
  else if(cmd == CMD_CLOCK)
  {
    rtc_set_dt(&def_clock);
  }
  else if(cmd == CMD_LIMITS)
  {
    db.lim = def_limits;
    eefs_save(PT_LIMITS, &db.lim);
  }
  // zero init objects, archived in EEFS
  else
  {
    const if_obj_t *obj = &objs[cmd];
    // object must have pointer to DB and EEFS PT
    if(!obj->table || !obj->eefs_pt) return 1;
    // reset object in RAM & EEFS
    memset(obj->table, 0, obj->size);
    eefs_save(obj->eefs_pt, obj->table);
  }

  return 0;
}

void pkg_auto_load_objs()
{
  for(int i = 0; i < SIZE_OF_ARRAY(objs); i++)
  {
    if(objs[i].eefs_pt && !(objs[i].flags & PKG_NO_AUTO))
    {
      if(eefs_load(objs[i].eefs_pt, objs[i].table))
        pkg_reset_data(objs[i].command);
    }
  }

  device_full_update_shedule();
  calc_setup(db.config.profile_time);
  dev_update_shedule_crc();
}

static void pkg_send(if_msg_t *rcv, const void *data, uint32_t size)
{
  uint16_t crc = 0;
  iovec vec[] = {
    { db.config.addr, sizeof(db.config.addr) },
    { &rcv->cmd, sizeof(rcv->cmd) },
    { (void*)data, size },
    { &crc, sizeof(crc) }};
  crc = crc_calc16(vec, SIZE_OF_ARRAY(vec) - 1);

  rcv->io_if->transaction(rcv->io_if, vec, SIZE_OF_ARRAY(vec), 0, 0);
}

static void pkg_ack(if_msg_t *rcv)
{
  pkg_send(rcv, 0, 0);
}

static void pkg_nack(if_msg_t *rcv, if_status_e status)
{
  rcv->cmd |= PKG_ERR_FLAG;
  uint8_t buf[] = { (uint8_t)status };
  pkg_send(rcv, buf, sizeof(buf));
}

static void pkg_default_handler(if_msg_t *rcv)
{
  const if_obj_t *obj = rcv->obj;

  if(rcv->cmd & PKG_WRITE_ACCESS_FLAG)
  {
    if(obj->validate && obj->validate(rcv->payload) != VALIDATE_OK)
    {
      pkg_nack(rcv, ST_BAD_PARAM);
      return;
    }
    // diff handler
    if(obj->handler) obj->handler(rcv, PKG_MGMT);
    // use auto memory management if manual is't defined
    if(obj->table && !(obj->flags & PKG_NO_AUTO))
    {
      memcpy(obj->table, rcv->payload, obj->size);
      if(obj->eefs_pt) eefs_save(obj->eefs_pt, obj->table);
    }
    // apply handler
    if(obj->handler) obj->handler(rcv, PKG_WR);
    pkg_ack(rcv);
  }
  else
  {
    if(obj->handler) obj->handler(rcv, PKG_RD);
    if(obj->table && !(obj->flags & PKG_NO_AUTO)) pkg_send(rcv, obj->table, obj->size);
  }
}

void pkg_tarif_handler(if_msg_t *rcv, handler_stage_e stage)
{
  const if_obj_t *obj = rcv->obj;

  if(stage == PKG_MGMT)
  {
    uint8_t sign = obj->command - CMD_REGULAR_HOLIDAYS + 1;
    event(PT_JOURNAL_SHEDULE_UPD, PWD_PORT | (sign << SIGN_OFFSET), 0);
    return;
  }

  uint32_t index = db.config.edit_schedule_index;
  uint8_t *data = obj->table + index * sizeof(schedule_t);

  if(rcv->cmd & PKG_WRITE_ACCESS_FLAG)
  {
    memcpy(data, rcv->payload, obj->size);
    eefs_save(obj->eefs_pt + index, &db.schedule[index]);
    dev_update_shedule_crc();
  }
  else
  {
    pkg_send(rcv, data, obj->size);
  }
}

void pkg_config_handler(if_msg_t *rcv, handler_stage_e stage)
{
  // configuration diff event notifier
  if(stage == PKG_MGMT)
  {
    device_config_t *rcv_cfg = (device_config_t *)rcv->payload;
    if(db.config.baudrate != rcv_cfg->baudrate)
    {
      event(PT_JOURNAL_IF_UPD, PWD_PORT, 0);
    }
    if(db.config.profile_time != rcv_cfg->profile_time ||
        db.config.profile_type != rcv_cfg->profile_type)
    {
      event(PT_JOURNAL_PROFILE_CFG_UPD, PWD_PORT, 0);
    }
    if(db.config.manual_tarif != rcv_cfg->manual_tarif)
    {
      uint8_t sign = rcv_cfg->manual_tarif ? 1 : 0;
      event(PT_JOURNAL_SHEDULE_EVT, PWD_PORT | (sign << SIGN_OFFSET), 0);
    }
    return;
  }

  // handler
  if(rcv->cmd & PKG_WRITE_ACCESS_FLAG)
  {
    // need for auto/manual shedule, tarif index
    device_full_update_shedule();
    // set rb size
    calc_setup(db.config.profile_time);
  }
}

void pkg_acc_handler(if_msg_t *rcv, handler_stage_e stage)
{
  if(stage == PKG_MGMT) return;

  calc_get(db.acc.average);
}

void pkg_limits_handler(if_msg_t *rcv, handler_stage_e stage)
{
  // configuration diff event notifier
  if(stage == PKG_MGMT)
  {
    limits_t *rcv_cfg = (limits_t *)rcv->payload;

    uint8_t relay_sign = 0;
    relay_sign |= (db.lim.relay.st != rcv_cfg->relay.st) ? RELAY_FLAG_CFG : 0;
    relay_sign |= (db.lim.relay.ret != rcv_cfg->relay.ret) ? RELAY_FLAG_CONDITION : 0;
    if(relay_sign)
    {
      event(PT_JOURNAL_RELAY_LIMITS_UPD, PWD_PORT | (relay_sign << SIGN_OFFSET), 0);
    }

    uint8_t net_sign = 0;
    net_sign |= (db.lim.net.u_max != rcv_cfg->net.u_max) ? NET_FLAG_U_MAX : 0;
    net_sign |= (db.lim.net.u_min != rcv_cfg->net.u_min) ? NET_FLAG_U_MIN : 0;
    net_sign |= (db.lim.net.df != rcv_cfg->net.df) ? NET_FLAG_DF : 0;
    net_sign |= (db.lim.net.i_min != rcv_cfg->net.i_min) ? NET_FLAG_I_MIN : 0;
    if(net_sign)
    {
      event(PT_JOURNAL_NET_LIMITS_UPD, PWD_PORT | (net_sign << SIGN_OFFSET), 0);
    }

    return;
  }

  // handler
  if(rcv->cmd & PKG_WRITE_ACCESS_FLAG)
  {
    dev_update_relay();
  }
}

void pkg_handler(if_msg_t *rcv)
{
  // process cmd
  switch(rcv->obj->command)
  {
    case CMD_SESSION:
      // WR cmd - open session
      if(rcv->cmd & PKG_WRITE_ACCESS_FLAG)
      {
        uint8_t user_index = 0;

        // search valid pwd
        for(user_index = 0; user_index < AR_MAX; user_index++)
        {
          if(!memcmp(rcv->payload, db.config.pwd[user_index].data, DEVICE_PWD_LEN))
          {
//            slog("user id: %d", user_index);
            rcv->session_tmr = MIN_TO_SEC(rcv->payload[DEVICE_PWD_LEN]);
            rcv->pwd_lock_cnt = 0;
            rcv->user_index = user_index;
            pkg_ack(rcv);
            break;
          }
        }
        // bad pwd
        if(user_index >= AR_MAX)
        {
          rcv->session_tmr = 0;
          rcv->pwd_lock_cnt++;
          rcv->user_index = 0;

          if(rcv->pwd_lock_cnt >= db.config.pwd_lock_cnt)
          {
            rcv->session_tmr = MIN_TO_SEC(DEVICE_PWD_LOCK_MIN);
            event_std(PT_JOURNAL_BAD_PWD_LOCK_EVT);
          }
          else event_std(PT_JOURNAL_BAD_PWD_EVT);

          pkg_nack(rcv, ST_BAD_PWD);
        }
      }
      else
      {
        // RD cmd - close session
        rcv->session_tmr = 0;
        rcv->pwd_lock_cnt = 0;
        rcv->user_index = 0;

        pkg_ack(rcv);
      }
      break;

    case CMD_RESET:
    {
      uint32_t cmd = *((uint32_t *)rcv->payload);
      if(!pkg_reset_data(cmd))
      {
        if(cmd == CMD_ENERGY) event(PT_JOURNAL_ENERGY_RST, PWD_PORT, 0);
        pkg_ack(rcv);
      }
      else pkg_nack(rcv, ST_BAD_PARAM);
    }
    break;

    case CMD_CLOCK:
    {
      if(rcv->cmd & PKG_WRITE_ACCESS_FLAG)
      {
        rtc_dt_t *rtc = (rtc_dt_t *)rcv->payload;
        rtc_dt_t last_rtc;

        rtc_get_dt(&last_rtc);
        event(PT_JOURNAL_RTC, rtc->time.sec, last_rtc.time.sec);

        rtc_set_dt(rtc);
        pkg_ack(rcv);
      }
      else
      {
        rtc_dt_t dt;
        rtc_get_dt(&dt);
        pkg_send(rcv, &dt, sizeof(dt));
      }
    }
    break;

    case CMD_REBOOT:
      pkg_ack(rcv);
      wait_ms(PKG_TIMEOUT_MS);
      wdt_reset();
      break;

    case CMD_EEFS_PT_SEARCH:
      if(rcv->cmd & PKG_WRITE_ACCESS_FLAG)
      {
        eefs_search_t *cfg = (eefs_search_t *)rcv->payload;

        if(cfg->index) // get data block
        {
          if(cfg->pt >= PT_MAX)
          {
            pkg_nack(rcv, ST_BAD_PARAM);
            break;
          }
          uint32_t size = eefs_get_block_size(cfg->pt) + sizeof(eefs_record_header_t);
          uint8_t buf[size];
          eefs_find(cfg, buf);
          pkg_send(rcv, buf, size);
        }
        else // get count of data blocks
        {
          uint32_t cnt = eefs_find(cfg, 0);
          cnt |= CMD_SEARCH_INFO_FLAG;
          pkg_send(rcv, &cnt, sizeof(cnt));
        }
      }
      break;

    case CMD_EEFS_PT_RESET:
    {
      uint32_t pt = *(uint32_t *)rcv->payload;
      if(pt >= PT_MAX)
      {
        pkg_nack(rcv, ST_BAD_PARAM);
        break;
      }
      eefs_erase_pt(pt);
      pkg_ack(rcv);
    }
    break;

    default:
      pkg_default_handler(rcv);
      break;
  }
}

void pkg_receive(if_msg_t *rcv)
{
//  slog_pkg(rcv);

  // is IF locked ?
  if(rcv->pwd_lock_cnt >= db.config.pwd_lock_cnt) return;

  if(rcv->size < DEVICE_PKG_SERVICE_PART_SIZE)
  {
//    slog("pkg size: %d, but min allowed %d", rcv->size, DEVICE_PKG_SERVICE_PART_SIZE);
    return;
  }

  // check serial number
  if(memcmp(rcv->data, db.config.addr, DEVICE_ADDR_LEN))
  {
//    slog("bad SN");
    return;
  }

  // check crc
  iovec vec = { rcv->data, rcv->size - DEVICE_CRC_LEN };
  uint16_t crc = crc_calc16(&vec, 1);
  if(memcmp(&rcv->data[rcv->size - DEVICE_CRC_LEN], &crc, DEVICE_CRC_LEN))
  {
//    slog("crc error");
    pkg_nack(rcv, ST_BAD_CRC);
    return;
  }

  // check cmd
  rcv->cmd = *(rcv->data + DEVICE_ADDR_LEN);
  uint8_t cmd = rcv->cmd & PKG_CMD_PART;
  if(cmd >= CMD_MAX)
  {
    pkg_nack(rcv, ST_BAD_CMD);
    return;
  }

  // is session open ?
  if((!rcv->session_tmr) && cmd != CMD_SESSION) return;

  // check access type
  if_access_type_e rcv_access_type = (rcv->cmd & PKG_WRITE_ACCESS_FLAG) ? OBJ_WR : OBJ_RD;

  // find cmd in supported cmd list
  uint32_t cmd_index;
  for(cmd_index = 0; cmd_index < SIZE_OF_ARRAY(objs); cmd_index++)
  {
    if(cmd == (uint8_t)objs[cmd_index].command)
    {
      rcv->obj = &objs[cmd_index];
      break;
    }
  }
  // not found
  if(cmd_index >= SIZE_OF_ARRAY(objs))
  {
    pkg_nack(rcv, ST_BAD_CMD);
    return;
  }

  // validate access type
  if(!(objs[cmd_index].access_type[rcv->user_index] & rcv_access_type))
  {
//    slog("access denied: request %s, but allowed only %s", access_type_str[rcv_access_type], access_type_str[objs[i].access_type]);
    pkg_nack(rcv, ST_BAD_ACCESS_TYPE);
    return;
  }

  // check data size
  int rcv_data_size = rcv->size - DEVICE_PKG_SERVICE_PART_SIZE;
  if(rcv_access_type == OBJ_WR)
  {
    if(objs[cmd_index].size != rcv_data_size)
    {
      slog("bad data size for write (must be %d, really: %d)!", objs[cmd_index].size, rcv_data_size);
      pkg_nack(rcv, ST_BAD_DATA_SIZE);
      return;
    }
  }
  else if(rcv_data_size > 0)
  {
    slog("bad data size for read (must be %d, really: %d)!", 0, rcv_data_size);
    pkg_nack(rcv, ST_BAD_DATA_SIZE);
    return;
  }

  // process data
  rcv->payload = rcv->data + DEVICE_ADDR_LEN + DEVICE_CMD_LEN;
  rcv->payload_size = rcv_data_size;
  pkg_handler(rcv);
}

