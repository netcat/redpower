/*
 * slog.h
 *
 *  Created on: Aug 7, 2019
 *      Author: vchumakov
 */

#ifndef SYS_SYSLOG_H_
#define SYS_SYSLOG_H_

#include "sys/pkg.h"

#define DBG_BUF_SIZE 256

#ifdef DEBUG
#define CONFIG_USE_SYSLOG
#endif

#ifdef CONFIG_USE_SYSLOG
void slog(const char *fmt, ...);
void slog_pkg(if_msg_t *pkg);
void slog_dt(const char *str, rtc_dt_t *dt);
#else
#define slog(...)
#define slog_pkg(...)
#define slog_dt(...)
#endif

#endif /* SYS_SYSLOG_H_ */
