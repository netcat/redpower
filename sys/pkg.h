/*
 * if.h
 *
 *  Created on: Aug 7, 2019
 *      Author: vchumakov
 */

#ifndef SYS_PKG_H_
#define SYS_PKG_H_

#include "eefs.h"
/*
 * general pkg structure:
  sn cd dt cc

  sn - device serial number 16 bytes
  cd - cmd (0x80 if write, 0x40 on error)
  dt - data
  cc - crc16
*/

// pkg structure
// s/n
#define DEVICE_ADDR_LEN 4
#define DEFAULT_ADDR 0x01, 0x00, 0x00, 0x00
// pwd
#define DEVICE_PWD_LEN 8
#define DEFAULT_PWD 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08
// sizes
#define DEVICE_CMD_LEN 1
#define DEVICE_CRC_LEN 2
#define DEVICE_PKG_SERVICE_PART_SIZE (DEVICE_ADDR_LEN + DEVICE_CMD_LEN + DEVICE_CRC_LEN)
#define DEVICE_SESSION_TIME_LEN 1
// command
#define PKG_WRITE_ACCESS_FLAG 0x80
#define PKG_ERR_FLAG 0x40
#define PKG_CMD_PART 0x3f
// cfg
#define PKG_TIMEOUT_MS 50

// service macros
#define MIN_TO_SEC(t) ((t) * 60)
#define PWD_PORT (rcv->user_index | (rcv->io_if->flags << 4))
#define SIGN_OFFSET 4

// search info flag
#define CMD_SEARCH_INFO_FLAG (1 << 31)
#define PKG_NO_AUTO (1 << 0)

#define IF_CMD_STRS \
  "CMD_SESSION", \
  "CMD_SW_ID", \
  "CMD_CONFIG", \
  "CMD_RESET", \
  "CMD_CLOCK", \
  "CMD_INSTANT", \
  "CMD_ENERGY", \
  "CMD_REGULAR_HOLIDAYS", \
  "CMD_FIXED_HOLIDAYS", \
  "CMD_SEASONS", \
  "CMD_TARIF_PROGRAMS", \
  "CMD_STATUS", \
  "CMD_COUNTERS", \
  "CMD_ACC", \
  "CMD_SHEDULE0", \
  "CMD_SHEDULE1", \
  "CMD_LIMITS", \
  "CMD_REBOOT", \
  "CMD_EEFS_PT_SEARCH", \
  "CMD_EEFS_PT_RESET", \
  "CMD_LIM_SHEDULE",

typedef enum
{
  CMD_SESSION,
  CMD_SW_ID,
  CMD_CONFIG,
  CMD_RESET,
  CMD_CLOCK,
  CMD_INSTANT,
  CMD_ENERGY,
  CMD_REGULAR_HOLIDAYS,
  CMD_FIXED_HOLIDAYS,
  CMD_SEASONS,
  CMD_TARIF_PROGRAMS,
  CMD_STATUS,
  CMD_COUNTERS,
  CMD_ACC,
  CMD_SHEDULE0,
  CMD_SHEDULE1,
  CMD_LIMITS,
  CMD_REBOOT,
  CMD_EEFS_PT_SEARCH,
  CMD_EEFS_PT_RESET,
  CMD_LIM_SHEDULE,

  CMD_MAX,

  CMD_LOG_MSG = 0x3F,
} if_cmd_e;

typedef enum
{
  OBJ_NO = 0x00,
  OBJ_RD = 0x01,
  OBJ_WR = 0x02,
  OBJ_RW = (OBJ_RD | OBJ_WR),
} if_access_type_e;

// access rule group of users
enum
{
  AR_ADMIN,
  AR_USER,

  AR_MAX
};

typedef enum
{
  ST_OK,
  ST_BAD_CMD,
  ST_BAD_CRC,
  ST_BAD_ACCESS_TYPE,
  ST_BAD_DATA_SIZE,
  ST_BAD_PWD,
  ST_BAD_PARAM,

  ST_MAX
} if_status_e;

typedef enum
{
  PKG_MGMT,
  PKG_WR,
  PKG_RD,
} handler_stage_e;

typedef struct
{
  // if
  io_if_t *io_if;
  // rxbuf
  uint8_t *data;
  uint32_t size;
  uint32_t flag;
  const struct if_obj_s *obj;
  // rx pkg processing
  uint8_t *payload;
  uint32_t payload_size;
  uint8_t cmd;
  uint8_t user_index;
  // private
  uint32_t session_tmr;
  uint32_t pwd_lock_cnt;
} if_msg_t;

typedef struct if_obj_s
{
  if_cmd_e command;
  if_access_type_e access_type[AR_MAX];
  uint32_t size;
  uint8_t *table;

  eefs_pt_e eefs_pt;
  uint32_t (*validate)(void *data);
  void (*handler)(if_msg_t *rcv, handler_stage_e stage);
  uint32_t flags;
} if_obj_t;

extern const if_obj_t objs[];
extern const char *objs_names[];

void pkg_receive(if_msg_t *pkg);
void pkg_auto_load_objs(void);
uint32_t pkg_reset_data(if_cmd_e cmd);

#endif /* SYS_PKG_H_ */
