/*
 * event.h
 *
 *  Created on: Aug 29, 2019
 *      Author: vchumakov
 */

#ifndef SYS_EVENTS_H_
#define SYS_EVENTS_H_

#include <stdint.h>

#include "eefs.h"
#include "def/def_journal.h"

#define TAMPER1_ST GPIO_GET_PIN(TAMPER1_PORT, TAMPER1_PIN)
#define TAMPER2_ST GPIO_GET_PIN(TAMPER2_PORT, TAMPER2_PIN)
#define MAG_ST (!GPIO_GET_PIN(MAG_PORT, MAG_PIN))

#define DELTA_U_PERCENT(val) ABS((((float)PWR_U_NOM - (val)) / (float)PWR_U_NOM) * 100)

void event_std(eefs_pt_e pt);
void event(eefs_pt_e pt, uint8_t info, uint32_t ext_info);
uint32_t count_tick(rtc_dt_t *dt);
void event_update(rtc_dt_t *dt);

#endif /* SYS_EVENTS_H_ */
