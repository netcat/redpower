/*
 * calculate.h
 *
 *  Created on: Aug 6, 2019
 *      Author: vchumakov
 */

#ifndef SYS_CALC_H_
#define SYS_CALC_H_

#include <stdint.h>

#define CALC_PMETER_CONST 10000 // calibration ?
#define CALC_ACC_AVERAGE_NUM 60

uint32_t calc_acc_to_energy(power_dir_float_t *acc, power_dir_t *storage);
void calc_instant_to_acc(instant_data_t *inst, power_dir_float_t *acc);
void calc_instant_to_avg(instant_data_t *inst, average_data_t *acc);

void calc_rb_append(average_data_t *average, uint32_t cnt);
void calc_setup(uint32_t size);
void calc_get(average_data_t *average);

#endif /* SYS_CALC_H_ */
