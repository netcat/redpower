/*
 * eefs.h
 *
 *  Created on: Jul 30, 2019
 *      Author: netcat
 */

#ifndef SYS_EEFS_H_
#define SYS_EEFS_H_

#include "hal/hw_rtc.h"

#define EEFS_VERSION_MAJOR 0
#define EEFS_VERSION_MINOR 4

#define EEFS_BLOCK_ADDR(header, block) ((header)->addr + (((header)->block_size + sizeof(eefs_record_header_t)) * block))

#define EEFS_PT_STRS \
  "PT_INFO", \
  "PT_CONFIG", \
  "PT_SHEDULE0", \
  "PT_SHEDULE1", \
  "PT_ENERGY", \
  "PT_COUNTERS", \
  "PT_SLICE_DAY", \
  "PT_SLICE_MONTH", \
  "PT_SLICE_YEAR", \
  "PT_ACC", \
  "PT_LIMITS", \
  "PT_LIM_SHEDULE", \
  \
  "PT_JOURNAL_RTC", \
  "PT_JOURNAL_RTC_SYNC", \
  "PT_JOURNAL_RTC_SYNC_LIMIT_D", \
  "PT_JOURNAL_RTC_SYNC_CRITICAL", \
  "PT_JOURNAL_RTC_SYNC_LIMIT_MY", \
  "PT_JOURNAL_RTC_SUMMER_TIME_EVT", \
  "PT_JOURNAL_RTC_CORRECTION_UPD", \
  "PT_JOURNAL_RTC_SUMMER_TIME_UPD", \
  "PT_JOURNAL_ENERGY_RST", \
  "PT_JOURNAL_PROFILE_CFG_UPD", \
  "PT_JOURNAL_METROLOGY_UPD", \
  "PT_JOURNAL_SHEDULE_EVT", \
  "PT_JOURNAL_SHEDULE_UPD", \
  "PT_JOURNAL_SHEDULE_SW", \
  "PT_JOURNAL_PWR_LIMITS_UPD", \
  "PT_JOURNAL_NET_LIMITS_UPD", \
  "PT_JOURNAL_RELAY_LIMITS_UPD", \
  "PT_JOURNAL_RELAY_LIMITS_EVT", \
  "PT_JOURNAL_IF_UPD", \
  "PT_JOURNAL_PWR_UP_DOWN_EVT", \
  "PT_JOURNAL_PHASE_A_U_OVER_MIN", \
  "PT_JOURNAL_PHASE_B_U_OVER_MIN", \
  "PT_JOURNAL_PHASE_C_U_OVER_MIN", \
  "PT_JOURNAL_PHASE_A_U_OVER_MAX", \
  "PT_JOURNAL_PHASE_B_U_OVER_MAX", \
  "PT_JOURNAL_PHASE_C_U_OVER_MAX", \
  "PT_JOURNAL_PHASE_A_I_OVER_MAX", \
  "PT_JOURNAL_PHASE_B_I_OVER_MAX", \
  "PT_JOURNAL_PHASE_C_I_OVER_MAX", \
  "PT_JOURNAL_SUM_I_OVER_MIN", \
  "PT_JOURNAL_F_OVER_LIMIT", \
  "PT_JOURNAL_S_OVER_MAX", \
  "PT_JOURNAL_ENERGY_OVER_MAX", \
  "PT_JOURNAL_BAD_PWD_LOCK_EVT", \
  "PT_JOURNAL_BAD_PWD_EVT", \
  "PT_JOURNAL_MAG_EVT", \
  "PT_JOURNAL_EMAG_EVT", \
  "PT_JOURNAL_OPEN_CONN_EVT", \
  "PT_JOURNAL_OPEN_EVT", \
  "PT_JOURNAL_PHASE_ERR_EVT", \
  "PT_JOURNAL_TERM_EVT", \
  \
  "PT_PROFILE"

typedef enum
{
  PT_INFO,
  PT_CONFIG,
  PT_SHEDULE0,
  PT_SHEDULE1,
  PT_ENERGY,
  PT_COUNTERS,
  PT_SLICE_DAY,
  PT_SLICE_MONTH,
  PT_SLICE_YEAR,
  PT_ACC,
  PT_LIMITS,
  PT_LIM_SHEDULE,

  PT_JOURNAL_RTC,
  PT_JOURNAL_RTC_SYNC,
  PT_JOURNAL_RTC_SYNC_LIMIT_D,
  PT_JOURNAL_RTC_SYNC_CRITICAL,
  PT_JOURNAL_RTC_SYNC_LIMIT_MY,
  PT_JOURNAL_RTC_SUMMER_TIME_EVT,
  PT_JOURNAL_RTC_CORRECTION_UPD,
  PT_JOURNAL_RTC_SUMMER_TIME_UPD,
  PT_JOURNAL_ENERGY_RST,
  PT_JOURNAL_PROFILE_CFG_UPD,
  PT_JOURNAL_METROLOGY_UPD,
  PT_JOURNAL_SHEDULE_EVT,
  PT_JOURNAL_SHEDULE_UPD,
  PT_JOURNAL_SHEDULE_SW,
  PT_JOURNAL_PWR_LIMITS_UPD,
  PT_JOURNAL_NET_LIMITS_UPD,
  PT_JOURNAL_RELAY_LIMITS_UPD,
  PT_JOURNAL_RELAY_LIMITS_EVT,
  PT_JOURNAL_IF_UPD,
  PT_JOURNAL_PWR_UP_DOWN_EVT,
  PT_JOURNAL_PHASE_A_U_OVER_MIN,
  PT_JOURNAL_PHASE_B_U_OVER_MIN,
  PT_JOURNAL_PHASE_C_U_OVER_MIN,
  PT_JOURNAL_PHASE_A_U_OVER_MAX,
  PT_JOURNAL_PHASE_B_U_OVER_MAX,
  PT_JOURNAL_PHASE_C_U_OVER_MAX,
  PT_JOURNAL_PHASE_A_I_OVER_MAX,
  PT_JOURNAL_PHASE_B_I_OVER_MAX,
  PT_JOURNAL_PHASE_C_I_OVER_MAX,
  PT_JOURNAL_SUM_I_OVER_MIN,
  PT_JOURNAL_F_OVER_LIMIT,
  PT_JOURNAL_S_OVER_MAX,
  PT_JOURNAL_ENERGY_OVER_MAX,
  PT_JOURNAL_BAD_PWD_LOCK_EVT,
  PT_JOURNAL_BAD_PWD_EVT,
  PT_JOURNAL_MAG_EVT,
  PT_JOURNAL_EMAG_EVT,
  PT_JOURNAL_OPEN_CONN_EVT,
  PT_JOURNAL_OPEN_EVT,
  PT_JOURNAL_PHASE_ERR_EVT,
  PT_JOURNAL_TERM_EVT,

  PT_PROFILE,

  PT_MAX
} eefs_pt_e;

typedef struct
{
  uint16_t ver_major;
  uint16_t ver_minor;
  uint16_t parts;
  uint16_t blocks;
  uint32_t size;
  uint32_t res[1];
} eefs_info_header_t;

typedef struct
{
  rtc_dt_t dt;
  uint16_t crc;
  uint8_t flags;
  uint8_t reserved[1];
} eefs_record_header_t;

typedef struct
{
  uint32_t code;
  uint32_t blocks;
  uint32_t block_size;

  // private:
  uint32_t addr;
  uint32_t index;
} eefs_pt_t;

typedef enum
{
  SEARCH_DAILY,
  SEARCH_MONTHLY,
  SEARCH_YEARLY,
  SEARCH_PROFILES,
} search_mode_t;

typedef struct
{
  rtc_interval_t interval;
  uint32_t pt;
  uint32_t index;
} eefs_search_t;

void eefs_init();
void eefs_format();
void eefs_erase_pt(eefs_pt_e pt);
int eefs_get_block_size(eefs_pt_e pt);
void eefs_save(eefs_pt_e pt, void *data);
void eefs_save_timed(eefs_pt_e pt, void *data, rtc_dt_t *dt);
int eefs_load(eefs_pt_e pt, void *data);
uint32_t eefs_find(eefs_search_t *cfg, uint8_t *buf);

#endif /* SYS_EEFS_H_ */
