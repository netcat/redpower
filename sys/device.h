/*
 * device.h
 *
 *  Created on: Jul 5, 2019
 *      Author: vchumakov
 */

#ifndef DEVICE_H_
#define DEVICE_H_

#include <stdint.h>
#include "hal/hw_rtc.h"

#include "pkg.h"
#include "def/def_power.h"
#include "def/def_schedule.h"
#include "def/def_counters.h"
#include "def/def_config.h"
#include "def/def_journal.h"
#include "eefs.h"

#define DEVICE_PWD_LOCK_MIN 1
#define INV(x) ((x) ? 0 : 1)

typedef struct
{
  // RTC
  // rtc sec IF
  uint32_t rtc_sec_tick;
  // TR reg val each IF
  rtc_raw_t rtc_raw;

  // TP
  // index in shedule tables (0 if no data)
  uint32_t regular_holyday_index;
  uint32_t fixed_holyday_index;
  uint32_t season_index;
  uint32_t tarif_program_index;
  uint32_t tarif_index;
  uint32_t system_event;

  // other
  uint32_t crc_shedule[POWER_SHEDULE_NUM];
} dev_st_t;

typedef struct
{
  dev_st_t st;

  // power data
  instant_data_t instant_data[PHASE_NUM];
  enengy_t energy_data;
  acc_data_t acc;

  schedule_t schedule[POWER_SHEDULE_NUM];
  device_config_t config;
  counters_t cnt;
  limits_t lim;
  lim_shedule_t lim_shedule;
} dev_db_t;

extern dev_db_t db;

void device_init(void);
void device_full_update_shedule(void);
void device_loop(void);
void dev_update_relay(void);
void device_full_update_shedule(void);
void dev_update_shedule_crc(void);
void device_update_half_hour(rtc_dt_t *dt);

#endif /* DEVICE_H_ */
