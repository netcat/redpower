/*
 * device.c
 *
 *  Created on: Jul 5, 2019
 *      Author: vchumakov
 */

#include "hal/hw_defs.h"

#include "device.h"

#include "bsp.h"
#include "hw_rtc.h"
#include "hw_gpio.h"
#include "eefs.h"
#include "validators.h"
#include "calc.h"
#include "sys/syslog.h"
#include "pkg.h"
#include "cs5490.h"
#include "hw_crc.h"
#include "sys/events.h"

dev_db_t db = { 0 };

void RTC_Alarm_IRQHandler(void)
{
  if(RTC->ISR & RTC_ISR_ALRAF)
  {
    RTC->ISR = ~RTC_ISR_ALRAF; // clear alarm IE
    EXTI->PR1 |= EXTI_PR1_PIF18; // clear trigger request
    EXTI->EMR1 &= ~EXTI_EMR1_EM18; // clear event mask

    db.st.rtc_sec_tick = 1;
    rtc_get_raw(&db.st.rtc_raw);
  }
}

void device_init()
{
#ifdef DEBUG
  db.config.debug_mode = DEBUG_ON;
  slog("DEBUG, build: %s, %s", __DATE__, __TIME__);
//  pkg_printf_obj_table();
#endif

  if(RTC_BACKUP_REG(0) != RTC_BACKUP_MAGIC)
  {
    RTC_BACKUP_REG(0) = RTC_BACKUP_MAGIC;
    pkg_reset_data(CMD_CLOCK);
  }

  eefs_init();
  pkg_auto_load_objs();
}

void device_update_daily(rtc_date_t *date)
{
  uint32_t index = INV(db.config.edit_schedule_index);

  // get TP indexes from shedule tables
  // return value must be > 0, if = 0 - not found
  db.st.regular_holyday_index = validate_is_regular_holiday(db.schedule[index].regular_holiday, POWER_HOLIDAY_NUM, date);
  db.st.fixed_holyday_index = validate_is_fixed_holiday(db.schedule[index].fixed_holiday, POWER_HOLIDAY_FIX_NUM, date);
  db.st.season_index = validate_is_season(db.schedule[index].season, POWER_SEASON_NUM, date);

  // select TP by individual priority:
  // fixed - hi
  // regular - medium
  // season - low
  if(db.st.fixed_holyday_index != VALIDATE_NODATA) db.st.tarif_program_index = db.st.fixed_holyday_index;
  else if(db.st.regular_holyday_index != VALIDATE_NODATA) db.st.tarif_program_index = db.st.regular_holyday_index;
  else if(db.st.season_index != VALIDATE_NODATA) db.st.tarif_program_index = db.st.season_index;
  else db.st.tarif_program_index = 0;
}

void device_update_half_hour(rtc_dt_t *dt)
{
  // if manual
  if(db.config.manual_tarif > 0) db.st.tarif_index = db.config.manual_tarif;
  // if event
  else if(db.st.system_event) db.st.tarif_index = TARIF_EXT_INDEX;
  // else use tarif tables
  else if(db.st.tarif_program_index)
  {
    uint32_t index = INV(db.config.edit_schedule_index);
    db.st.tarif_index = (db.schedule[index] \
      .tarif_program[db.st.tarif_program_index - 1].index_per_hour[dt->time.hour] >> (dt->time.min ? 4 : 0)) & 0x0f;
  }
  else db.st.tarif_index = 0;
}

void device_full_update_shedule()
{
  rtc_dt_t dt;
  rtc_get_dt(&dt);
  device_update_daily(&dt.date);
  device_update_half_hour(&dt);
}

void dev_update_shedule_crc()
{
  for(int i = 0; i < POWER_SHEDULE_NUM; i++)
  {
    iovec vec = { &db.schedule[i], sizeof(schedule_t) };
    uint16_t crc = crc_calc16(&vec, 1);
    db.st.crc_shedule[i] = crc;
  }
}

static void dev_session_tmr_update()
{
  // substract time of session
  for(int i = OIF_RS485; i < OIF_MAX; i++)
  {
    if_msg_t *t = hal_pkg_if(i);

    if(t->session_tmr)
    {
      t->session_tmr--;
      if(!t->session_tmr)
      {
        t->pwd_lock_cnt = 0;
        slog("%s: session/lock[%d] expired", __FUNCTION__, i);
      }
    }
  }
}

void dev_update_relay()
{
  uint32_t relay = db.lim.relay.st;
  if(relay == RELAY_ON) bsp_relay_set(1);
  else if(relay == RELAY_OFF) bsp_relay_set(0);
  else
  {
    // todo: auto mode
  }
}

void device_loop()
{
  // each 1 sec
  if(db.st.rtc_sec_tick)
  {
    db.st.rtc_sec_tick = 0;
    GPIO_TOGGLE(LED_ACT_PORT, LED_ACT_PIN);

    rtc_dt_t dt;
    rtc_raw2time(&db.st.rtc_raw, &dt.time);
    rtc_raw2date(&db.st.rtc_raw, &dt.date);

    // each 1 min (sec = 0)
    if(!RTC_RAW_SEC(db.st.rtc_raw.time))
    {
      // each 1 day
      if(dt.time.hour == 0 && dt.time.min == 0)
      {
        slog("each 1day");

        // applying new tarification
        if(validate_is_same_dates(&dt.date, &db.config.shedule_date))
        {
          slog("applying new tarification");
          db.config.edit_schedule_index = INV(db.config.edit_schedule_index);

          event_std(PT_JOURNAL_SHEDULE_SW);
        }

        // slices
        eefs_save_timed(PT_SLICE_DAY, &db.energy_data, &dt);
        // each 1 month
        if(dt.date.day == 1)
        {
          eefs_save_timed(PT_SLICE_MONTH, &db.energy_data, &dt);
          // each 1 year
          if(dt.date.month == 1) eefs_save_timed(PT_SLICE_YEAR, &db.energy_data, &dt);
        }

        // refresh tarif program
        device_update_daily(&dt.date);
      }

#ifdef DEBUG
      eefs_save_timed(PT_SLICE_DAY, &db.energy_data, &dt); // test only
#endif

      // each 30m
      if(dt.time.min == 0 || dt.time.min == 30)
      {
        slog("each 30m");
        // get tarif number
        device_update_half_hour(&dt);
      }
    }

    uint32_t store_flag = 0;
    for(int phase = 0; phase < PHASE_NUM; phase++)
    {
      // is measure enabled?
      if(db.config.measure_mode)
      {
        // get measuring data
        if(!cs5490_get_instant(hal_io_if(phase), &db.instant_data[phase]))
        {
          // store data in accumulator
          calc_instant_to_acc(&db.instant_data[phase], &db.acc.energy.phase[phase]);
          calc_instant_to_avg(&db.instant_data[phase], &db.acc.sum[phase]);
          // there is something to store?
          store_flag += calc_acc_to_energy(&db.acc.energy.phase[phase], &db.energy_data.tarif[db.st.tarif_index].phase[phase]);
        }
        else
        {
          // todo: reinit meter IC & system event
          slog("%s: meter IC[%d] read error", phase);
        }
      }
    }

    // profile params average
    if(db.acc.sum_cnt++ >= CALC_ACC_AVERAGE_NUM)
    {
      calc_rb_append(db.acc.sum, db.acc.sum_cnt);
      db.acc.sum_cnt = 0;
    }

    // save energy
    if(store_flag)
    {
      eefs_save(PT_ENERGY, &db.energy_data);
      eefs_save(PT_ACC, &db.acc.energy);
    }

    event_update(&dt);
    uint32_t any_event_flag = count_tick(&dt);
    dev_session_tmr_update();

    // generate system event on
    if(any_event_flag)
    {
      if(!db.st.system_event)
      {
        db.st.system_event = 1;
        device_update_half_hour(&dt);
      }
      // backup timers
      eefs_save(PT_COUNTERS, &db.cnt);
    }
    else if(db.st.system_event)
    {
      db.st.system_event = 0;
      device_update_half_hour(&dt);

      // backup timers
      eefs_save(PT_COUNTERS, &db.cnt);
    }
  }
}

