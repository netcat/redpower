/*
 * schedule.h
 *
 *  Created on: Jul 8, 2019
 *      Author: vchumakov
 */

#ifndef SYS_DEF_SCHEDULE_H_
#define SYS_DEF_SCHEDULE_H_

#include <stdint.h>
#include "hal/hw_rtc.h"

#define POWER_HOLIDAY_NUM 16

typedef struct
{
  uint8_t day;
  uint8_t month;
  uint8_t index;
} holiday_t;

#define POWER_HOLIDAY_FIX_NUM 96

typedef struct
{
  uint8_t day;
  uint8_t month;
  uint8_t year;
  uint8_t index;
} holiday_fix_t;

#define POWER_TARIF_PROGRAM_NUM 32
// 2x30 in one byte
#define POWER_TARIF_PROGRAM_PERIOD_NUM 24

typedef struct
{
  uint8_t priority;
  uint8_t index_per_hour[POWER_TARIF_PROGRAM_PERIOD_NUM];
} tarif_program_t;

#define POWER_SEASON_NUM 12

typedef struct
{
  uint8_t day;
  uint8_t month;
  uint8_t index_per_wd[7];
} season_t;

typedef struct
{
  holiday_t regular_holiday[POWER_HOLIDAY_NUM];
  holiday_fix_t fixed_holiday[POWER_HOLIDAY_FIX_NUM];
  season_t season[POWER_SEASON_NUM];
  tarif_program_t tarif_program[POWER_TARIF_PROGRAM_NUM];
} schedule_t;

#define POWER_SHEDULE_NUM 2

// ---------- limits shedule ----------

#define LIM_ZONE_NUM 3

typedef struct
{
  rtc_time_t st;
  rtc_time_t end;
} lim_zone_t;

typedef struct
{
  lim_zone_t zone[LIM_ZONE_NUM];
  rtc_date_t date;
} lim_shedule_item_t;

#define LIM_SHEDULE_NUM 12

typedef struct
{
  lim_shedule_item_t shedule[LIM_SHEDULE_NUM];
  uint32_t limit[LIM_ZONE_NUM];
  uint32_t maximum[LIM_ZONE_NUM];
} lim_shedule_t;

#endif /* SYS_DEF_SCHEDULE_H_ */
