/*
 * default.h
 *
 *  Created on: Jul 10, 2019
 *      Author: vchumakov
 */

#ifndef SYS_DEFAULT_H_
#define SYS_DEFAULT_H_

#include "def_power.h"
#include "def_schedule.h"

const device_config_t def_config = {
  { DEFAULT_ADDR },
  { {{ DEFAULT_PWD }} },
  BAUD_115200,
  3,
  DEBUG_ON,
  MEASURE_OFF,
  1,
  { 1, 1, 19, 2 },
  0,
  3,
  0
};

const rtc_dt_t def_clock = {
    { 00, 00, 00 },
    { 1, 1, 19, 2 }
};

const limits_t def_limits = {
    { .u_max = 270, .u_min = 220, .u_gist = 10,
      .df = 250,
      .i_max = 95000, .i_min = 100, .i_gist = 10, },
    { .st = RELAY_AUTO, .ret = RELAY_RET_CMD, }
};

#endif /* SYS_DEFAULT_H_ */
