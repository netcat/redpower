/*
 * def_config.h
 *
 *  Created on: Aug 22, 2019
 *      Author: vchumakov
 */

#ifndef SYS_DEF_CONFIG_H_
#define SYS_DEF_CONFIG_H_

#include "hal/hw_rtc.h"
#include "pkg.h"

typedef enum
{
  DEBUG_OFF,
  DEBUG_ON,
} debug_mode_e;

typedef enum
{
  MEASURE_OFF,
  MEASURE_ON,
} measure_mode_e;

typedef enum
{
  RELAY_AUTO,
  RELAY_ON,
  RELAY_OFF,

  RELAY_MAX
} relay_e;

typedef enum
{
  RELAY_RET_CMD,
  RELAY_RET_CMD_BT,

  RELAY_RET_MAX
} relay_ret_e;

typedef enum
{
  BAUD_1200,
  BAUD_2400,
  BAUD_4800,
  BAUD_9600,
  BAUD_14400,
  BAUD_19200,
  BAUD_38400,
  BAUD_57600,
  BAUD_115200,

  BAUD_MAX,
} device_baudrate_e;

typedef struct
{
  uint8_t data[DEVICE_PWD_LEN];
} device_pwd_t;

typedef struct
{
  uint8_t addr[DEVICE_ADDR_LEN];
  device_pwd_t pwd[AR_MAX];
  uint8_t baudrate;
  uint8_t pwd_lock_cnt;
  uint8_t debug_mode;
  uint8_t measure_mode;
  uint8_t edit_schedule_index;
  rtc_date_t shedule_date;
  uint8_t manual_tarif;
  uint8_t profile_time;
  uint8_t profile_type;
} device_config_t;

#endif /* SYS_DEF_CONFIG_H_ */
