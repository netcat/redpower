/*
 * power.h
 *
 *  Created on: Jul 5, 2019
 *      Author: vchumakov
 */

#ifndef SYS_DEF_POWER_H_
#define SYS_DEF_POWER_H_

#include <stdint.h>

enum
{
  PHASE_A,
  PHASE_B,
  PHASE_C,
  PHASE_NUM,
};

#define TARIF_SUM 1
#define TARIF_STD 8
#define TARIF_EXT 1
#define TARIFS_NUM (TARIF_SUM + TARIF_STD + TARIF_EXT)

#define TARIF_SUM_INDEX 0
#define TARIF_STD_INDEX(n) (TARIF_SUM + (n))
#define TARIF_EXT_INDEX (TARIF_SUM + TARIF_STD + TARIF_EXT - 1)

// default net params
#define PWR_U_MIN 50
#define PWR_U_MAX 380
#define PWR_U_NOM 230

#define PWR_I_MIN 100 // mA
#define PWR_I_MAX 100000 // mA

#define PWR_NET_FREQ_NOM 50.0
#define PWR_DF_GIST_PERCENT 5

// ---------- float data type (instant, storage) ----------
typedef struct
{
  float S;
  float P;
  float Q;
} power_float_t;

typedef struct
{
  power_float_t fwd;
  power_float_t bwd;
} power_dir_float_t;

typedef struct
{
  power_dir_float_t phase[PHASE_NUM];
} power_phase_float_t;

// instant
typedef struct
{
  power_float_t load;
  float U;
  float I;
  float F;
  float PF;
  float angle;
} instant_data_t;

typedef struct
{
  float U;
  float I;
  float S;
  float F;
} average_data_t;

// acc
typedef struct
{
  power_phase_float_t energy;
  // calculated average (every 1min)
  average_data_t average[PHASE_NUM];
  // buffer for average data calculating
  average_data_t sum[PHASE_NUM];
  uint32_t sum_cnt;
} acc_data_t;

// ---------- integer data type (energy) ----------
typedef struct
{
  uint32_t S;
  uint32_t P;
  uint32_t Q;
} power_t;

typedef struct
{
  power_t fwd;
  power_t bwd;
} power_dir_t;

typedef struct
{
  power_dir_t phase[PHASE_NUM];
} power_phase_t;

// energy
typedef struct
{
  power_phase_t tarif[TARIFS_NUM];
} enengy_t;

// ---------- limits ----------

enum
{
  NET_FLAG_U_MAX = 1,
  NET_FLAG_U_MIN = 2,
  NET_FLAG_DF = 4,
  NET_FLAG_I_MIN = 8,
};

typedef struct
{
  uint32_t u_max;
  uint32_t u_min;
  uint32_t u_gist;

  uint32_t df;
  int32_t t_max;

  uint32_t i_max;
  uint32_t i_min;
  uint32_t i_gist;
} limits_net_t;

enum
{
  RELAY_FLAG_CFG = 1,
  RELAY_FLAG_CONDITION = 2,
};

typedef struct
{
  uint32_t st;
  uint32_t ret;
} limits_relay_t;

typedef struct
{
  limits_net_t net;
  limits_relay_t relay;
} limits_t;

#endif /* SYS_DEF_POWER_H_ */
