/*
 * def_counters.h
 *
 *  Created on: Jul 31, 2019
 *      Author: netcat
 */

#ifndef SYS_DEF_COUNTERS_H_
#define SYS_DEF_COUNTERS_H_

#include <stdint.h>
#include "hal/hw_rtc.h"

typedef struct
{
  uint32_t val;
  uint32_t last;
  rtc_dt_t dt;
  uint32_t flags;
} cnt_t;

typedef struct
{
  cnt_t disabled;
  cnt_t mag;
  cnt_t u_min[PHASE_NUM];
  cnt_t u_max[PHASE_NUM];
  cnt_t i_max[PHASE_NUM];
  cnt_t i_min_sum;
  cnt_t df;
  cnt_t S_hi_over_limit;
  cnt_t S_lo_over_limit;
  cnt_t clock_correction;
  cnt_t bad_phase_connection;
  cnt_t tamper[2];
  cnt_t hot;
} counters_t;

#endif /* SYS_DEF_COUNTERS_H_ */
