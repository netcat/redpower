/*
 * def_journal.h
 *
 *  Created on: Aug 21, 2019
 *      Author: vchumakov
 */

#ifndef SYS_DEF_JOURNAL_H_
#define SYS_DEF_JOURNAL_H_

#include <stdint.h>
#include "def/def_power.h"

typedef struct
{
  uint32_t ext_info;
  uint8_t info;
} journal_t;

typedef struct
{
  float phase[PHASE_NUM];
} profile_t;

#endif /* SYS_DEF_JOURNAL_H_ */
