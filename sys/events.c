/*
 * events.c
 *
 *  Created on: Aug 29, 2019
 *      Author: vchumakov
 */

#include "device.h"
#include "sys/events.h"
#include "sys/syslog.h"
#include "bsp.h"
#include "hw_gpio.h"

extern const char *eefs_pt_names[];

void event_std(eefs_pt_e pt)
{
  slog("%s: %s", __FUNCTION__, eefs_pt_names[pt]);
  journal_t t = { 0 };
  eefs_save(pt, &t);
}

void event(eefs_pt_e pt, uint8_t info, uint32_t ext_info)
{
  slog("%s: %s, info: %02x, ext_info: %02x", __FUNCTION__, eefs_pt_names[pt], info, ext_info);
  journal_t t = { .info = info, .ext_info = ext_info };
  eefs_save(pt, &t);
}

static void count_start(cnt_t *cnt, rtc_dt_t *dt)
{
  cnt->last = cnt->val;
  cnt->dt = *dt;
}

static uint32_t count_end(cnt_t *cnt)
{
  uint32_t diff = cnt->val - cnt->last;
  cnt->last = 0;
  return diff;
}

uint32_t count_tick(rtc_dt_t *dt)
{
  uint32_t cnt = sizeof(counters_t) / sizeof(cnt_t);
  cnt_t *p = (cnt_t *)&db.cnt;
  uint32_t any_event_flag = 0;

  for(uint32_t i = 0; i < cnt; i++)
  {
    if(p[i].flags)
    {
      any_event_flag |= p[i].flags;
      p[i].val++;
    }
  }

  return any_event_flag;
}

static void count_comp(uint32_t sign_st, uint32_t sign_end, cnt_t *cnt, rtc_dt_t *dt, eefs_pt_e evt, uint8_t arg)
{
  if(sign_st && !cnt->flags)
  {
    cnt->flags = 1;
    count_start(cnt, dt);
    event_std(evt);
  }
  else if(sign_end && cnt->flags)
  {
    cnt->flags = 0;
    event(evt, arg, count_end(cnt));
  }
}

void event_update(rtc_dt_t *dt)
{
  // max of deviation
  static float u_max_d[PHASE_NUM] = { 0 }, u_min_d[PHASE_NUM] = { 0 };
  static uint8_t max_delta = 0, min_delta = 0;

  float i_sum = 0;
  uint32_t f_flags = 0;

  for(int phase = 0; phase < PHASE_NUM; phase++)
  {
    // Umax
    float u = db.instant_data[phase].U;
    const uint32_t u_gist = (PWR_U_NOM * db.lim.net.u_gist / 100);

    uint32_t u_max_sign_st = u > db.lim.net.u_max;
    uint32_t u_max_sign_end = u < (db.lim.net.u_max - u_gist);
    // save Umax pick
    if(u_max_d[phase] < u)
    {
      u_max_d[phase] = u;
      max_delta = DELTA_U_PERCENT(u);
    }
    if(u_max_sign_end) u_max_d[phase] = PWR_U_MIN;

    count_comp(u_max_sign_st, u_max_sign_end, &db.cnt.u_max[phase], dt, PT_JOURNAL_PHASE_A_U_OVER_MAX + phase, max_delta);

    // Umin
    uint32_t u_min_sign_st = u < db.lim.net.u_min;
    uint32_t u_min_sign_end = u > (db.lim.net.u_min + u_gist);
    // save Umin pick
    if(u_min_d[phase] > u)
    {
      u_min_d[phase] = u;
      min_delta = DELTA_U_PERCENT(u);
    }
    if(u_min_sign_end) u_min_d[phase] = PWR_U_MAX;

    count_comp(u_min_sign_st, u_min_sign_end, &db.cnt.u_min[phase], dt, PT_JOURNAL_PHASE_A_U_OVER_MIN + phase, min_delta);

    // Imax
    float i_val = db.instant_data[phase].I * 1000; // scale to mA
    const uint32_t i_max_gist = (db.lim.net.i_max * db.lim.net.i_gist / 100);

    uint32_t i_sign_st = i_val > db.lim.net.i_max;
    uint32_t i_sign_end = i_val < (db.lim.net.i_max - i_max_gist);
    count_comp(i_sign_st, i_sign_end, &db.cnt.i_max[phase], dt, PT_JOURNAL_PHASE_A_I_OVER_MAX + phase, 0);

    // Isum
    i_sum += i_val;

    // dF
    int32_t df = ABS((int)((db.instant_data[phase].F - PWR_NET_FREQ_NOM) * 1000));
    const uint32_t df_gist = (db.lim.net.df * PWR_DF_GIST_PERCENT) / 100;

    if(df > db.lim.net.df) f_flags |= (1 << phase);
    else if(df < (db.lim.net.df - df_gist)) f_flags &= ~(1 << phase);
  }
#ifdef DEBUG
  i_sum *= 1000; // test only
#endif

  // Imin_sum
  const uint32_t i_min_gist = (db.lim.net.i_min * db.lim.net.i_gist / 100);

  uint32_t sign_st = (int)i_sum < db.lim.net.i_min;
  uint32_t sign_end = (int)i_sum > (db.lim.net.i_min + i_min_gist);
  count_comp(sign_st, sign_end, &db.cnt.i_min_sum, dt, PT_JOURNAL_SUM_I_OVER_MIN, 0);

  // dF
  count_comp(f_flags, !f_flags, &db.cnt.df, dt, PT_JOURNAL_F_OVER_LIMIT, 0);

  // TAMPERS
  uint32_t st2 = TAMPER2_ST;
  count_comp(st2, !st2, &db.cnt.tamper[1], dt, PT_JOURNAL_OPEN_EVT, st2);

  uint32_t st1 = TAMPER1_ST;
  count_comp(st1, !st1, &db.cnt.tamper[0], dt, PT_JOURNAL_OPEN_CONN_EVT, st1);

  // MAG
  uint32_t mag = MAG_ST;
  count_comp(mag, !mag, &db.cnt.mag, dt, PT_JOURNAL_EMAG_EVT, 0);
}
