/*
 * validators.c
 *
 *  Created on: Aug 1, 2019
 *      Author: netcat
 */

#include <stdlib.h>
#include <string.h>

#include "hw_defs.h"
#include "def/def_power.h"
#include "def/def_schedule.h"
#include "def/def_counters.h"
#include "def/def_config.h"
#include "hal/hw_rtc.h"
#include "validators.h"

static void validate_day_month(uint8_t *d, uint8_t *m)
{
  int months[] = { 30, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
  *m = MID(1, *m, 12);
  *d = MID(1, *d, months[*m - 1]);
}

void validate_date(rtc_date_t *date)
{
  validate_day_month(&date->day, &date->month);
  date->year = MIN(date->year, 99);
}

uint32_t validate_config(void *data)
{
  device_config_t *cfg = (device_config_t *)data;
  validate_date(&cfg->shedule_date);
  return VALIDATE_OK;
}

uint32_t validate_is_dt_exist(rtc_dt_t *dt)
{
  return dt->date.wd > 0 && dt->date.wd <= 7 &&
     dt->date.month > 0 && dt->date.month <= 12 &&
     dt->date.day > 0 && dt->date.month <= 31 &&
     dt->date.year <= 99;
}

uint32_t validate_is_same_dates(rtc_date_t *date1, rtc_date_t *date2)
{
  return date1->day == date2->day
      && date1->month == date2->month
      && date1->year == date2->year;
}

static int compare_regular_holidays(const void *a, const void *b)
{
  holiday_t *av = (holiday_t *)a;
  holiday_t *bv = (holiday_t *)b;

  // 0 is invalid - move to end
  if(!av->day) return 1;
  else if(!bv->day) return -1;

  if(av->month != bv->month) return (int)(av->month - bv->month);
  else if((av->month <= bv->month) && (av->day > bv->day)) return (int)(av->day - bv->day);

  return 0;
}

uint32_t validate_regular_holidays(void *data)
{
  holiday_t *p = (holiday_t *)data;
  uint32_t cnt = POWER_HOLIDAY_NUM;

  while(cnt--)
  {
    p->index = MID(1, p->index, POWER_TARIF_PROGRAM_NUM);
    if(!p->day || !p->month) memset(p, 0, sizeof(holiday_t));
    else validate_day_month(&p->day, &p->month);
    p++;
  }

  qsort(data, POWER_HOLIDAY_NUM, sizeof(holiday_t), compare_regular_holidays);

  return VALIDATE_OK;
}

static int compare_fixed_holidays(const void *a, const void *b)
{
  holiday_fix_t *av = (holiday_fix_t *)a;
  holiday_fix_t *bv = (holiday_fix_t *)b;

  // 0 is invalid - move to end
  if(!av->day) return 1;
  else if(!bv->day) return -1;

  else if(av->year > bv->year) return (int)(av->year - bv->year);
  else if((av->year <= bv->year) && (av->month > bv->month)) return (int)(av->month - bv->month);
  else if((av->year <= bv->year) && (av->month <= bv->month) && (av->day > bv->day)) return (int)(av->day - bv->day);

  return 0;
}

uint32_t validate_fixed_holidays(void *data)
{
  holiday_fix_t *p = (holiday_fix_t *)data;
  uint32_t cnt = POWER_HOLIDAY_FIX_NUM;

  while(cnt--)
  {
    p->index = MID(1, p->index, POWER_TARIF_PROGRAM_NUM);
    p->year = MID(0, p->year, 99);
    if(!p->day || !p->month || !p->year) memset(p, 0, sizeof(holiday_fix_t));
    else validate_day_month(&p->day, &p->month);
    p++;
  }

  qsort(data, POWER_HOLIDAY_FIX_NUM, sizeof(holiday_fix_t), compare_fixed_holidays);

  return VALIDATE_OK;
}

static int compare_seasons(const void *a, const void *b)
{
  season_t *av = (season_t *)a;
  season_t *bv = (season_t *)b;

  // 0 is invalid - move to end
  if(!av->day) return 1;
  else if(!bv->day) return -1;

  else if(av->month > bv->month) return av->month - bv->month;
  else if((av->month == bv->month) && (av->day > bv->day)) return av->day - bv->day;

  return 0;
}

uint32_t validate_seasons(void *data)
{
  season_t *p = (season_t *)data;
  uint32_t cnt = POWER_SEASON_NUM;

  while(cnt--)
  {
    for(int i = 0; i < sizeof(p->index_per_wd); i++)
      p->index_per_wd[i] = MID(1, p->index_per_wd[i], POWER_TARIF_PROGRAM_NUM);
    if(!p->day || !p->month) memset(p, 0, sizeof(season_t));
    else validate_day_month(&p->day, &p->month);
    p++;
  }

  qsort(data, POWER_SEASON_NUM, sizeof(season_t), compare_seasons);

  return VALIDATE_OK;
}

uint32_t validate_tarif_programs(void *data)
{
  tarif_program_t *p = (tarif_program_t *)data;
  uint32_t cnt = POWER_TARIF_PROGRAM_NUM;

  while(cnt--)
  {
    uint32_t f = 0;

    // check if column was filling
    f |= p->priority;
    for(int i = 0; !f && i < sizeof(p->index_per_hour); i++)
      if(p->index_per_hour[i]) f = 1;

    for(int i = 0; f && i < sizeof(p->index_per_hour); i++)
    {
      uint8_t p1 = p->index_per_hour[i] & 0x0f;
      uint8_t p2 = p->index_per_hour[i] >> 4;
      p1 = MID(1, p1, TARIF_STD);
      p2 = MID(1, p2, TARIF_STD);
      p->index_per_hour[i] = p1 | (p2 << 4);

      if(f) p->priority = MID(1, p->priority, POWER_TARIF_PROGRAM_NUM);
    }
    p++;
  }

  return VALIDATE_OK;
}

uint32_t validate_limits(void *data)
{
  limits_t *p = (limits_t *)data;

  p->net.u_max = MID(PWR_U_MIN, p->net.u_max, PWR_U_MAX);
  p->net.u_min = MID(PWR_U_MIN, p->net.u_min, PWR_U_MAX);
  p->net.u_gist = MID(1, p->net.u_gist, 100);

  p->net.df = MID(1, p->net.df, 1000);
  p->net.t_max = MID(-40, p->net.t_max, 85);

  p->net.i_max = MID(PWR_I_MIN, p->net.i_max, PWR_I_MAX);
  p->net.i_min = MID(PWR_I_MIN, p->net.i_min, PWR_I_MAX);
  p->net.i_gist = MID(1, p->net.i_gist, 100);

  p->relay.st = MID(0, p->relay.st, RELAY_MAX - 1);
  p->relay.ret = MID(0, p->relay.ret, RELAY_RET_MAX - 1);

  return VALIDATE_OK;
}

uint32_t validate_lim_shedule(void *data)
{
  return VALIDATE_OK;
}

uint32_t validate_is_regular_holiday(holiday_t *h, uint32_t cnt, rtc_date_t *date)
{
  uint32_t ret = VALIDATE_NODATA;

  while(cnt--)
  {
    if(!h->day) break;
    if(h->day == date->day && h->month == date->month)
    {
      ret = h->index;
      break;
    }
    h++;
  }

  return ret;
}

uint32_t validate_is_fixed_holiday(holiday_fix_t *h, uint32_t cnt, rtc_date_t *date)
{
  uint32_t ret = VALIDATE_NODATA;

  while(cnt--)
  {
    if(!h->day) break;
    if(h->day == date->day &&
       h->month == date->month &&
       h->year == date->year)
    {
      ret = h->index;
      break;
    }
    h++;
  }

  return ret;
}

uint32_t validate_is_season(season_t *s, uint32_t cnt, rtc_date_t *date)
{
  // no data ([0] not exist)
  if(!s[0].day)
  {
//    slog("%s: no data", __FUNCTION__);
    return VALIDATE_NODATA;
  }

  uint32_t wd = date->wd - 1;

  // if only one rule
  if(!s[1].day)
  {
//    slog("%s: only one rule", __FUNCTION__);
    return s[0].index_per_wd[wd];
  }

  int i;
  for(i = 1; i < cnt; i++)
  {
    if(!s[i].day)
    {
//      slog("%s: found end of rules in index: %d", __FUNCTION__, i);
      break;
    }

    if(DM_CMP(s[i - 1], *date) && DM_CMP(*date, s[i]))
    {
//      slog("%s: linear found: %d", __FUNCTION__, i);
      return s[i - 1].index_per_wd[wd];
    }
  }

  return s[i - 1].index_per_wd[wd];
}

