/*
 * slog.c
 *
 *  Created on: Aug 7, 2019
 *      Author: vchumakov
 */

#include <stdarg.h>
#include <stdio.h>

#include "hal/hw_defs.h"
#include "bsp.h"
#include "device.h"
#include "syslog.h"
#include "hal/hw_crc.h"
#include "hal/hw_tim.h"

#ifdef CONFIG_USE_SYSLOG

static void dbg_buf(char *buf, uint32_t size)
{
  uint16_t crc;
  uint8_t cmd = CMD_LOG_MSG;
  iovec vec[] = {
    { db.config.addr, sizeof(db.config.addr) },
    { &cmd, 1 },
    { buf, size },
    { &crc, sizeof(crc) }};
  crc = crc_calc16(vec, SIZE_OF_ARRAY(vec) - 1);

  extern io_if_t io_if_table[];
  io_if_table[HAL_RS458_IO_IF].transaction(&io_if_table[HAL_RS458_IO_IF], vec, SIZE_OF_ARRAY(vec), 0, 0);

  wait_ms(PKG_TIMEOUT_MS);
}

void slog(const char *fmt, ...)
{
  if(db.config.debug_mode == DEBUG_OFF) return;

  va_list argptr;
  int n = 0;
  char buf[DBG_BUF_SIZE];

  va_start(argptr, fmt);
  n += vsnprintf(buf, sizeof(buf), fmt, argptr);
  va_end(argptr);

  dbg_buf(buf, n + 1);
}

void slog_pkg(if_msg_t *pkg)
{
  if(db.config.debug_mode == DEBUG_OFF) return;

  char buf[DBG_BUF_SIZE];
  int n = 0;

  n += snprintf(buf + n, sizeof(buf) - n, "RX (%d): ", (int)pkg->size);

  for(int i = 0; i < pkg->size; i++)
    n += snprintf(buf + n, sizeof(buf) - n, "%02x ", pkg->data[i]);

  dbg_buf(buf, strlen(buf) + 1);
}

const char *wd_str[] = { "Mo", "Tu", "We", "Th", "Fr", "Sa", "Su" };

void slog_dt(const char *str, rtc_dt_t *dt)
{
  if(db.config.debug_mode == DEBUG_OFF) return;

  char buf[DBG_BUF_SIZE];

  const char *wd_str_ptr = 0;
  if(dt->date.wd > 0 && dt->date.wd <= SIZE_OF_ARRAY(wd_str)) wd_str_ptr = wd_str[dt->date.wd - 1];
  else wd_str_ptr = "--";

  snprintf(buf, sizeof(buf), "%s: %s %02d/%02d/%02d %02d:%02d:%02d", str, wd_str_ptr,
      dt->date.day, dt->date.month, dt->date.year,
      dt->time.hour, dt->time.min, dt->time.sec);

  dbg_buf(buf, strlen(buf) + 1);
}
#endif

