/*
 * eefs.c
 *
 *  Created on: Jul 30, 2019
 *      Author: netcat
 */

#include "hal/hw_defs.h"

#include "device.h"
#include "def/def_power.h"
#include "def/def_schedule.h"
#include "def/def_counters.h"
#include "def/def_journal.h"
#include "sys/validators.h"
#include "hal/hw_crc.h"

#include "m24m01.h"
#include "eefs.h"
#include "sys/syslog.h"

// power data
const char *eefs_pt_names[] = { EEFS_PT_STRS };
//STATIC_ASSERT(SIZE_OF_ARRAY(eefs_pt_names) == PT_MAX);

eefs_pt_t eefs_pt[] = {
    { PT_INFO, 1, sizeof(eefs_info_header_t) },
    { PT_CONFIG, 2, sizeof(device_config_t) },
    { PT_SHEDULE0, 2, sizeof(schedule_t) },
    { PT_SHEDULE1, 2, sizeof(schedule_t) },
    { PT_ENERGY, 2, sizeof(enengy_t) },
    { PT_COUNTERS, 2, sizeof(counters_t) },
    { PT_SLICE_DAY, 64, sizeof(enengy_t) }, // fixme: 128
    { PT_SLICE_MONTH, 40, sizeof(enengy_t) },
    { PT_SLICE_YEAR, 10, sizeof(enengy_t) },
    { PT_ACC, 2, sizeof(power_phase_float_t) },
    { PT_LIMITS, 2, sizeof(limits_t) },
    { PT_LIM_SHEDULE, 2, sizeof(lim_shedule_t) },

    { PT_JOURNAL_RTC, 20, sizeof(journal_t) },
    { PT_JOURNAL_RTC_SYNC, 20, sizeof(journal_t) },
    { PT_JOURNAL_RTC_SYNC_LIMIT_D, 12, sizeof(journal_t) },
    { PT_JOURNAL_RTC_SYNC_CRITICAL, 15, sizeof(journal_t) },
    { PT_JOURNAL_RTC_SYNC_LIMIT_MY, 12, sizeof(journal_t) },
    { PT_JOURNAL_RTC_SUMMER_TIME_EVT, 4, sizeof(journal_t) },
    { PT_JOURNAL_RTC_CORRECTION_UPD, 4, sizeof(journal_t) },
    { PT_JOURNAL_RTC_SUMMER_TIME_UPD, 4, sizeof(journal_t) },
    { PT_JOURNAL_ENERGY_RST, 4, sizeof(journal_t) },
    { PT_JOURNAL_PROFILE_CFG_UPD, 4, sizeof(journal_t) },
    { PT_JOURNAL_METROLOGY_UPD, 4, sizeof(journal_t) },
    { PT_JOURNAL_SHEDULE_EVT, 12, sizeof(journal_t) },
    { PT_JOURNAL_SHEDULE_UPD, 20, sizeof(journal_t) },
    { PT_JOURNAL_SHEDULE_SW, 12, sizeof(journal_t) },
    { PT_JOURNAL_PWR_LIMITS_UPD, 12, sizeof(journal_t) },
    { PT_JOURNAL_NET_LIMITS_UPD, 12, sizeof(journal_t) },
    { PT_JOURNAL_RELAY_LIMITS_UPD, 4, sizeof(journal_t) },
    { PT_JOURNAL_RELAY_LIMITS_EVT, 12, sizeof(journal_t) },
    { PT_JOURNAL_IF_UPD, 12, sizeof(journal_t) },
    { PT_JOURNAL_PWR_UP_DOWN_EVT, 20, sizeof(journal_t) },
    { PT_JOURNAL_PHASE_A_U_OVER_MIN, 12, sizeof(journal_t) },
    { PT_JOURNAL_PHASE_B_U_OVER_MIN, 12, sizeof(journal_t) },
    { PT_JOURNAL_PHASE_C_U_OVER_MIN, 12, sizeof(journal_t) },
    { PT_JOURNAL_PHASE_A_U_OVER_MAX, 12, sizeof(journal_t) },
    { PT_JOURNAL_PHASE_B_U_OVER_MAX, 12, sizeof(journal_t) },
    { PT_JOURNAL_PHASE_C_U_OVER_MAX, 12, sizeof(journal_t) },
    { PT_JOURNAL_PHASE_A_I_OVER_MAX, 12, sizeof(journal_t) },
    { PT_JOURNAL_PHASE_B_I_OVER_MAX, 12, sizeof(journal_t) },
    { PT_JOURNAL_PHASE_C_I_OVER_MAX, 12, sizeof(journal_t) },
    { PT_JOURNAL_SUM_I_OVER_MIN, 12, sizeof(journal_t) },
    { PT_JOURNAL_F_OVER_LIMIT, 12, sizeof(journal_t) },
    { PT_JOURNAL_S_OVER_MAX, 12, sizeof(journal_t) },
    { PT_JOURNAL_ENERGY_OVER_MAX, 12, sizeof(journal_t) },
    { PT_JOURNAL_BAD_PWD_LOCK_EVT, 4, sizeof(journal_t) },
    { PT_JOURNAL_BAD_PWD_EVT, 12, sizeof(journal_t) },
    { PT_JOURNAL_MAG_EVT, 12, sizeof(journal_t) },
    { PT_JOURNAL_EMAG_EVT, 12, sizeof(journal_t) },
    { PT_JOURNAL_OPEN_CONN_EVT, 12, sizeof(journal_t) },
    { PT_JOURNAL_OPEN_EVT, 4, sizeof(journal_t) },
    { PT_JOURNAL_PHASE_ERR_EVT, 12, sizeof(journal_t) },
    { PT_JOURNAL_TERM_EVT, 4, sizeof(journal_t) },

    { PT_PROFILE, 4, sizeof(profile_t) }, // fixme: 6144
};

eefs_info_header_t eefs_info_header = {
    .ver_major = EEFS_VERSION_MAJOR,
    .ver_minor = EEFS_VERSION_MINOR,
    .parts = SIZE_OF_ARRAY(eefs_pt),
};

// get pointer to the next empty block of partition
static void eefs_next_index(eefs_pt_e pt)
{
//  slog("%s", __FUNCTION__);

  eefs_pt_t *pt_header = &eefs_pt[pt];
  uint32_t index = pt_header->index;

  // need to write first block?
  // index of market always link on a last block with data.
  // on init state of EEFS it block may be empty.
  if(!index)
  {
    // empty header for comparing
    eefs_record_header_t h_empty = { 0 };
    // actual header read
    eefs_record_header_t h;
    eeprom_read(pt_header->addr + (pt_header->block_size + sizeof(eefs_record_header_t)) * pt_header->index,
        (uint8_t *)&h, sizeof(eefs_record_header_t));

    // return on empty
    if(!memcmp(&h, &h_empty, sizeof(eefs_record_header_t))) return;
  }

  // move marker of list
  index++;
  if(index >= pt_header->blocks) index = 0;
  pt_header->index = index;
}

// erasing all headers of blocks in PT of FS
void eefs_erase_pt(eefs_pt_e pt)
{
  eefs_pt_t *pt_header = &eefs_pt[pt];
  pt_header->index = 0;

  for(uint32_t block = 0; block < pt_header->blocks; block++)
  {
    eefs_record_header_t h = { 0 };
    eeprom_write(pt_header->addr + (pt_header->block_size + sizeof(eefs_record_header_t)) * block,
        (uint8_t *)&h, sizeof(eefs_record_header_t));
  }
}

// erasing all headers of blocks in FS
static void eefs_erase_all()
{
  slog("%s", __FUNCTION__);

  for(uint32_t pt = 0; pt < SIZE_OF_ARRAY(eefs_pt); pt++)
  {
    eefs_erase_pt(pt);
  }
}

void eefs_verify()
{
  slog("%s", __FUNCTION__);

  rtc_dt_t dt;
  rtc_get_dt(&dt);

  for(uint32_t pt = 0; pt < SIZE_OF_ARRAY(eefs_pt); pt++)
  {
    eefs_pt_t *pt_header = &eefs_pt[pt];

    for(uint32_t block = 0; block < pt_header->blocks; block++)
    {
      uint32_t addr = pt_header->addr + (pt_header->block_size + sizeof(eefs_record_header_t)) * block;

      // header
      eefs_record_header_t h;
      eeprom_read(addr, (uint8_t *)&h, sizeof(eefs_record_header_t));

      // if date of block in the future - erase header of block
      if(!rtc_dt_cmp(&h.dt, &dt))
      {
        eefs_record_header_t h_empty = { 0 };
        eeprom_write(addr, (uint8_t *)&h_empty, sizeof(eefs_record_header_t));

        slog("rm future block: %d in %s", block, eefs_pt_names[pt]);
      }

      // redundant: may remove bad blocks on startup (now crc cheking on read operation)
#if 0
      // data
      uint8_t buf[pt_header->block_size];
      eeprom_read(addr + sizeof(eefs_record_header_t), buf, pt_header->block_size);

      // check crc
      iovec vec = { buf, pt_header->block_size };
      uint16_t crc = crc_calc16(&vec, 1);

      // clear block on crc error
      if(h.crc != crc)
      {
        eefs_record_header_t h_empty = {};
        eeprom_write(addr, (uint8_t *)&h_empty, sizeof(eefs_record_header_t));
      }
#endif

    }
  }
}

// find last index of markers, used on init state
static uint32_t eefs_find_index(eefs_pt_e pt)
{
//  slog("%s: %s", __FUNCTION__, eefs_pt_names[pt]);

  eefs_pt_t *pt_header = &eefs_pt[pt];

  // first block
  uint32_t ret = 0;
  eefs_record_header_t ret_header = { 0 };
  eeprom_read(pt_header->addr, (uint8_t *)&ret_header, sizeof(eefs_record_header_t));

  for(uint32_t block = 1; block < pt_header->blocks; block++)
  {
    eefs_record_header_t h = { 0 };
    eeprom_read(pt_header->addr + (pt_header->block_size + sizeof(eefs_record_header_t)) * block,
        (uint8_t *)&h, sizeof(eefs_record_header_t));

    if(rtc_dt_cmp(&ret_header.dt, &h.dt))
    {
      ret = block;
      ret_header = h;
    }
    else return ret;
  }
  return ret;
}

void eefs_format()
{
  slog("%s", __FUNCTION__);
  eefs_erase_all();
  eefs_save(PT_INFO, &eefs_info_header);
}

static void eefs_init_pt_table()
{
//  slog("%s", __FUNCTION__);

  // calculate eeprom allocation table
  for(uint32_t i = 0; i < eefs_info_header.parts; i++)
  {
    eefs_pt[i].addr = eefs_info_header.size;
    eefs_pt[i].index = 0;
    eefs_info_header.size += (eefs_pt[i].block_size + sizeof(eefs_record_header_t)) * eefs_pt[i].blocks;
    eefs_info_header.blocks += eefs_pt[i].blocks;
  }

  slog("parts: %d, blocks: %d, size: %d (free: %d)",
      eefs_info_header.parts, eefs_info_header.blocks,
      eefs_info_header.size, M24M01_SIZE - eefs_info_header.size);
}

void eefs_init()
{
//  slog("%s", __FUNCTION__);

  eefs_init_pt_table();

  eefs_info_header_t stored_info;
  uint32_t ret = eefs_load(PT_INFO, &stored_info);

  if(ret || memcmp(&eefs_info_header, &stored_info, sizeof(eefs_info_header_t)))
  {
    eefs_format();
  }
  else
  {
    // if EEFS info_header ok - verify time of journals
    eefs_verify();

    // find actual
    for(uint32_t i = 0; i < stored_info.parts; i++)
    {
      eefs_pt[i].index = eefs_find_index(i);
  //    slog("(%d) %s: %d", i, eefs_pt_names[i], eefs_pt[i].index);
    }
  }
}

void eefs_save_timed(eefs_pt_e pt, void *data, rtc_dt_t *dt)
{
  eefs_pt_t *pt_header = &eefs_pt[pt];

  // shift index and get actual addr
  eefs_next_index(pt);
  uint32_t addr = pt_header->addr + (pt_header->block_size + sizeof(eefs_record_header_t)) * pt_header->index;

  // fill header
  eefs_record_header_t h = { 0 };
  if(dt) h.dt = *dt;
  else rtc_get_dt(&h.dt);

//  slog_dt(__FUNCTION__, &h.dt);
  iovec vec = { data, pt_header->block_size };
  h.crc = crc_calc16(&vec, 1);
  eeprom_write(addr, (uint8_t *)&h, sizeof(eefs_record_header_t));

  // data
  eeprom_write(addr + sizeof(eefs_record_header_t), (uint8_t *)data, pt_header->block_size);
}

void eefs_save(eefs_pt_e pt, void *data)
{
//  slog("%s: %s", __FUNCTION__, eefs_pt_names[pt]);
  eefs_save_timed(pt, data, 0);
}

static int eefs_load_index(eefs_pt_e pt, void *data, uint32_t block)
{
//  slog("%s: %s", __FUNCTION__, eefs_pt_names[pt]);

  eefs_pt_t *pt_header = &eefs_pt[pt];
  uint32_t addr = pt_header->addr + (pt_header->block_size + sizeof(eefs_record_header_t)) * block;

  // header
  eefs_record_header_t h;
  eeprom_read(addr, (uint8_t *)&h, sizeof(eefs_record_header_t));

  // data
  uint8_t buf[pt_header->block_size];
  eeprom_read(addr + sizeof(eefs_record_header_t), buf, pt_header->block_size);

  // check crc
  iovec vec = { buf, pt_header->block_size };
  uint16_t crc = crc_calc16(&vec, 1);
  if(h.crc != crc)
  {
    slog("%s %s: bad crc: %04x (%04x stored)", __FUNCTION__, eefs_pt_names[pt], crc, h.crc);
    return 1;
  }

  // load data if ok
  memcpy((uint8_t *)data, buf, sizeof(buf));
  return 0;
}

int eefs_load(eefs_pt_e pt, void *data)
{
  return eefs_load_index(pt, data, eefs_pt[pt].index);
}

// ret 0 on error or ret block size
int eefs_get_block_size(eefs_pt_e pt)
{
  if(pt >= PT_MAX) return 0;
  return eefs_pt[pt].block_size;
}

// ret > 0 - number of available blocks
// write finded block to buf
// ret 0 - if no data or no results
uint32_t eefs_find(eefs_search_t *cfg, uint8_t *buf)
{
//  slog("%s: %s", __FUNCTION__, eefs_pt_names[cfg->pt]);

  eefs_pt_t *pt_header = &eefs_pt[cfg->pt];
  uint32_t find_block_cnt = 0;
  uint32_t find_index_it = pt_header->index;

  uint32_t find_index;
  // if index = 0 - special mode - get amount of items
  if(!cfg->index) find_index = pt_header->blocks;
  else find_index = cfg->index - 1;

//  slog_dt("st", &cfg->interval.st);
//  slog_dt("end", &cfg->interval.end);

  while(1)
  {
    // calc addr
    uint32_t addr = pt_header->addr + (pt_header->block_size + sizeof(eefs_record_header_t)) * find_index_it;
    // load header
    eefs_record_header_t h;
    eeprom_read(addr, (uint8_t *)&h, sizeof(eefs_record_header_t));

    // dt is valid?
    if(!validate_is_dt_exist(&h.dt))
    {
//      slog_dt("invalid date", &h.dt);
      break;
    }

    // data between find parameters?
    if(rtc_dt_between(&cfg->interval, &h.dt))
    {
//      slog_dt("found", &h.dt);
      if(find_block_cnt == find_index)
      {
//        slog_dt("strike", &h.dt);
        if(buf)
        {
          eeprom_read(addr, buf, pt_header->block_size + sizeof(eefs_record_header_t));
        }
        return find_block_cnt;
      }
      find_block_cnt++;
    }

    // go previous
    if(!find_index_it) find_index_it = pt_header->blocks - 1;
    else find_index_it--;
    // end of round on index == marker
    if(find_index_it == pt_header->index) break;
  }

  // if no results
  if(cfg->index) return 0;
  // ret
  return find_block_cnt;
}
