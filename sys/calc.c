/*
 * calculate.c
 *
 *  Created on: Aug 6, 2019
 *      Author: vchumakov
 */

#include "hal/hw_defs.h"
#include "def/def_power.h"
#include "calc.h"

#include <math.h>

static average_data_t avg[PHASE_NUM] = { 0 };

void calc_rb_append(average_data_t *average, uint32_t cnt)
{
  for(int phase = PHASE_A; phase <= PHASE_C; phase++)
  {
    avg[phase].U = average[phase].U / cnt;
    avg[phase].I = average[phase].I / cnt;
    avg[phase].S = average[phase].S / cnt;
    avg[phase].F = average[phase].F / cnt;
    memset(&average[phase], 0, sizeof(average_data_t));
  }
}

void calc_setup(uint32_t size)
{

}

void calc_get(average_data_t *average)
{
  memcpy(average, avg, sizeof(average_data_t) * PHASE_NUM);
}

uint32_t calc_acc_to_energy(power_dir_float_t *acc, power_dir_t *storage)
{
  float *from = (float *)acc;
  uint32_t *to = (uint32_t *)storage;
  uint32_t ret = 0;

  for(uint32_t i = 0; i < sizeof(power_dir_float_t) / sizeof(float); i++)
  {
    float val = floor(*from);
    if(val > 0)
    {
      *from -= val;
      *to += (uint32_t)val;
      ret = 1;
    }
    to++;
    from++;
  }

  return ret;
}

void calc_instant_to_acc(instant_data_t *inst, power_dir_float_t *acc)
{
  float t;

  t = inst->load.S * CALC_PMETER_CONST;
  if(t > 0) acc->fwd.S += t;
  else acc->bwd.S += -t;

  t = inst->load.P * CALC_PMETER_CONST;
  if(t > 0) acc->fwd.P += t;
  else acc->bwd.P += -t;

  t = inst->load.Q * CALC_PMETER_CONST;
  if(t > 0) acc->fwd.Q += t;
  else acc->bwd.Q += -t;
}

void calc_instant_to_avg(instant_data_t *inst, average_data_t *acc)
{
  acc->U += inst->U;
  acc->I += inst->I;
  acc->S += inst->load.S;
  acc->F += inst->F;
}
