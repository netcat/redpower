/*
 * validators.h
 *
 *  Created on: Aug 1, 2019
 *      Author: netcat
 */

#ifndef SYS_VALIDATORS_H_
#define SYS_VALIDATORS_H_

#include <stdint.h>

#define VALIDATE_OK 0
#define VALIDATE_NODATA 0

#define D_CMP(a, b) ((a).day < (b).day)
#define M_CMP(a, b) ((a).month < (b).month)
#define M_SAME(a, b) ((a).month == (b).month)
#define Y_CMP(a, b) ((a).year < (b).year)
#define Y_SAME(a, b) ((a).year == (b).year)

#define DM_CMP(a, b) (M_CMP(a, b) || (M_SAME(a, b) && D_CMP(a, b)))
#define DMY_CMP(a, b) (Y_CMP(a, b) || (Y_SAME(a, b) && M_CMP(a, b)) || (M_SAME(a, b) && D_CMP(a, b)))

uint32_t validate_is_regular_holiday(holiday_t *h, uint32_t cnt, rtc_date_t *date);
uint32_t validate_is_fixed_holiday(holiday_fix_t *h, uint32_t cnt, rtc_date_t *date);
uint32_t validate_is_season(season_t *s, uint32_t cnt, rtc_date_t *date);
uint32_t validate_is_dt_exist(rtc_dt_t *dt);
uint32_t validate_is_same_dates(rtc_date_t *date1, rtc_date_t *date2);

void validate_date(rtc_date_t *date);

uint32_t validate_config(void *data);
uint32_t validate_seasons(void *data);
uint32_t validate_limits(void *data);
uint32_t validate_regular_holidays(void *data);
uint32_t validate_fixed_holidays(void *data);
uint32_t validate_tarif_programs(void *data);
uint32_t validate_lim_shedule(void *data);

#endif /* SYS_VALIDATORS_H_ */
