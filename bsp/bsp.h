/*
 * bsp.h
 *
 *  Created on: Apr 12, 2019
 *      Author: vchumakov
 */

#ifndef BSP_H_
#define BSP_H_

#include "hal/hw_defs.h"


void hal_pkg_tx(const uint8_t *data, uint32_t size);
uint32_t hal_pkg_rx(if_pkg_t *pkg);
void hal_pkg_enable_rx(void);
void bsp_init(void);

#define LEDS_PORT GPIOH
#define LED_GREEN_PIN 11
#define LED_YELLOW_PIN 12

// if change: fix clk enable & IRQ handler
#define HAL_RS485_TIM TIM2

#endif /* BSP_H_ */
