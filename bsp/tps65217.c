/*
 * tps65217.c
 *
 *  Created on: Dec 27, 2018
 *      Author: netcat
 */

#include <stdlib.h>
#include <stdint.h>

#include "tps65217.h"
#include "hal/hw_i2c.h"
#include "bsp.h"

static uint32_t pwr_reg_write(uint32_t reg, uint32_t val, uint32_t access_level)
{
  uint32_t ret = 0;
  uint8_t pwd = reg ^ 0x7D;

  if(access_level == 0)
  {
    ret = i2c_write(I2C_PWR_ADDR, reg, 1, (uint8_t *)&val, 1);
  }
  else if(access_level == 1)
  {
    ret = i2c_write(I2C_PWR_ADDR, TPS65217_REG_PASSWORD, 1, &pwd, 1);
    if(ret) return ret;
    ret = i2c_write(I2C_PWR_ADDR, reg, 1, (uint8_t *)&val, 1);
  }
  else if(access_level == 2)
  {
    ret = i2c_write(I2C_PWR_ADDR, TPS65217_REG_PASSWORD, 1, &pwd, 1);
    if(ret) return ret;
    ret = i2c_write(I2C_PWR_ADDR, reg, 1, (uint8_t *)&val, 1);
    if(ret) return ret;
    ret = i2c_write(I2C_PWR_ADDR, TPS65217_REG_PASSWORD, 1, &pwd, 1);
    if(ret) return ret;
    ret = i2c_write(I2C_PWR_ADDR, reg, 1, (uint8_t *)&val, 1);
  }

  return ret;
}

uint8_t pwr_id()
{
  uint8_t id;
  if(!i2c_read(I2C_PWR_ADDR, TPS65217_REG_CHIPID, 1, &id, sizeof(id)))
  {
    return id & TPS65217_CHIP_ID;
  }
  return 0;
}

void pwr_init()
{
  // Power conf
  pwr_reg_write(TPS65217_REG_PPATH, 0x3f, 0); // USB = 1800mA

  // LDO conf
  pwr_reg_write(TPS65217_REG_DEFLS1, 0x2f, 2); // set LDO3 = 2.5v
  pwr_reg_write(TPS65217_REG_DEFLS2, 0x20, 2); // set LDO2 = 1.5v
  pwr_reg_write(TPS65217_REG_DEFLDO1, 0x0f, 2); // set LDO1 = 3.3v
  pwr_reg_write(TPS65217_REG_DEFLDO2, 0x38, 2); // set LDO2 = 3.3v

  // set up sequencer
  pwr_reg_write(TPS65217_REG_SEQ1, 0x10, 1); // dis DC2, en DC1 on strobe 1
  pwr_reg_write(TPS65217_REG_SEQ2, 0x10, 1); // dis LDO1, en DC3 on strobe 1
  pwr_reg_write(TPS65217_REG_SEQ3, 0x00, 1); // dis LDO2 & LDO3
  pwr_reg_write(TPS65217_REG_SEQ4, 0x00, 1); // dis LDO4

  pwr_reg_write(TPS65217_REG_ENABLE, 0x1d, 1); // en DC1 & DC3 & LDO2 (pcb_v2.0: i2c resistor issue on DC3 & LDO2)
  pwr_reg_write(TPS65217_REG_DEFDCDC1, 0x37, 2); // set DC1 = 3.3v
  pwr_reg_write(TPS65217_REG_DEFDCDC2, 0x3f, 2); // set DC2 = ADJ
  pwr_reg_write(TPS65217_REG_DEFDCDC3, 0x38, 2); // set DC3 = 3.3v
  pwr_reg_write(TPS65217_REG_DEFSLEW, 0x86, 2); // apply config of DC-DC (GO)
}

