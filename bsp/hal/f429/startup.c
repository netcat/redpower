#include <stdlib.h>
#include <stdint.h>

extern unsigned int _sidata, _sdata, _edata;
extern unsigned int _sbss, _ebss;
extern unsigned int _estack;

#define weak __attribute__ ((weak))

void weak Reset_Handler(void);
void weak NMI_Handler(void);
void weak HardFault_Handler(void);
void weak MemManage_Handler(void);
void weak BusFault_Handler(void);
void weak UsageFault_Handler(void);
void weak SVC_Handler(void);
void weak DebugMon_Handler(void);
void weak PendSV_Handler(void);
void weak SysTick_Handler(void);
void weak WWDG_IRQHandler(void);
void weak PVD_IRQHandler(void);
void weak TAMP_STAMP_IRQHandler(void);
void weak RTC_WKUP_IRQHandler(void);
void weak FLASH_IRQHandler(void);
void weak RCC_IRQHandler(void);
void weak EXTI0_IRQHandler(void);
void weak EXTI1_IRQHandler(void);
void weak EXTI2_IRQHandler(void);
void weak EXTI3_IRQHandler(void);
void weak EXTI4_IRQHandler(void);
void weak DMA1_Stream0_IRQHandler(void);
void weak DMA1_Stream1_IRQHandler(void);
void weak DMA1_Stream2_IRQHandler(void);
void weak DMA1_Stream3_IRQHandler(void);
void weak DMA1_Stream4_IRQHandler(void);
void weak DMA1_Stream5_IRQHandler(void);
void weak DMA1_Stream6_IRQHandler(void);
void weak ADC_IRQHandler(void);
void weak CAN1_TX_IRQHandler(void);
void weak CAN1_RX0_IRQHandler(void);
void weak CAN1_RX1_IRQHandler(void);
void weak CAN1_SCE_IRQHandler(void);
void weak EXTI9_5_IRQHandler(void);
void weak TIM1_BRK_TIM9_IRQHandler(void);
void weak TIM1_UP_TIM10_IRQHandler(void);
void weak TIM1_TRG_COM_TIM11_IRQHandler(void);
void weak TIM1_CC_IRQHandler(void);
void weak TIM2_IRQHandler(void);
void weak TIM3_IRQHandler(void);
void weak TIM4_IRQHandler(void);
void weak I2C1_EV_IRQHandler(void);
void weak I2C1_ER_IRQHandler(void);
void weak I2C2_EV_IRQHandler(void);
void weak I2C2_ER_IRQHandler(void);
void weak SPI1_IRQHandler(void);
void weak SPI2_IRQHandler(void);
void weak USART1_IRQHandler(void);
void weak USART2_IRQHandler(void);
void weak USART3_IRQHandler(void);
void weak EXTI15_10_IRQHandler(void);
void weak RTC_Alarm_IRQHandler(void);
void weak OTG_FS_WKUP_IRQHandler(void);
void weak TIM8_BRK_TIM12_IRQHandler(void);
void weak TIM8_UP_TIM13_IRQHandler(void);
void weak TIM8_TRG_COM_TIM14_IRQHandler(void);
void weak TIM8_CC_IRQHandler(void);
void weak DMA1_Stream7_IRQHandler(void);
void weak FMC_IRQHandler(void);
void weak SDIO_IRQHandler(void);
void weak TIM5_IRQHandler(void);
void weak SPI3_IRQHandler(void);
void weak UART4_IRQHandler(void);
void weak UART5_IRQHandler(void);
void weak TIM6_DAC_IRQHandler(void);
void weak TIM7_IRQHandler(void);
void weak DMA2_Stream0_IRQHandler(void);
void weak DMA2_Stream1_IRQHandler(void);
void weak DMA2_Stream2_IRQHandler(void);
void weak DMA2_Stream3_IRQHandler(void);
void weak DMA2_Stream4_IRQHandler(void);
void weak ETH_IRQHandler(void);
void weak ETH_WKUP_IRQHandler(void);
void weak CAN2_TX_IRQHandler(void);
void weak CAN2_RX0_IRQHandler(void);
void weak CAN2_RX1_IRQHandler(void);
void weak CAN2_SCE_IRQHandler(void);
void weak OTG_FS_IRQHandler(void);
void weak DMA2_Stream5_IRQHandler(void);
void weak DMA2_Stream6_IRQHandler(void);
void weak DMA2_Stream7_IRQHandler(void);
void weak USART6_IRQHandler(void);
void weak I2C3_EV_IRQHandler(void);
void weak I2C3_ER_IRQHandler(void);
void weak OTG_HS_EP1_OUT_IRQHandler(void);
void weak OTG_HS_EP1_IN_IRQHandler(void);
void weak OTG_HS_WKUP_IRQHandler(void);
void weak OTG_HS_IRQHandler(void);
void weak DCMI_IRQHandler(void);
void weak CRYP_IRQHandler(void);
void weak HASH_RNG_IRQHandler(void);
void weak FPU_IRQHandler(void);
void weak UART7_IRQHandler(void);
void weak UART8_IRQHandler(void);
void weak SPI4_IRQHandler(void);
void weak SPI5_IRQHandler(void);
void weak SPI6_IRQHandler(void);
void weak SAI1_IRQHandler(void);
void weak LTDC_IRQHandler(void);
void weak LTDC_ER_IRQHandler(void);
void weak DMA2D_IRQHandler(void);

int main(void);

void Default_Reset_Handler()
{
	uint32_t *src = (uint32_t*)&_sidata;
	uint32_t *dst = (uint32_t*)&_sdata;
	while(dst < (uint32_t*)&_edata) *dst++ = *src++;

	dst = (uint32_t*)&_sbss;
	while(dst < (uint32_t*)&_ebss) *dst++ = 0;
	
	main();
}

void Default_Handler()
{
  while(1);
}

__attribute__((section(".isr_vector")))
void (* const vector_table[])(void) = {
  // core
  (void *)&_estack,
  Reset_Handler,
  NMI_Handler,
  HardFault_Handler,
  MemManage_Handler,
  BusFault_Handler,
  UsageFault_Handler,
  0,
  0,
  0,
  0,
  SVC_Handler,
  DebugMon_Handler,
  0,
  PendSV_Handler,
  SysTick_Handler,

  // external
  WWDG_IRQHandler,
  PVD_IRQHandler,
  TAMP_STAMP_IRQHandler,
  RTC_WKUP_IRQHandler,
  FLASH_IRQHandler,
  RCC_IRQHandler,
  EXTI0_IRQHandler,
  EXTI1_IRQHandler,
  EXTI2_IRQHandler,
  EXTI3_IRQHandler,
  EXTI4_IRQHandler,
  DMA1_Stream0_IRQHandler,
  DMA1_Stream1_IRQHandler,
  DMA1_Stream2_IRQHandler,
  DMA1_Stream3_IRQHandler,
  DMA1_Stream4_IRQHandler,
  DMA1_Stream5_IRQHandler,
  DMA1_Stream6_IRQHandler,
  ADC_IRQHandler,
  CAN1_TX_IRQHandler,
  CAN1_RX0_IRQHandler,
  CAN1_RX1_IRQHandler,
  CAN1_SCE_IRQHandler,
  EXTI9_5_IRQHandler,
  TIM1_BRK_TIM9_IRQHandler,
  TIM1_UP_TIM10_IRQHandler,
  TIM1_TRG_COM_TIM11_IRQHandler,
  TIM1_CC_IRQHandler,
  TIM2_IRQHandler,
  TIM3_IRQHandler,
  TIM4_IRQHandler,
  I2C1_EV_IRQHandler,
  I2C1_ER_IRQHandler,
  I2C2_EV_IRQHandler,
  I2C2_ER_IRQHandler,
  SPI1_IRQHandler,
  SPI2_IRQHandler,
  USART1_IRQHandler,
  USART2_IRQHandler,
  USART3_IRQHandler,
  EXTI15_10_IRQHandler,
  RTC_Alarm_IRQHandler,
  OTG_FS_WKUP_IRQHandler,
  TIM8_BRK_TIM12_IRQHandler,
  TIM8_UP_TIM13_IRQHandler,
  TIM8_TRG_COM_TIM14_IRQHandler,
  TIM8_CC_IRQHandler,
  DMA1_Stream7_IRQHandler,
  FMC_IRQHandler,
  SDIO_IRQHandler,
  TIM5_IRQHandler,
  SPI3_IRQHandler,
  UART4_IRQHandler,
  UART5_IRQHandler,
  TIM6_DAC_IRQHandler,
  TIM7_IRQHandler,
  DMA2_Stream0_IRQHandler,
  DMA2_Stream1_IRQHandler,
  DMA2_Stream2_IRQHandler,
  DMA2_Stream3_IRQHandler,
  DMA2_Stream4_IRQHandler,
  ETH_IRQHandler,
  ETH_WKUP_IRQHandler,
  CAN2_TX_IRQHandler,
  CAN2_RX0_IRQHandler,
  CAN2_RX1_IRQHandler,
  CAN2_SCE_IRQHandler,
  OTG_FS_IRQHandler,
  DMA2_Stream5_IRQHandler,
  DMA2_Stream6_IRQHandler,
  DMA2_Stream7_IRQHandler,
  USART6_IRQHandler,
  I2C3_EV_IRQHandler,
  I2C3_ER_IRQHandler,
  OTG_HS_EP1_OUT_IRQHandler,
  OTG_HS_EP1_IN_IRQHandler,
  OTG_HS_WKUP_IRQHandler,
  OTG_HS_IRQHandler,
  DCMI_IRQHandler,
  CRYP_IRQHandler,
  HASH_RNG_IRQHandler,
  FPU_IRQHandler,
  UART7_IRQHandler,
  UART8_IRQHandler,
  SPI4_IRQHandler,
  SPI5_IRQHandler,
  SPI6_IRQHandler,
  SAI1_IRQHandler,
  LTDC_IRQHandler,
  LTDC_ER_IRQHandler,
  DMA2D_IRQHandler,
};

#pragma weak Reset_Handler = Default_Reset_Handler
#pragma weak NMI_Handler = Default_Handler
#pragma weak HardFault_Handler = Default_Handler
#pragma weak MemManage_Handler = Default_Handler
#pragma weak BusFault_Handler = Default_Handler
#pragma weak UsageFault_Handler = Default_Handler
#pragma weak SVC_Handler = Default_Handler
#pragma weak DebugMon_Handler = Default_Handler
#pragma weak PendSV_Handler = Default_Handler
#pragma weak SysTick_Handler = Default_Handler
#pragma weak WWDG_IRQHandler = Default_Handler
#pragma weak PVD_IRQHandler = Default_Handler
#pragma weak TAMP_STAMP_IRQHandler = Default_Handler
#pragma weak RTC_WKUP_IRQHandler = Default_Handler
#pragma weak FLASH_IRQHandler = Default_Handler
#pragma weak RCC_IRQHandler = Default_Handler
#pragma weak EXTI0_IRQHandler = Default_Handler
#pragma weak EXTI1_IRQHandler = Default_Handler
#pragma weak EXTI2_IRQHandler = Default_Handler
#pragma weak EXTI3_IRQHandler = Default_Handler
#pragma weak EXTI4_IRQHandler = Default_Handler
#pragma weak DMA1_Stream0_IRQHandler = Default_Handler
#pragma weak DMA1_Stream1_IRQHandler = Default_Handler
#pragma weak DMA1_Stream2_IRQHandler = Default_Handler
#pragma weak DMA1_Stream3_IRQHandler = Default_Handler
#pragma weak DMA1_Stream4_IRQHandler = Default_Handler
#pragma weak DMA1_Stream5_IRQHandler = Default_Handler
#pragma weak DMA1_Stream6_IRQHandler = Default_Handler
#pragma weak ADC_IRQHandler = Default_Handler
#pragma weak CAN1_TX_IRQHandler = Default_Handler
#pragma weak CAN1_RX0_IRQHandler = Default_Handler
#pragma weak CAN1_RX1_IRQHandler = Default_Handler
#pragma weak CAN1_SCE_IRQHandler = Default_Handler
#pragma weak EXTI9_5_IRQHandler = Default_Handler
#pragma weak TIM1_BRK_TIM9_IRQHandler = Default_Handler
#pragma weak TIM1_UP_TIM10_IRQHandler = Default_Handler
#pragma weak TIM1_TRG_COM_TIM11_IRQHandler = Default_Handler
#pragma weak TIM1_CC_IRQHandler = Default_Handler
#pragma weak TIM2_IRQHandler = Default_Handler
#pragma weak TIM3_IRQHandler = Default_Handler
#pragma weak TIM4_IRQHandler = Default_Handler
#pragma weak I2C1_EV_IRQHandler = Default_Handler
#pragma weak I2C1_ER_IRQHandler = Default_Handler
#pragma weak I2C2_EV_IRQHandler = Default_Handler
#pragma weak I2C2_ER_IRQHandler = Default_Handler
#pragma weak SPI1_IRQHandler = Default_Handler
#pragma weak SPI2_IRQHandler = Default_Handler
#pragma weak USART1_IRQHandler = Default_Handler
#pragma weak USART2_IRQHandler = Default_Handler
#pragma weak USART3_IRQHandler = Default_Handler
#pragma weak EXTI15_10_IRQHandler = Default_Handler
#pragma weak RTC_Alarm_IRQHandler = Default_Handler
#pragma weak OTG_FS_WKUP_IRQHandler = Default_Handler
#pragma weak TIM8_BRK_TIM12_IRQHandler = Default_Handler
#pragma weak TIM8_UP_TIM13_IRQHandler = Default_Handler
#pragma weak TIM8_TRG_COM_TIM14_IRQHandler = Default_Handler
#pragma weak TIM8_CC_IRQHandler = Default_Handler
#pragma weak DMA1_Stream7_IRQHandler = Default_Handler
#pragma weak FMC_IRQHandler = Default_Handler
#pragma weak SDIO_IRQHandler = Default_Handler
#pragma weak TIM5_IRQHandler = Default_Handler
#pragma weak SPI3_IRQHandler = Default_Handler
#pragma weak UART4_IRQHandler = Default_Handler
#pragma weak UART5_IRQHandler = Default_Handler
#pragma weak TIM6_DAC_IRQHandler = Default_Handler
#pragma weak TIM7_IRQHandler = Default_Handler
#pragma weak DMA2_Stream0_IRQHandler = Default_Handler
#pragma weak DMA2_Stream1_IRQHandler = Default_Handler
#pragma weak DMA2_Stream2_IRQHandler = Default_Handler
#pragma weak DMA2_Stream3_IRQHandler = Default_Handler
#pragma weak DMA2_Stream4_IRQHandler = Default_Handler
#pragma weak ETH_IRQHandler = Default_Handler
#pragma weak ETH_WKUP_IRQHandler = Default_Handler
#pragma weak CAN2_TX_IRQHandler = Default_Handler
#pragma weak CAN2_RX0_IRQHandler = Default_Handler
#pragma weak CAN2_RX1_IRQHandler = Default_Handler
#pragma weak CAN2_SCE_IRQHandler = Default_Handler
#pragma weak OTG_FS_IRQHandler = Default_Handler
#pragma weak DMA2_Stream5_IRQHandler = Default_Handler
#pragma weak DMA2_Stream6_IRQHandler = Default_Handler
#pragma weak DMA2_Stream7_IRQHandler = Default_Handler
#pragma weak USART6_IRQHandler = Default_Handler
#pragma weak I2C3_EV_IRQHandler = Default_Handler
#pragma weak I2C3_ER_IRQHandler = Default_Handler
#pragma weak OTG_HS_EP1_OUT_IRQHandler = Default_Handler
#pragma weak OTG_HS_EP1_IN_IRQHandler = Default_Handler
#pragma weak OTG_HS_WKUP_IRQHandler = Default_Handler
#pragma weak OTG_HS_IRQHandler = Default_Handler
#pragma weak DCMI_IRQHandler = Default_Handler
#pragma weak CRYP_IRQHandler = Default_Handler
#pragma weak HASH_RNG_IRQHandler = Default_Handler
#pragma weak FPU_IRQHandler = Default_Handler
#pragma weak UART7_IRQHandler = Default_Handler
#pragma weak UART8_IRQHandler = Default_Handler
#pragma weak SPI4_IRQHandler = Default_Handler
#pragma weak SPI5_IRQHandler = Default_Handler
#pragma weak SPI6_IRQHandler = Default_Handler
#pragma weak SAI1_IRQHandler = Default_Handler
#pragma weak LTDC_IRQHandler = Default_Handler
#pragma weak LTDC_ER_IRQHandler = Default_Handler
#pragma weak DMA2D_IRQHandler = Default_Handler
