/*
 * hw_uart.h
 *
 *  Created on: Feb 8, 2017
 *      Author: netcat
 */

#ifndef HW_UART_H_
#define HW_UART_H_

#include "hw_defs.h"

#define UART1_BAUDRATE 115200

void uart1_init(void);
void uart_out(USART_TypeDef *uart, uint8_t data);
uint32_t uart_in(USART_TypeDef *uart, uint8_t *c);

#endif /* HW_UART_H_ */
