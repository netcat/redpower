/*
 * hw_rcc.c
 *
 *  Created on: Feb 7, 2017
 *      Author: netcat
 */

#include <stdlib.h>
#include <stdint.h>
#include "hal/hw_rcc.h"

void set_sys_clock()
{
  // enable HSE
  RCC->CR |= RCC_CR_HSEON;
  while(!(RCC->CR & RCC_CR_HSERDY));

  // select regulator voltage output Scale 1 mode
  RCC->APB1ENR |= RCC_APB1ENR_PWREN;
  PWR->CR |= PWR_CR_VOS;

  uint32_t t = RCC->CFGR;
  t &= ~(RCC_CFGR_HPRE | RCC_CFGR_PPRE1 | RCC_CFGR_PPRE2);
  t |= RCC_CFGR_PPRE1_DIV4 | RCC_CFGR_PPRE2_DIV2;
  RCC->CFGR = t;

  RCC->PLLCFGR = PLL_M | (PLL_N << 6) | (((PLL_P >> 1) - 1) << 16) | RCC_PLLCFGR_PLLSRC_HSE | (PLL_Q << 24);

  // enable the main PLL
  RCC->CR |= RCC_CR_PLLON;
  while(!(RCC->CR & RCC_CR_PLLRDY));

  // enable the over-drive to extend the clock frequency to 180 MHz
  PWR->CR |= PWR_CR_ODEN;
  while(!(PWR->CSR & PWR_CSR_ODRDY));
  PWR->CR |= PWR_CR_ODSWEN;
  while(!(PWR->CSR & PWR_CSR_ODSWRDY));

  // configure flash prefetch, instruction cache, data cache and wait state
  FLASH->ACR = FLASH_ACR_ICEN | FLASH_ACR_DCEN | FLASH_ACR_PRFTEN | FLASH_ACR_LATENCY_5WS;

  // select the main PLL as system clock source
  RCC->CFGR &= ~RCC_CFGR_SW;
  RCC->CFGR |= RCC_CFGR_SW_PLL | RCC_CFGR_PPRE1_DIV4 | RCC_CFGR_PPRE2_DIV2;
  while((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL);
}

