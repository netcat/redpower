/*
 * hw_tim.c
 *
 *  Created on: Jul 5, 2019
 *      Author: vchumakov
 */

#include "hal/hw_tim.h"

void tim_init(TIM_TypeDef *tim, uint32_t timeout)
{
	// 8mhz / 8000 = 1khz
	tim->PSC = 8000 - 1; // prescaler
	tim->ARR = timeout; // ms
	tim->DIER |= TIM_DIER_UIE; // allow interrupt
	tim_reset(tim);
	tim_stop(tim);

	tim->EGR |= TIM_EGR_UG;
	tim->SR = ~TIM_SR_UIF;
}

void tim_start(TIM_TypeDef *tim)
{
  tim->CNT = 0; // reset counter
  tim->CR1 |= TIM_CR1_CEN; // start
}

void tim_reset(TIM_TypeDef *tim)
{
  tim->CNT = 0; // reset counter
}

void tim_stop(TIM_TypeDef *tim)
{
  tim->CR1 &= ~TIM_CR1_CEN; // stop
}


