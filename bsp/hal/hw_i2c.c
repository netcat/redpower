#include <stdlib.h>
#include <stdint.h>
#include "hal/hw_gpio.h"
#include "hal/hw_i2c.h"
#include "hal/hw_rcc.h"

static void i2c_cfg(I2C_TypeDef* i2c, uint32_t clk)
{
  uint32_t t;

  // set freq
  uint32_t freq_range = RCC_APB1_CLK_HZ / 1000000;
  i2c->CR2 = freq_range;

  // disable
  i2c->CR1 &= ~I2C_CR1_PE;

  if(clk <= 100000)
  {
    t = RCC_APB1_CLK_HZ / (clk << 1);
    if(t < 0x04) t = 0x04;
    i2c->TRISE = freq_range + 1;
  }
  else
  {
    t = RCC_APB1_CLK_HZ / (clk * 3);
    if(!(t & I2C_CCR_CCR)) t = 1;
    t |= I2C_CCR_FS;
    i2c->TRISE = ((freq_range * 300) / 1000) + 1;
  }
  i2c->CCR = t;

  // enable, ack enable
  i2c->CR1 |= I2C_CR1_PE | 0x0400;
  // ack addr 7bit
  i2c->OAR1 = 0x4000;
}

void i2c1_init()
{
  // gpio cfg
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;
  
  gpio_set_af(GPIOB, 6, GPIO_AF_I2C1);
  gpio_init_ext(GPIOB, 6, GPIO_MODE_AF, GPIO_OTYPE_OD, GPIO_SPEED_100MHZ, GPIO_PUPD_NOPULL);
  gpio_set_af(GPIOB, 7, GPIO_AF_I2C1);
  gpio_init_ext(GPIOB, 7, GPIO_MODE_AF, GPIO_OTYPE_OD, GPIO_SPEED_100MHZ, GPIO_PUPD_NOPULL);

  // clk en
  RCC->APB1ENR |= RCC_APB1ENR_I2C1EN;

  i2c_cfg(I2C1, 100000);
}

#define i2c_event(I2Cx, I2C_EVENT) (((I2Cx->SR1 | (I2Cx->SR2 << 16)) & I2C_EVENT) == I2C_EVENT)
#define i2c_timeout(x) timeout_cnt = 0xffff; while(x) { if (timeout_cnt-- == 0) goto ret; }
#define i2c_flag(I2Cx, I2C_FLAG) ((I2C_FLAG) & 0x10000000) ? (I2Cx->SR1 & (I2C_FLAG)) : (I2Cx->SR2 & ((I2C_FLAG) >> 16))

uint32_t i2c_read(uint8_t addr, uint32_t reg, uint8_t reg_addr_size, uint8_t *buf, uint32_t len)
{
  uint32_t timeout_cnt = 0;
  I2C_TypeDef *I2Cx = I2C1;

  i2c_timeout(i2c_flag(I2Cx, I2C_FLAG_BUSY));

  // start
  I2Cx->CR1 |= I2C_CR1_START;
  i2c_timeout(!i2c_event(I2Cx, I2C_EVENT_MASTER_MODE_SELECT));

  // addr
  I2Cx->DR = addr & 0xfe;
  i2c_timeout(!i2c_event(I2Cx, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));

  // mem addr
  if(reg_addr_size == 4)
  {
    I2Cx->DR = reg >> 24;
    i2c_timeout(!i2c_event(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
    I2Cx->DR = reg >> 16;
    i2c_timeout(!i2c_event(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
    I2Cx->DR = reg >> 8;
    i2c_timeout(!i2c_event(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
  }
  else if(reg_addr_size == 2)
  {
    I2Cx->DR = reg >> 8;
    i2c_timeout(!i2c_event(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
  }
  I2Cx->DR = reg;
  i2c_timeout(!i2c_event(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED));

  // restart
  I2Cx->CR1 |= I2C_CR1_START;
  i2c_timeout(!i2c_event(I2Cx, I2C_EVENT_MASTER_MODE_SELECT));

  // send Address
  I2Cx->DR = addr | 0x01;
  i2c_timeout(!i2c_event(I2Cx, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));

  // recv data
  I2Cx->CR1 |= I2C_CR1_ACK;
  while(len--)
  {
    i2c_timeout(!i2c_event(I2Cx, I2C_EVENT_MASTER_BYTE_RECEIVED));
    *buf++ = I2Cx->DR;
  }
  I2Cx->CR1 &= ~I2C_CR1_ACK;

  // wait for stop
  I2Cx->CR1 |= I2C_CR1_STOP;
  i2c_timeout(i2c_flag(I2Cx, I2C_FLAG_STOPF));
  return 0;

ret:
  I2Cx->CR1 |= I2C_CR1_STOP;
  i2c_timeout(i2c_flag(I2Cx, I2C_FLAG_STOPF));
  return 1;
}

uint8_t i2c_write(uint8_t addr, uint32_t reg, uint8_t reg_addr_size, const uint8_t *buf, uint8_t len)
{
  uint32_t timeout_cnt = 0;
  I2C_TypeDef *I2Cx = I2C1;

  i2c_timeout(i2c_flag(I2Cx, I2C_FLAG_BUSY));

  // start
  I2Cx->CR1 |= I2C_CR1_START;
  i2c_timeout(!i2c_event(I2Cx, I2C_EVENT_MASTER_MODE_SELECT));

  // dev addr
  I2Cx->DR = addr & 0xfe;
  i2c_timeout(!i2c_event(I2Cx, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));

  // mem addr
  if(reg_addr_size == 4)
  {
    I2Cx->DR = reg >> 24;
    i2c_timeout(!i2c_event(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
    I2Cx->DR = reg >> 16;
    i2c_timeout(!i2c_event(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
    I2Cx->DR = reg >> 8;
    i2c_timeout(!i2c_event(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
  }
  else if(reg_addr_size == 2)
  {
    I2Cx->DR = reg >> 8;
    i2c_timeout(!i2c_event(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
  }
  I2Cx->DR = reg;
  i2c_timeout(!i2c_event(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED));

  // send data
  while(len--)
  {
    I2Cx->DR = *buf++;
    i2c_timeout(!i2c_flag(I2Cx, I2C_FLAG_BTF));
  }

  I2Cx->CR1 |= I2C_CR1_STOP;
  i2c_timeout(i2c_flag(I2Cx, I2C_FLAG_STOPF));
  return 0;

ret:
  I2Cx->CR1 |= I2C_CR1_STOP;
  i2c_timeout(i2c_flag(I2Cx, I2C_FLAG_STOPF));
  return 1;
}
