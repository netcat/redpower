/*
 * hw_uart.c
 *
 *  Created on: Feb 8, 2017
 *      Author: netcat
 */

#include "hal/hw_gpio.h"
#include "hal/hw_rcc.h"
#include "hal/hw_uart.h"

const gpio_init_t gpio_uart1_init[] = {
  // USART1
  { GPIOA, 9 }, // UART_EXT_TX
  { GPIOA, 10 }, // UART_EXT_RX
  {}};

static void usart_config(USART_TypeDef *uart, uint32_t baudrate)
{
  // cfg
  uart->CR1 = USART_CR1_TE | USART_CR1_RE;
  uart->CR2 = 0;
  uart->CR3 = 0;

  // rx ie
  uart->CR1 |= USART_CR1_RXNEIE;

  // baud
  uint32_t int_div, frac_div, t;
  int_div = (25 * RCC_APB1_CLK_HZ) / (baudrate << 1);
  t = (int_div / 100) << 4;
  frac_div = int_div - 100 * (t >> 4);
  t |= ((frac_div * 16 + 50) / 100) & 0x0f;
  uart->BRR = t;

  NVIC_EnableIRQ(USART1_IRQn);

  // en
  uart->CR1 |= USART_CR1_UE;
}

void uart1_init()
{
  // gpio cfg
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
  gpio_init_af(gpio_uart1_init, GPIO_AF_USART1);
  // clk en
  RCC->APB2ENR |= RCC_APB2ENR_USART1EN;

  usart_config(USART1, UART1_BAUDRATE);
}

void uart_out(USART_TypeDef *uart, uint8_t data)
{
  while(!(uart->SR & USART_SR_TXE));
  uart->DR = data;
}

uint32_t uart_in(USART_TypeDef *uart, uint8_t *c)
{
  uint32_t ret = uart->SR & USART_SR_RXNE;
  if(ret)
  {
    uart->SR &= ~USART_SR_RXNE;
    *c = uart->DR;
  }
  return ret;
}

