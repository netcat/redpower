/*
 * hw_rcc.h
 *
 *  Created on: Feb 7, 2017
 *      Author: netcat
 */

#ifndef HW_RCC_H
#define HW_RCC_H

#include "hw_defs.h"

#define HSE_HZ 8000000
#define RCC_APB1_CLK_HZ 42000000
#define RCC_APB2_CLK_HZ 84000000

enum
{
  PLL_M = 8,
  PLL_N = 336,
  PLL_P = 2,   // SYSCLK
  PLL_Q = 7    // USB_FS, SDIO, RNG
};

#define PLL_Q_FREQ (((HSE_HZ / PLL_M) * PLL_N) / PLL_Q)

void set_sys_clock(void);

#endif
