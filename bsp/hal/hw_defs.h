/*
 * hw_uart.h
 *
 *  Created on: Apr 8, 2017
 *      Author: netcat
 */

#ifndef HW_DEFS_H_
#define HW_DEFS_H_

#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "stm32f429xx.h"

#define SIZE_OF_ARRAY(a) (sizeof(a) / sizeof(a[0]))
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))

typedef struct
{
  uint8_t *data; // pointer to data
  uint32_t len; // length of data
} iovec;

#endif /* HW_DEFS_H_ */
