#include <stdlib.h>
#include <stdint.h>

#include "hal/hw_gpio.h"
#include "hal/hw_rcc.h"
#include "hal/hw_uart.h"
#include "hal/hw_i2c.h"
#include "hal/hw_spi.h"
#include "hal/hw_tim.h"

#include "sys/device.h"
#include "bsp.h"

#define IF_BUF_SIZE 512

typedef struct
{
  uint32_t cnt;
  uint32_t flag;
  uint8_t data[IF_BUF_SIZE];
} buf_t;

static buf_t buf_rs485;

void USART1_IRQHandler(void)
{
  if(USART1->SR & USART_SR_RXNE)
  {
    USART1->SR &= ~USART_SR_RXNE;
    uint8_t data = USART1->DR;
    if(!buf_rs485.flag)
    {
      if(!buf_rs485.cnt) tim_start(HAL_RS485_TIM);
      buf_rs485.data[buf_rs485.cnt++] = data;
      if(buf_rs485.cnt > IF_BUF_SIZE) buf_rs485.cnt = 0; // quick owf protect
      tim_reset(HAL_RS485_TIM);
    }
  }
}

void TIM2_IRQHandler()
{
  if(HAL_RS485_TIM->SR & TIM_SR_UIF)
  {
    HAL_RS485_TIM->SR &= ~TIM_SR_UIF;

    if(buf_rs485.cnt)
    {
      tim_stop(HAL_RS485_TIM);
      buf_rs485.flag = 1;
    }
  }
}

void hal_pkg_tx(const uint8_t *data, uint32_t size)
{
  //  uart1_tx_mode();
    while(size--) uart_out(USART1, *data++);
  //  uart2_rx_mode();
  //  led2_blink();
}

uint32_t hal_pkg_rx(if_pkg_t *pkg)
{
  if(buf_rs485.flag)
  {
    gpio_set(LEDS_PORT, LED_GREEN_PIN);

    pkg->data = buf_rs485.data;
    pkg->size = buf_rs485.cnt;
    pkg->tx = hal_pkg_tx;
    return 1;
  }
  return 0;
}

void hal_pkg_enable_rx()
{
  buf_rs485.cnt = 0;
  buf_rs485.flag = 0;

  gpio_reset(LEDS_PORT, LED_GREEN_PIN);
}

void bsp_init()
{
  set_sys_clock();

  // periferials
  i2c1_init();
  pwr_init();
  uart1_init();

  // en TIM2 clk
  RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
  tim_init(HAL_RS485_TIM, PKG_TIMEOUT_MS);
  NVIC_EnableIRQ(TIM2_IRQn); // allow TIM2_IRQn

  // leds
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOHEN;
  gpio_init(LEDS_PORT, LED_GREEN_PIN, GPIO_MODE_OUT);
  gpio_init(LEDS_PORT, LED_YELLOW_PIN, GPIO_MODE_OUT);

  gpio_set(LEDS_PORT, LED_YELLOW_PIN);
}
