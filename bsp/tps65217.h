/*
 * tps65217.h
 *
 *  Created on: Dec 27, 2018
 *      Author: netcat
 */

#ifndef TPS65217_H_
#define TPS65217_H_

#define I2C_PWR_ADDR 0x48

#define TPS65217_CHIP_ID 0xf0
#define TPS65217_CHIP_ID_B 0xf0

enum
{
  TPS65217_REG_CHIPID = 0x00,
  TPS65217_REG_PPATH = 0x01,
  TPS65217_REG_STATUS = 0x0a,
  TPS65217_REG_PASSWORD = 0x0b,
  TPS65217_REG_DEFDCDC1 = 0x0E,
  TPS65217_REG_DEFDCDC2 = 0x0F,
  TPS65217_REG_DEFDCDC3 = 0x10,
  TPS65217_REG_DEFSLEW = 0x11,
  TPS65217_REG_DEFLDO1 = 0x12,
  TPS65217_REG_DEFLDO2 = 0x13,
  TPS65217_REG_DEFLS1 = 0x14,
  TPS65217_REG_DEFLS2 = 0x15,
  TPS65217_REG_ENABLE = 0x16,
  TPS65217_REG_SEQ1 = 0x19,
  TPS65217_REG_SEQ2 = 0x1A,
  TPS65217_REG_SEQ3 = 0x1B,
  TPS65217_REG_SEQ4 = 0x1C,
  TPS65217_REG_SEQ5 = 0x1D,
  TPS65217_REG_SEQ6 = 0x1E,
};

void pwr_init(void);
uint8_t pwr_id(void);

#endif /* TPS65217_H_ */
