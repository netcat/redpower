NAME = redpower
ODIR = obj
MCU = cortex-m4
TOOLCHAIN = arm-none-eabi-

BSP = bsp

CC = $(TOOLCHAIN)gcc
CPPC = $(TOOLCHAIN)g++
OBJCOPY = $(TOOLCHAIN)objcopy
GDB = $(TOOLCHAIN)gdb
SIZE = $(TOOLCHAIN)size

# openocd config
DBG = stlink-v2
DBG_IF = hla_swd
DBG_MCU = stm32f4x

# ld
LDFILE = $(BSP)/hal/f429/link.ld

# src
SRC += main.c

# sys
SRC += sys/device.c
SRC += sys/crc16.c

# hal
SRC += $(BSP)/tps65217.c
SRC += $(BSP)/eefs.c
SRC += $(BSP)/hal/hw_gpio.c
SRC += $(BSP)/hal/hw_rcc.c
SRC += $(BSP)/hal/hw_uart.c
SRC += $(BSP)/hal/hw_i2c.c
SRC += $(BSP)/hal/hw_spi.c
SRC += $(BSP)/hal/hw_tim.c
SRC += $(BSP)/hal/f429/startup.c
SRC += $(BSP)/bsp.c

OBJS = $(patsubst %,$(ODIR)/%, ${SRC:.c=.o})

CPPSRC += $(wildcard apps/*.cpp) $(wildcard apps/*/*.cpp)
CPPOBJS = $(patsubst %,$(ODIR)/%, ${CPPSRC:.cpp=.o})

#inc
INC += .
INC += $(BSP)
INC += $(BSP)/hal/cmsis
INC += $(BSP)/hal/f429
INC += sys
_INC = $(INC:%=-I%)

CFLAGS = -std=gnu99 -mcpu=$(MCU) -mthumb -g -Wall -O1 -mfloat-abi=softfp -mfpu=fpv4-sp-d16 -ffunction-sections -fdata-sections
CFLAGS += -Wno-implicit-function-declaration
CPPFLAGS = $(CFLAGS) -fno-rtti -fno-exceptions -std=c++11 -Wno-implicit-function-declaration
LDFLAGS = -mcpu=$(MCU) -mthumb -T${LDFILE} -flto
LDFLAGS += -specs=nano.specs -specs=nosys.specs -nostartfiles

.PHONY : all
all: $(NAME).elf

$(ODIR)/%.o: %.c
	@mkdir -p $(ODIR)/$(dir $<)
	@echo "  CC\t$<"
	@$(CC) $(CFLAGS) $(_INC) -c $< -o $@

#$(ODIR)/%.o: %.cpp
#	@mkdir -p $(ODIR)/$(dir $<)
#	@echo "  CC\t$<"
#	@$(CPPC) $(CPPFLAGS) $(_INC) -c $< -o $@

$(NAME).elf: $(OBJS) $(CPPOBJS)
	@echo "  LD\t$@"
	@$(CPPC) $^ -o $@ $(LDFLAGS)
	@$(OBJCOPY) -O ihex $(NAME).elf $(NAME).hex
	@$(OBJCOPY) -O binary $(NAME).elf $(NAME).bin
	@$(SIZE) $(NAME).elf

flash: $(NAME).elf
	@openocd -f interface/$(DBG).cfg \
		-c"transport select $(DBG_IF)" \
		-f target/$(DBG_MCU).cfg \
		-c"init" \
		-c"reset halt" \
		-c"flash write_image erase $(NAME).elf" \
		-c"reset run" \
		-c"shutdown"

debug: $(NAME).elf
	@openocd -f interface/$(DBG).cfg \
		-c"transport select $(DBG_IF)" \
		-f target/$(DBG_MCU).cfg \
		-c"init" \
		-c"reset halt" \
		-c"flash write_image erase $(NAME).elf" \
		-c"reset halt"

rst: $(NAME).elf
	@openocd -f interface/$(DBG).cfg \
		-c"transport select $(DBG_IF)" \
		-f target/$(DBG_MCU).cfg \
		-c"init" \
		-c"reset run" \
		-c"shutdown"

gdb:
	$(GDB) --eval-command="target remote :3333" -tui $(NAME).elf

clean:
	@rm -f $(OBJS)
	@rm -f $(CPPOBJS)
	@rm -rf $(ODIR)
	@rm -f $(NAME).elf $(NAME).bin $(NAME).map

