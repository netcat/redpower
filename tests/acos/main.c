#include <stdio.h>
#include <stdint.h>
#include <sys/types.h>
#include <math.h>
#include <time.h>

#define M_RAD_TO_GRAD(rad) ((rad) * 180.0 / M_PI)
#define M_GRAD_TO_RAD(grad) ((grad) * M_PI / 180.0)

float acos_q_convergence(float x)
{
  float a = 1.43 + 0.59 * x;
  a = (a + (2 + 2 * x) / a) / 2;

  float b = 1.65 - 1.41 * x;
  b = (b + (2 - 2 * x) / b) / 2;

  float c = 0.88 - 0.77 * x;
  c = (c + (2 - a) / c) / 2;

  return (8 * (c + (2 - a) / c) - (b + (2 - 2 * x) / b)) / 6;
}

float acos_q_convergence_1(float x)
{
  float a = 1.43 + 0.59 * x;
  a = (a + (2 + 2 * x) / a) / 2;

  float b = 1.65 - 1.41 * x;
  b = (b + (2 - 2 * x) / b) / 2;

  float c = 0.88 - 0.77 * x;
  c = (c + (2 - a) / c) / 2;

  return 8 / 3 * c - b / 3;
}

float q_rsqrt(float number)
{
  long i;
  float x2, y;
  const float threehalfs = 1.5F;

  x2 = number * 0.5F;
  y = number;
  i = *(long *)&y;
  i = 0x5f3759df - (i >> 1);
  y = *(float *)&i;
  y = y * (threehalfs - (x2 * y * y));

  return y;
}

float acos_nvidia_q(float x)
{
  float negate = x < 0;
  x = x > 0 ? x : -x;
  float ret = -0.0187293;
  ret *= x;
  ret += 0.0742610;
  ret *= x;
  ret -= 0.2121144;
  ret *= x;
  ret += 1.5707288;
  ret *= 1.0 / q_rsqrt(1.0 - x);
  ret = ret - 2 * negate * ret;
  return negate * M_PI + ret;
}

float acos_linear(float x)
{
  return 3.14159 - 1.57079 * x;
}

float acos_chebyshev_pade(float x)
{
 float a = (2 * x - 1);
 float a2 = a * a;
 float a3 = a2 * a;
 float L = 0.5689111419 - 0.2644381021 * x - 0.4212611542 * a2 + 0.1475622352 * a3;
 float M = 2.006022274 - 2.343685222 * x + 0.3316406750 * a2 + 0.02607135626 * a3;
 return M_PI_2 - L / M;
}

float acos_nvidia(float x)
{
  float negate = x < 0;
  x = x > 0 ? x : -x;
  float ret = -0.0187293;
  ret *= x;
  ret += 0.0742610;
  ret *= x;
  ret -= 0.2121144;
  ret *= x;
  ret += 1.5707288;
  ret *= sqrt(1.0 - x);
  ret = ret - 2 * negate * ret;
  return negate * M_PI + ret;
}

float acos_std(float x)
{
  return acos(x);
}

void test(float min, float max, float step, const char *name, float (*f_test)(float))
{
  clock_t begin = clock();

  float max_d = 0;
  float avg = 0;
  uint32_t cnt = 0;
  for(float i = min; i < max; i += step)
  {
    float diff = fabs(acos(i) - f_test(i));
    if(diff > max_d)
    {
      max_d = diff;
      //printf("[%f]: %f \n", i, diff);
    }
    cnt++;
    avg += diff;
  }
  avg /= cnt;

  clock_t end = clock();
  float time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

  printf("%20s:   %f (%*.2f)   %f (%*.2f)   %.5f \n",
      name, max_d, 6, M_RAD_TO_GRAD(max_d), avg, 5, M_RAD_TO_GRAD(avg), time_spent);
}

#define ACOS_TEST(name) test(-1, 1, 0.000001, #name, name)

int main()
{
  printf("\n%*s%*s%*s%*s\n\n", 20, "NAME", 20, "MAX rad (deg)", 20, "AVG rad (deg)", 10, "MSEC");

  ACOS_TEST(acos_std);
  ACOS_TEST(acos_nvidia);
  ACOS_TEST(acos_nvidia_q);
  ACOS_TEST(acos_chebyshev_pade);
  ACOS_TEST(acos_linear);
  ACOS_TEST(acos_q_convergence);
  ACOS_TEST(acos_q_convergence_1);

  return 0;
}
