/*
 * cs5490.c
 *
 *  Created on: Jul 12, 2019
 *      Author: netcat
 */

#include "hal/hw_defs.h"
#include "sys/def/def_power.h"
#include "cs5490.h"
#include "device.h"
#include <math.h>

#ifdef __IAR_SYSTEMS_ICC__
#define M_PI 3.14159265358979323846
#define M_PI_2 1.57079632679489661923
#endif

static void cs5490_instruction(io_if_t *io_if, uint32_t val)
{
  uint8_t cfg[] = { CS5490_INSTRUCTION_MASK | (val & CS5490_CMD_MASK) };
  iovec vec = { cfg, sizeof(cfg) };
  io_if->transaction(io_if, &vec, 1, 0, 0);
}

void cs5490_write(io_if_t *io_if, uint32_t page, uint32_t addr, uint32_t val)
{
  uint8_t cfg[] = {
      CS5490_PAGE_SELECT_MASK | (page & CS5490_CMD_MASK),
      CS5490_REG_WRITE_MASK | (addr & CS5490_CMD_MASK)
  };

  iovec vec[] = {
      { cfg, sizeof(cfg) },
      { &val, CS5490_DATA_SIZE }
  };

  io_if->transaction(io_if, vec, SIZE_OF_ARRAY(vec), 0, 0);
}

uint32_t cs5490_read(io_if_t *io_if, uint32_t page, uint32_t addr, uint8_t *val)
{
  uint8_t cfg[] = {
      CS5490_PAGE_SELECT_MASK | (page & CS5490_CMD_MASK),
      CS5490_REG_READ_MASK | (addr & CS5490_CMD_MASK)
  };

  iovec vec = { cfg, sizeof(cfg) };
  uint32_t st = io_if->transaction(io_if, &vec, 1, val, CS5490_DATA_SIZE);
  return st;
}

// see: tests/acos/results.txt
static float acos_fast(float x)
{
  float a = (2 * x - 1);
  float a2 = a * a;
  float a3 = a2 * a;
  float L = 0.5689111419 - 0.2644381021 * x - 0.4212611542 * a2 + 0.1475622352 * a3;
  float M = 2.006022274 - 2.343685222 * x + 0.3316406750 * a2 + 0.02607135626 * a3;
  return M_PI_2 - L / M;
}

// signed, 23 frac
static float cs5490_unpack_float(uint32_t val)
{
  float f;
  *(uint32_t *)&f = (0x3f8 << 20) | (val & 0x7fffff);
  if(val & (1 << 23)) f -= 1; // sign ?
  f -= 1;
  return f;
}

void cs5490_rst(io_if_t *io_if)
{
  cs5490_instruction(io_if, CS5490_SOFTWARE_RESET);
}

void cs5490_setup(io_if_t *io_if)
{
  cs5490_instruction(io_if, CS5490_INSTRUCTION_CONTIMUE_MODE);
  cs5490_write(io_if, 0, 7, CS5490_RX_CSUM_OFF | CS5490_BR(CS5490_BR_FULL_SPEED));
}

uint32_t cs5490_get_instant(io_if_t *io_if, instant_data_t *db)
{
  typedef struct
  {
    float *out;
    uint32_t reg;
    uint32_t shift;
  } instant_data_getter_t;

  instant_data_getter_t gtable[] = {
      { &db->load.S, CS5490_ADDR_S, },
      { &db->load.P, CS5490_ADDR_P_AVG, },
      { &db->load.Q, CS5490_ADDR_Q_AVG, },
      { &db->U, CS5490_ADDR_V_RMS, 1, },
      { &db->I, CS5490_ADDR_I_RMS, 1, },
      { &db->F, CS5490_ADDR_EPSILON, },
      { &db->PF, CS5490_ADDR_PF, },
  };

  // request
  for(int i = 0; i < SIZE_OF_ARRAY(gtable); i++)
  {
    uint32_t val = 0;
    uint32_t st = cs5490_read(io_if, CS5490_PAGE_RMS, gtable[i].reg, (uint8_t *)&val);
    if(st) return st;
    *gtable[i].out = cs5490_unpack_float(val >> gtable[i].shift);
  }

  // scale
  db->U *= 1000;
  db->F *= CS5490_ESPILON_PS;
  db->angle = M_RAD_TO_GRAD(acos_fast(db->PF));

  return 0;
}
