/*
 * cs5490.h
 *
 *  Created on: Jul 12, 2019
 *      Author: netcat
 */

#ifndef CS5490_H_
#define CS5490_H_

// access masks
#define CS5490_REG_READ_MASK 0x00
#define CS5490_REG_WRITE_MASK 0x40
#define CS5490_PAGE_SELECT_MASK 0x80
#define CS5490_INSTRUCTION_MASK 0xC0
#define CS5490_CMD_MASK 0x3f

#define CS5490_SOFTWARE_RESET 0x01
#define CS5490_INSTRUCTION_CONTIMUE_MODE 0x15
#define CS5490_RX_CSUM_OFF 0x20000

// pages
#define CS5490_PAGE_RMS 16

enum
{
  CS5490_ADDR_P_AVG = 5,      // Active Power (Pavg)
  CS5490_ADDR_I_RMS = 6,      // RMS Current (Irms)
  CS5490_ADDR_V_RMS = 7,      // RMS Voltage (Vrms)
  CS5490_ADDR_Q_AVG = 14,     // Reactive Power (Qavg)
  CS5490_ADDR_S = 20,         // Apparent Power (S)
  CS5490_ADDR_PF = 21,        // Power Factor (PF)
  CS5490_ADDR_EPSILON = 49,   // Line to Sample Frequency Ratio (Epsilon)
};

// system constants
#define CS5490_POWER_UP_DELAY_MS 130 // >= 130ms wait for POR reset
#define CS5490_SERIAL_TIMEOUT_MS 128 // >= 128ms for serial time out (if device already configured)
#define CS5490_MCLK_HZ 4096000
#define CS5490_ESPILON_PS 4000
#define CS5490_DATA_SIZE 3
#define CS5490_BR(baudrate) ((uint32_t)((baudrate) * (524288.0 / CS5490_MCLK_HZ) + 0.5))

#define CS5490_BR_DEFAULT 600
#define CS5490_BR_FULL_SPEED 9600

void cs5490_rst(io_if_t *io_if);
void cs5490_setup(io_if_t *io_if);
uint32_t cs5490_read(io_if_t *io_if, uint32_t page, uint32_t addr, uint8_t *val);
void cs5490_write(io_if_t *io_if, uint32_t page, uint32_t addr, uint32_t val);
uint32_t cs5490_get_instant(io_if_t *io_if, instant_data_t *db);

#endif /* CS5490_H_ */
