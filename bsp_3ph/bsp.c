/*
 * bsp.c
 *
 *  Created on: Apr 12, 2019
 *      Author: netcat
 */

#include <stdlib.h>
#include <stdint.h>

#include "hal/hw_gpio.h"
#include "hal/hw_uart.h"
#include "hal/hw_spi.h"
#include "hal/hw_crc.h"
#include "hal/hw_rtc.h"
#include "hal/hw_rcc.h"
#include "hal/hw_tim.h"
#include "hal/hw_i2c.h"

#include "sys/device.h"
#include "sys/pkg.h"
#include "sys/def/def_power.h"
#include "cs5490.h"
#include "bsp.h"
#include "sys/syslog.h"

// control of RS458 RX/TX mode
static void u5_sel(uint32_t select)
{
  if(select) GPIO_SET(RXTX_485_PORT, RXTX_485_PIN);
  else GPIO_RESET(RXTX_485_PORT, RXTX_485_PIN);
}

static void uart5_init()
{
  // clk: gpio & uart
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIOCEN | RCC_AHB2ENR_GPIODEN;
  RCC->APB1ENR1 |= RCC_APB1ENR1_UART5EN;

  // rst
  RCC->APB1RSTR1 |= RCC_APB1RSTR1_UART5RST;
  RCC->APB1RSTR1 &= ~RCC_APB1RSTR1_UART5RST;

  // RX/TX mode pin
  gpio_init(RXTX_485_PORT, RXTX_485_PIN, GPIO_MODE_OUT);
  GPIO_RESET(RXTX_485_PORT, RXTX_485_PIN);

  // GPIO cfg
  const gpio_init_t gpio_uart5_table[] = {
    { GPIOC, 12 }, // UART5_TX
    { GPIOD, 2 }, // UART5_RX
    { 0 }};
  gpio_init_af(gpio_uart5_table, GPIO_AF8_UART5);

  uart_config(UART5, HAL_RS485_BAUDRATE, 1);
  NVIC_EnableIRQ(UART5_IRQn);
}

static void uart4_init()
{
  // clk: gpio & uart
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIOCEN;
  RCC->APB1ENR1 |= RCC_APB1ENR1_UART4EN;

  // rst
  RCC->APB1RSTR1 |= RCC_APB1RSTR1_UART4RST;
  RCC->APB1RSTR1 &= ~RCC_APB1RSTR1_UART4RST;

  // GPIO cfg
  const gpio_init_t gpio_uart4_table[] = {
    { GPIOC, 10 }, // UART4_TX
    { GPIOC, 11 }, // UART4_RX
    { 0 }};
  gpio_init_af(gpio_uart4_table, GPIO_AF8_UART4);

  uart_config(UART4, HAL_RS485_BAUDRATE, 1);
  NVIC_EnableIRQ(UART4_IRQn);
}

static void usart1_init(uint32_t br)
{
  // clk: gpio & uart
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIOAEN;
  RCC->APB2ENR |= RCC_APB2ENR_USART1EN;

  // rst
  RCC->APB2RSTR |= RCC_APB2RSTR_USART1RST;
  RCC->APB2RSTR &= ~RCC_APB2RSTR_USART1RST;

  // GPIO cfg
  const gpio_init_t gpio_usart1_table[] = {
    { GPIOA, 10 }, // USART1_TX
    { GPIOA, 9 }, // USART1_RX
    { 0 }};
  gpio_init_af(gpio_usart1_table, GPIO_AF7_USART1);

  uart_config(USART1, br, 0);
}

static void usart2_init(uint32_t br)
{
  // clk: gpio & uart
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIODEN;
  RCC->APB1ENR1 |= RCC_APB1ENR1_USART2EN;

  // rst
  RCC->APB1RSTR1 |= RCC_APB1RSTR1_USART2RST;
  RCC->APB1RSTR1 &= ~RCC_APB1RSTR1_USART2RST;

  // GPIO cfg
  const gpio_init_t gpio_usart2_table[] = {
    { GPIOD, 5 }, // USART2_TX
    { GPIOD, 6 }, // USART2_RX
    { 0 }};
  gpio_init_af(gpio_usart2_table, GPIO_AF7_USART2);

  uart_config(USART2, br, 0);
}

static void usart3_init(uint32_t br)
{
  // clk: gpio & uart
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIODEN;
  RCC->APB1ENR1 |= RCC_APB1ENR1_USART3EN;

  // rst
  RCC->APB1RSTR1 |= RCC_APB1RSTR1_USART3RST;
  RCC->APB1RSTR1 &= ~RCC_APB1RSTR1_USART3RST;

  // GPIO cfg
  const gpio_init_t gpio_usart3_table[] = {
    { GPIOD, 8 }, // USART3_TX
    { GPIOD, 9 }, // USART3_RX
    { 0 }};
  gpio_init_af(gpio_usart3_table, GPIO_AF7_USART3);

  uart_config(USART3, br, 0);
}

io_if_t io_if_table[] = {
    { USART1, 0, uart_io, 0 },     // HAL_PHASE_A_IO_IF
    { USART2, 0, uart_io, 0 },     // HAL_PHASE_C_IO_IF
    { USART3, 0, uart_io, 0 },     // HAL_PHASE_B_IO_IF
    { UART5, u5_sel, uart_io, 1 }, // HAL_RS458_IO_IF (flag - port number)
    { UART4, 0, uart_io, 2 },      // HAL_GATEWAY (flag - port number)
};

uint8_t buf_rs485[HAL_RS485_BUF_SIZE];
uint8_t buf_gateway[HAL_RS485_BUF_SIZE];

if_msg_t if_pkg[] = {
    { .data = buf_rs485, .io_if = &io_if_table[HAL_RS458_IO_IF] },
    { .data = buf_gateway, .io_if = &io_if_table[HAL_GATEWAY] },
};

void UART5_IRQHandler(void)
{
  uint8_t data;
  if(uart_rx(UART5, &data) && (!if_pkg[OIF_RS485].flag))
  {
    uint32_t size = if_pkg[OIF_RS485].size;

    if(!size) tim_start(TIM5);
    if_pkg[OIF_RS485].data[size] = data;

    if(size < HAL_RS485_BUF_SIZE) if_pkg[OIF_RS485].size++; // owf protect

    tim_reset(TIM5);
  }
}

static void tim5_init()
{
  RCC->APB1ENR1 |= RCC_APB1ENR1_TIM5EN;
  tim_config(TIM5, PKG_TIMEOUT_MS, 1);
  NVIC_EnableIRQ(TIM5_IRQn);
}

void TIM5_IRQHandler()
{
  if(TIM5->SR & TIM_SR_UIF)
  {
    TIM5->SR &= ~TIM_SR_UIF;
    if(if_pkg[OIF_RS485].size) if_pkg[OIF_RS485].flag = 1;
  }
}

void UART4_IRQHandler(void)
{
  uint8_t data;

  if(uart_rx(UART4, &data) && (!if_pkg[OIF_GATEWAY].flag))
  {
    uint32_t size = if_pkg[OIF_GATEWAY].size;

    if(!size) tim_start(TIM4);
    if_pkg[OIF_GATEWAY].data[size] = data;

    if(size < HAL_RS485_BUF_SIZE) if_pkg[OIF_GATEWAY].size++; // owf protect

    tim_reset(TIM4);
  }
}

static void tim4_init()
{
  RCC->APB1ENR1 |= RCC_APB1ENR1_TIM4EN;
  tim_config(TIM4, PKG_TIMEOUT_MS, 1);
  NVIC_EnableIRQ(TIM4_IRQn);
}

void TIM4_IRQHandler()
{
  if(TIM4->SR & TIM_SR_UIF)
  {
    TIM4->SR &= ~TIM_SR_UIF;
    if(if_pkg[OIF_GATEWAY].size) if_pkg[OIF_GATEWAY].flag = 1;
  }
}

if_msg_t *hal_pkg_rx(uint32_t oif_index)
{
  if(if_pkg[oif_index].flag)
    return &if_pkg[oif_index];

  return 0;
}

if_msg_t *hal_pkg_if(uint32_t oif_index)
{
  return &if_pkg[oif_index];
}

void hal_pkg_free(if_msg_t *if_pkg)
{
  if_pkg->size = 0;
  if_pkg->flag = 0;
}

io_if_t *hal_io_if(uint32_t index)
{
  return &io_if_table[index];
}

static void bsp_power_meters_init()
{
  // todo: enable CRC of meters
#if 0
  usart1_init(CS5490_BR_DEFAULT);
  usart2_init(CS5490_BR_DEFAULT);
  usart3_init(CS5490_BR_DEFAULT);

//  delay_cyc(1000000);
  wait_ms(CS5490_POWER_UP_DELAY_MS);

  cs5490_rst(&io_if_table[HAL_PHASE_A_IO_IF]);
  cs5490_rst(&io_if_table[HAL_PHASE_B_IO_IF]);
  cs5490_rst(&io_if_table[HAL_PHASE_C_IO_IF]);

//  delay_cyc(1000000);
  wait_ms(CS5490_POWER_UP_DELAY_MS);

  cs5490_setup(&io_if_table[HAL_PHASE_A_IO_IF]);
  cs5490_setup(&io_if_table[HAL_PHASE_B_IO_IF]);
  cs5490_setup(&io_if_table[HAL_PHASE_C_IO_IF]);

//  delay_cyc(1000000);
  wait_ms(CS5490_SERIAL_TIMEOUT_MS);
#endif

  usart1_init(CS5490_BR_FULL_SPEED);
  usart2_init(CS5490_BR_FULL_SPEED);
  usart3_init(CS5490_BR_FULL_SPEED);
}

static void bsp_gpio_init()
{
  PWR->CR2 |= PWR_CR2_IOSV;

  // LEDs
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIODEN;
  gpio_init(LED_ACT_PORT, LED_ACT_PIN, GPIO_MODE_OUT);
  gpio_init(LED_REACT_PORT, LED_REACT_PIN, GPIO_MODE_OUT);

  // backlight
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIOEEN;
  gpio_init(LED_BL_PORT, LED_BL_PIN, GPIO_MODE_OUT);

  // U_GATE_L
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIOCEN;
  gpio_init(U_GATE_OPTO_L1_PORT, U_GATE_OPTO_L1_PIN, GPIO_MODE_OUT);
  GPIO_RESET(U_GATE_OPTO_L1_PORT, U_GATE_OPTO_L1_PIN);

  // REL_ON
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIOEEN;
  gpio_init(REL_ON_PORT, REL_ON_PIN, GPIO_MODE_OUT);
  GPIO_RESET(REL_ON_PORT, REL_ON_PIN);

  // REL_OFF
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIOBEN;
  gpio_init(REL_OFF_PORT, REL_OFF_PIN, GPIO_MODE_OUT);
  GPIO_RESET(REL_OFF_PORT, REL_OFF_PIN);

  // TAMPER1
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIOAEN;
  gpio_init(TAMPER1_PORT, TAMPER1_PIN, GPIO_MODE_IN);

  // TAMPER2
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIOAEN;
  gpio_init(TAMPER2_PORT, TAMPER2_PIN, GPIO_MODE_IN);

  // MAG
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIODEN;
  gpio_init(MAG_PORT, MAG_PIN, GPIO_MODE_IN);
}

#if 0
static void fast_test()
{
  if(cs5490_read(&io_if_table[HAL_PHASE_A_IO_IF], 0, 0) == 0xC02000)
  {
    cs5490_write(&io_if_table[HAL_PHASE_A_IO_IF], 0, 0, 0xC02100);
    if(cs5490_read(&io_if_table[HAL_PHASE_A_IO_IF], 0, 0) == 0xC02100)
    {
      cs5490_write(&io_if_table[HAL_PHASE_A_IO_IF], 0, 0, 0xC02000);
      if(cs5490_read(&io_if_table[HAL_PHASE_A_IO_IF], 0, 0) == 0xC02000)
      {
        GPIO_SET(LED_ACT_PORT, LED_ACT_PIN);
      }
    }
  }
}
#endif

void bsp_relay_set(uint32_t st)
{
  slog("%s: %d", __FUNCTION__, st);

  if(st)
  {
    GPIO_RESET(REL_OFF_PORT, REL_OFF_PIN);
    GPIO_SET(REL_ON_PORT, REL_ON_PIN);
  }
  else
  {
    GPIO_RESET(REL_ON_PORT, REL_ON_PIN);
    GPIO_SET(REL_OFF_PORT, REL_OFF_PIN);
  }
}

void bsp_init()
{
  rcc_setup_clk();
  bsp_gpio_init();

  crc_init(CRC16_MODBUS_POLY, CRC16_MODBUS_RESET_VALUE);

  // gateway
  uart4_init();
  tim4_init();

  // RS-485
  uart5_init();
  tim5_init();

  bsp_power_meters_init();

  i2c3_init();

  rcc_setup_lse();
  rtc_init(RTC);

//  fast_test();
}
