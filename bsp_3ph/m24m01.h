/*
 * m24m01.h
 *
 *  Created on: Jul 25, 2019
 *      Author: netcat
 */

#ifndef BSP_3PH_M24M01_H_
#define BSP_3PH_M24M01_H_

#define M24M01_DEV I2C3
#define M24M01_ADDR 0xA0
#define M24M01_PAGE_SEL_H 0x02
#define EEPROM_PAGE_SEL_L 0x00
#define M24M01_PAGE_SIZE 128
#define M24M01_PAGE_WRITE_TIME_MS 5
#define M24M01_SIZE (128 * 1024)

uint8_t eeprom_write(uint32_t addr, uint8_t *data, uint32_t count);
uint8_t eeprom_read(uint32_t addr, uint8_t *data, uint32_t count);

#endif /* BSP_3PH_M24M01_H_ */
