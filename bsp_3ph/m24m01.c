/*
 * m24m01.c
 *
 *  Created on: Jul 25, 2019
 *      Author: netcat
 */

#include <stdint.h>
#include <stdio.h>
#include "hal/hw_defs.h"
#include "hal/hw_i2c.h"
#include "m24m01.h"
#include "hal/hw_tim.h"
#include "sys/syslog.h"

uint8_t eeprom_write(uint32_t addr, uint8_t *data, uint32_t count)
{
  while(count)
  {
    uint32_t wrb = MIN(count, M24M01_PAGE_SIZE - (addr % M24M01_PAGE_SIZE));

    uint8_t dev = (addr < 0x10000) ? M24M01_ADDR : (M24M01_ADDR | M24M01_PAGE_SEL_H);
    uint8_t addr_t[] = { addr >> 8, addr };

    const iovec v[] = {
        { &dev, sizeof(dev) },
        { addr_t, sizeof(addr_t) },
        { data, wrb },
    };

//    slog("%s: %04x, w: %d", __FUNCTION__, addr, wrb);

    uint8_t st = i2c_io(M24M01_DEV, v, SIZE_OF_ARRAY(v), 0, 0);
    if(st)
    {
      slog("%s: i2c write io error", __FUNCTION__);
      return st;
    }
    wait_ms(M24M01_PAGE_WRITE_TIME_MS);

    addr += wrb;
    data += wrb;
    count -= wrb;
  }

  return 0;
}

uint8_t eeprom_read(uint32_t addr, uint8_t *data, uint32_t count)
{
  while(count)
  {
    uint32_t rdb = MIN(count, M24M01_PAGE_SIZE - (addr % M24M01_PAGE_SIZE));

    uint8_t dev = (addr < 0x10000) ? M24M01_ADDR : (M24M01_ADDR | M24M01_PAGE_SEL_H);
    uint8_t addr_t[] = { addr >> 8, addr };

    const iovec v[] = {
        { &dev, sizeof(dev) },
        { addr_t, sizeof(addr_t) },
    };

//    slog("%s: %04x, r: %d", __FUNCTION__, addr, rdb);

    uint8_t st = i2c_io(M24M01_DEV, v, SIZE_OF_ARRAY(v), data, rdb);
    if(st)
    {
      slog("%s: i2c read io error", __FUNCTION__);
      return st;
    }

    addr += rdb;
    data += rdb;
    count -= rdb;
  }

  return 0;
}

