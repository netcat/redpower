/*
 * hw_rtc.h
 *
 *  Created on: Jul 23, 2019
 *      Author: netcat
 */

#ifndef BSP_HAL_HW_RTC_H_
#define BSP_HAL_HW_RTC_H_

#include "hw_defs.h"

// PS defaults
#define RTC_ASYNCH_PS_DEFAULT 0x7F
#define RTC_SYNCH_PS_DEFAULT 0xFF
// WP keys
#define RTC_WP_EN 0xFF
#define RTC_WP_DIS_KEY1 0xCA
#define RTC_WP_DIS_KEY2 0x53
// backup domain
#define RTC_BACKUP_REG(reg) (*(volatile uint32_t *)((&RTC->BKP0R) + (reg)))
#define RTC_BACKUP_MAGIC 0x32F2

// time & date masks
#define RTC_TR_MASK (RTC_TR_HT | RTC_TR_HU | RTC_TR_MNT | RTC_TR_MNU | RTC_TR_ST | RTC_TR_SU)
#define RTC_DR_MASK (RTC_DR_WDU | RTC_DR_MT | RTC_DR_MU | RTC_DR_DT | RTC_DR_DU | RTC_DR_YT | RTC_DR_YU)

// time extractors
#define RTC_RAW_HOUR(time) (((time) & (RTC_TR_HT | RTC_TR_HU)) >> RTC_TR_HU_Pos)
#define RTC_RAW_MIN(time) (((time) & (RTC_TR_MNT | RTC_TR_MNU)) >> RTC_TR_MNU_Pos)
#define RTC_RAW_SEC(time) (((time) & (RTC_TR_ST | RTC_TR_SU)) >> RTC_TR_SU_Pos)
// date extractors
#define RTC_RAW_YEAR(date) (((date) & (RTC_DR_YT | RTC_DR_YU)) >> RTC_DR_YU_Pos)
#define RTC_RAW_WD(date) (((date) & RTC_DR_WDU) >> RTC_DR_WDU_Pos)
#define RTC_RAW_MONTH(date) (((date) & (RTC_DR_MT | RTC_DR_MU)) >> RTC_DR_MU_Pos)
#define RTC_RAW_DAY(date) (((date) & (RTC_DR_DT | RTC_DR_DU)) >> RTC_DR_DU_Pos)

typedef struct
{
  uint8_t hour;
  uint8_t min;
  uint8_t sec;
  uint8_t ms;
} rtc_time_t;

typedef struct
{
  uint8_t day;
  uint8_t month;
  uint8_t year;
  uint8_t wd;
} rtc_date_t;

typedef struct
{
  rtc_time_t time;
  rtc_date_t date;
} rtc_dt_t;

typedef struct
{
  uint32_t time;
  uint32_t date;
} rtc_raw_t;

typedef struct
{
  rtc_dt_t st;
  rtc_dt_t end;
} rtc_interval_t;

void rtc_init(RTC_TypeDef *RTCx);
void rtc_set_dt(const rtc_dt_t *dt);
void rtc_get_dt(rtc_dt_t *dt);
void rtc_get_raw(rtc_raw_t *rtc_raw);
void rtc_raw2date(const rtc_raw_t *rtc_raw, rtc_date_t *date);
void rtc_raw2time(const rtc_raw_t *rtc_raw, rtc_time_t *time);

uint32_t rtc_dt_cmp(const rtc_dt_t *dt1, const rtc_dt_t *dt2);
uint32_t rtc_dt_between(const rtc_interval_t *internal, const rtc_dt_t *dt);

#endif /* BSP_HAL_HW_RTC_H_ */
