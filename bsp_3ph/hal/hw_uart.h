/*
 * hw_uart.h
 *
 *  Created on: Feb 8, 2017
 *      Author: netcat
 */

#ifndef HW_UART_H_
#define HW_UART_H_

#include "hw_defs.h"

void uart_config(USART_TypeDef *uart, uint32_t baudrate, uint32_t rx_ie);
uint32_t uart_rx(USART_TypeDef *UARTx, uint8_t *data);
uint32_t uart_io(io_if_t *io_if, const iovec *vec, uint32_t vec_len, void *rd_buf, uint32_t rd_len);

#endif /* HW_UART_H_ */
