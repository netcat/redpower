/*
 * hw_tim.c
 *
 *  Created on: Jul 5, 2019
 *      Author: netcat
 */

#include "hal/hw_tim.h"
#include "hal/hw_rcc.h"

void delay_cyc(volatile unsigned int cnt) { while(cnt--); }

void wait_ms(uint32_t ms)
{
  // todo: it must be checked on oscillograph for apply new realisation
#if 1
  delay_cyc(ms * 5000);
#else
  RCC->APB1ENR1 |= RCC_APB1ENR1_TIM2EN;

  TIM2->PSC = ((RCC_APB1_CLK_HZ * RCC_TMRS_PRE_MUL) / (1000 * 4)) - 1; // PS (devided by 4 for 16bit result)
  TIM2->ARR = ms * 4; // 1ms (mul 4, see prev step)
  TIM2->CR1 = TIM_CR1_OPM; // one pulse mode
  TIM2->EGR = TIM_EGR_UG; // update generation

  TIM2->SR = ~TIM_SR_UIF; // update flag clear
  TIM2->CNT = 0; // reset counter

  TIM2->CR1 |= TIM_CR1_CEN; // en
  while(!(TIM2->SR & TIM_SR_UIF)); // wait flag
  TIM2->CR1 = 0; // dis
#endif
}

void tim_config(TIM_TypeDef *tim, uint32_t timeout, uint32_t one_pulse_mode)
{
	// 8mhz / 8000 = 1khz
  if(one_pulse_mode) tim->CR1 |= TIM_CR1_OPM; // one pulse mode
	tim->PSC = 8000 - 1; // prescaler
	tim->ARR = timeout; // ms
	tim->DIER |= TIM_DIER_UIE; // allow interrupt
	tim_reset(tim);
	tim_stop(tim);

	tim->EGR |= TIM_EGR_UG;
	tim->SR = ~TIM_SR_UIF;
}

void tim_start(TIM_TypeDef *tim)
{
  tim->CNT = 0; // reset counter
  tim->CR1 |= TIM_CR1_CEN; // start
}

void tim_reset(TIM_TypeDef *tim)
{
  tim->CNT = 0; // reset counter
}

void tim_stop(TIM_TypeDef *tim)
{
  tim->CR1 &= ~TIM_CR1_CEN; // stop
}


