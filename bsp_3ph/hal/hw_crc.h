/*
 * hw_crc.h
 *
 *  Created on: Jul 19, 2019
 *      Author: netcat
 */

#ifndef HW_CRC_H
#define HW_CRC_H

#define CRC16_MODBUS_POLY 0x8005
#define CRC16_MODBUS_RESET_VALUE 0xffff

void crc_init(uint32_t poly, uint32_t init_val);
uint32_t crc_calc16(const iovec *vec, uint32_t vec_len);

#endif /* HW_CRC_H */
