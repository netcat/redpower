/*
 * hal_i2c.h
 *
 *  Created on: Jul 15, 2017
 *      Author: netcat
 */

#ifndef HAL_HW_I2C_H_
#define HAL_HW_I2C_H_

#include "hw_defs.h"

#define I2C_MAX_STD_SPEED_HZ 100000

void i2c3_init();
uint32_t i2c_io(I2C_TypeDef *I2Cx, const iovec *vec, uint32_t vec_len, void *rd_buf, uint32_t rd_len);

#endif /* HAL_HW_I2C_H_ */
