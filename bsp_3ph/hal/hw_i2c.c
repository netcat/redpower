#include <stdlib.h>
#include <stdint.h>
#include "hal/hw_gpio.h"
#include "hal/hw_i2c.h"
#include "hal/hw_rcc.h"

#include "hal/hw_gpio.h"
#include "bsp.h"

static void i2c_cfg(I2C_TypeDef *I2Cx, uint32_t freq)
{
  // sw reset
  I2Cx->CR1 |= I2C_CR1_SWRST;
  I2Cx->CR1 &= ~I2C_CR1_SWRST;

  // setup freq
  I2Cx->TIMINGR = 0x00300820;

  // enable
  I2Cx->CR1 = I2C_CR1_PE;
}

void i2c3_init()
{
  // gpio cfg
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIOGEN;

  const gpio_init_t gpio_i2c3_init[] = {
    { GPIOG, 7 }, // SCL
    { GPIOG, 8 }, // SDA
    { 0 }};

  gpio_init_af_ext(gpio_i2c3_init, GPIO_AF4_I2C3, GPIO_OTYPE_OD, GPIO_PUPD_NOPULL);

  // clk en
  RCC->APB1ENR1 |= RCC_APB1ENR1_I2C3EN;

  // hw rst
  RCC->APB1RSTR1 |= RCC_APB1RSTR1_I2C3RST;
  RCC->APB1RSTR1 &= ~RCC_APB1RSTR1_I2C3RST;

  i2c_cfg(I2C3, I2C_MAX_STD_SPEED_HZ);
}

uint32_t i2c_io(I2C_TypeDef *I2Cx, const iovec *vec, uint32_t vec_len, void *rd_buf, uint32_t rd_len)
{
  uint32_t tout;
  uint8_t *rdb = rd_buf;
  uint8_t dev_addr = ((uint8_t*)vec[0].data)[0];

  uint32_t vec_data_len = 0;
  for(uint32_t i = 1; i < vec_len; i++) vec_data_len += vec[i].len;

  // reset
  I2Cx->CR2 = 0;
  EVT_TOUT(I2Cx->ISR & I2C_ISR_BUSY);

  // dev addr & start
  I2Cx->CR2 = (vec_data_len << I2C_CR2_NBYTES_Pos) | (dev_addr << I2C_CR2_SADD_Pos);
  I2Cx->CR2 |= I2C_CR2_START;
  EVT_TOUT(!(I2Cx->ISR & I2C_ISR_TXIS));

  // send internal addr & data
  for(uint32_t i = 1; i < vec_len; i++)
  {
    for(uint32_t j = 0; j < vec[i].len; j++)
    {
      EVT_TOUT(!(I2Cx->ISR & I2C_ISR_TXE));
      I2Cx->TXDR = ((uint8_t*)vec[i].data)[j];
    }
  }
  EVT_TOUT(!(I2Cx->ISR & I2C_ISR_TC));

  if(rd_len)
  {
    // dev addr & restart
    I2Cx->CR2 = (rd_len << I2C_CR2_NBYTES_Pos) | I2C_CR2_RD_WRN | (dev_addr << I2C_CR2_SADD_Pos);
    I2Cx->CR2 |= I2C_CR2_START;

    // recv data
    while(rd_len--)
    {
      EVT_TOUT(!(I2Cx->ISR & I2C_ISR_RXNE));
      *rdb++ = I2Cx->RXDR;
    }
  }

  // stop & wait until busy & clear stop flag
  I2Cx->CR2 |= I2C_CR2_STOP;
  return 0;

terr:
  I2Cx->CR2 |= I2C_CR2_STOP;
  return 1;
}

