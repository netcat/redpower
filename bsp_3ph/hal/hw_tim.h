/*
 * hw_tim.h
 *
 *  Created on: Jul 5, 2019
 *      Author: netcat
 */

#ifndef HAL_HW_TIM_H_
#define HAL_HW_TIM_H_

#include "hw_defs.h"

void wait_ms(uint32_t ms);

void tim_config(TIM_TypeDef *tim, uint32_t timeout, uint32_t one_pulse_mode);
void tim_start(TIM_TypeDef *tim);
void tim_reset(TIM_TypeDef *tim);
void tim_stop(TIM_TypeDef *tim);

#endif /* HAL_HW_TIM_H_ */
