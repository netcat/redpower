/*
 * hw_wdt.h
 *
 *  Created on: Aug 26, 2019
 *      Author: vchumakov
 */

#ifndef BSP_3PH_HAL_HW_WDT_C_
#define BSP_3PH_HAL_HW_WDT_C_

#include "hw_defs.h"

void wdt_reset(void);

#endif /* BSP_3PH_HAL_HW_WDT_C_ */


