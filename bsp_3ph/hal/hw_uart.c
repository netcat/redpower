/*
 * hw_uart.c
 *
 *  Created on: Feb 8, 2017
 *      Author: netcat
 */

#include "hal/hw_gpio.h"
#include "hal/hw_rcc.h"
#include "hal/hw_uart.h"

void uart_config(USART_TypeDef *UARTx, uint32_t br, uint32_t rx_ie)
{
  // cfg
  UARTx->CR1 = USART_CR1_TE | USART_CR1_RE;
  UARTx->CR2 = 0;
  UARTx->CR3 = 0;

  // rx ie enable
  if(rx_ie) UARTx->CR1 |= USART_CR1_RXNEIE;
  // baud

  UARTx->BRR = DIV_INT_ROUND(RCC_APB1_CLK_HZ, br);
  // en
  UARTx->CR1 |= USART_CR1_UE;
}

uint32_t uart_rx(USART_TypeDef *UARTx, uint8_t *data)
{
  uint32_t flag = UARTx->ISR & USART_ISR_RXNE;
  if(flag)
  {
    UARTx->ICR |= USART_ISR_RXNE;
    *data = UARTx->RDR;
  }
  return flag;
}

uint32_t uart_io(io_if_t *io_if, const iovec *vec, uint32_t vec_len, void *rd_buf, uint32_t rd_len)
{
  uint32_t tout;
  uint8_t *rdb = rd_buf;
  USART_TypeDef *UARTx = (USART_TypeDef*)io_if->reg;

  if(io_if->select) io_if->select(1);

  uint32_t i, j;
  for(i = 0; i < vec_len; i++)
    for(j = 0; j < vec[i].len; j++)
    {
      EVT_TOUT(!(UARTx->ISR & USART_ISR_TXE));
      UARTx->TDR = ((uint8_t*)vec[i].data)[j];
    }

  if(io_if->select)
  {
    // return to RX mode only after end of transaction
    while(!(UARTx->ISR & USART_ISR_TC));
    io_if->select(0);
  }

  while(rd_len--)
  {
    while(!(UARTx->ISR & USART_ISR_RXNE));
    if(rdb) *rdb++ = UARTx->RDR;
  }

  return 0;

terr:
  if(io_if->select) io_if->select(0);
  return 1;
}


