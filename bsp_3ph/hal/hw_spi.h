/*
 * hw_spi.h
 *
 *  Created on: Apr 6, 2019
 *      Author: netcat
 */

#ifndef HAL_HW_SPI_H_
#define HAL_HW_SPI_H_

#include "hw_defs.h"

typedef struct
{
	SPI_TypeDef *SPIx;
	void (*select)(uint32_t);
	uint32_t flags;
} spi_dev;

#define SPI_FLAG_FILL_FF 0x01 // use 0xff pattern to fill txdata line during read phase
#define SPI_FLAG_OVERLAP 0x02 // the bytes received during the write phase are used as read

void spi_cfg(SPI_TypeDef *SPIx);
void spi_io(const spi_dev *spi, const iovec *vec, uint32_t vec_len, void *rd_buf, uint32_t rd_len);

#endif /* HAL_HW_SPI_H_ */
