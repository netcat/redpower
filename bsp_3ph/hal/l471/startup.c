#include <stdlib.h>
#include <stdint.h>

extern unsigned int _sidata, _sdata, _edata;
extern unsigned int _sbss, _ebss;
extern unsigned int _estack;

#define weak __attribute__ ((weak))

// core
void weak Reset_Handler(void);
void weak NMI_Handler(void);
void weak HardFault_Handler(void);
void weak MemManage_Handler(void);
void weak BusFault_Handler(void);
void weak UsageFault_Handler(void);
void weak SVC_Handler(void);
void weak DebugMon_Handler(void);
void weak PendSV_Handler(void);
void weak SysTick_Handler(void);
// external
void weak WWDG_IRQHandler(void);
void weak PVD_PVM_IRQHandler(void);
void weak TAMP_STAMP_IRQHandler(void);
void weak RTC_WKUP_IRQHandler(void);
void weak FLASH_IRQHandler(void);
void weak RCC_IRQHandler(void);
void weak EXTI0_IRQHandler(void);
void weak EXTI1_IRQHandler(void);
void weak EXTI2_IRQHandler(void);
void weak EXTI3_IRQHandler(void);
void weak EXTI4_IRQHandler(void);
void weak DMA1_Channel1_IRQHandler(void);
void weak DMA1_Channel2_IRQHandler(void);
void weak DMA1_Channel3_IRQHandler(void);
void weak DMA1_Channel4_IRQHandler(void);
void weak DMA1_Channel5_IRQHandler(void);
void weak DMA1_Channel6_IRQHandler(void);
void weak DMA1_Channel7_IRQHandler(void);
void weak ADC1_2_IRQHandler(void);
void weak CAN1_TX_IRQHandler(void);
void weak CAN1_RX0_IRQHandler(void);
void weak CAN1_RX1_IRQHandler(void);
void weak CAN1_SCE_IRQHandler(void);
void weak EXTI9_5_IRQHandler(void);
void weak TIM1_BRK_TIM15_IRQHandler(void);
void weak TIM1_UP_TIM16_IRQHandler(void);
void weak TIM1_TRG_COM_TIM17_IRQHandler(void);
void weak TIM1_CC_IRQHandler(void);
void weak TIM2_IRQHandler(void);
void weak TIM3_IRQHandler(void);
void weak TIM4_IRQHandler(void);
void weak I2C1_EV_IRQHandler(void);
void weak I2C1_ER_IRQHandler(void);
void weak I2C2_EV_IRQHandler(void);
void weak I2C2_ER_IRQHandler(void);
void weak SPI1_IRQHandler(void);
void weak SPI2_IRQHandler(void);
void weak USART1_IRQHandler(void);
void weak USART2_IRQHandler(void);
void weak USART3_IRQHandler(void);
void weak EXTI15_10_IRQHandler(void);
void weak RTC_Alarm_IRQHandler(void);
void weak DFSDM1_FLT3_IRQHandler(void);
void weak TIM8_BRK_IRQHandler(void);
void weak TIM8_UP_IRQHandler(void);
void weak TIM8_TRG_COM_IRQHandler(void);
void weak TIM8_CC_IRQHandler(void);
void weak ADC3_IRQHandler(void);
void weak FMC_IRQHandler(void);
void weak SDMMC1_IRQHandler(void);
void weak TIM5_IRQHandler(void);
void weak SPI3_IRQHandler(void);
void weak UART4_IRQHandler(void);
void weak UART5_IRQHandler(void);
void weak TIM6_DAC_IRQHandler(void);
void weak TIM7_IRQHandler(void);
void weak DMA2_Channel1_IRQHandler(void);
void weak DMA2_Channel2_IRQHandler(void);
void weak DMA2_Channel3_IRQHandler(void);
void weak DMA2_Channel4_IRQHandler(void);
void weak DMA2_Channel5_IRQHandler(void);
void weak DFSDM1_FLT0_IRQHandler(void);
void weak DFSDM1_FLT1_IRQHandler(void);
void weak DFSDM1_FLT2_IRQHandler(void);
void weak COMP_IRQHandler(void);
void weak LPTIM1_IRQHandler(void);
void weak LPTIM2_IRQHandler(void);
void weak DMA2_Channel6_IRQHandler(void);
void weak DMA2_Channel7_IRQHandler(void);
void weak LPUART1_IRQHandler(void);
void weak QUADSPI_IRQHandler(void);
void weak I2C3_EV_IRQHandler(void);
void weak I2C3_ER_IRQHandler(void);
void weak SAI1_IRQHandler(void);
void weak SAI2_IRQHandler(void);
void weak SWPMI1_IRQHandler(void);
void weak TSC_IRQHandler(void);
void weak RNG_IRQHandler(void);
void weak FPU_IRQHandler(void);

int main(void);

void Default_Reset_Handler()
{
	unsigned int *src = &_sidata;
	unsigned int *dst = &_sdata;
	while(dst < &_edata) *dst++ = *src++;

	dst = &_sbss;
	while(dst < &_ebss) *dst++ = 0;
	
	main();
}

void Default_Handler()
{
  while(1);
}

__attribute__((section(".isr_vector")))
void (* const vector_table[])(void) = {
  // core
  (void (*)(void))&_estack,
  Reset_Handler,
  NMI_Handler,
  HardFault_Handler,
  MemManage_Handler,
  BusFault_Handler,
  UsageFault_Handler,
  0,
  0,
  0,
  0,
  SVC_Handler,
  DebugMon_Handler,
  0,
  PendSV_Handler,
  SysTick_Handler,
  // external
  WWDG_IRQHandler,
  PVD_PVM_IRQHandler,
  TAMP_STAMP_IRQHandler,
  RTC_WKUP_IRQHandler,
  FLASH_IRQHandler,
  RCC_IRQHandler,
  EXTI0_IRQHandler,
  EXTI1_IRQHandler,
  EXTI2_IRQHandler,
  EXTI3_IRQHandler,
  EXTI4_IRQHandler,
  DMA1_Channel1_IRQHandler,
  DMA1_Channel2_IRQHandler,
  DMA1_Channel3_IRQHandler,
  DMA1_Channel4_IRQHandler,
  DMA1_Channel5_IRQHandler,
  DMA1_Channel6_IRQHandler,
  DMA1_Channel7_IRQHandler,
  ADC1_2_IRQHandler,
  CAN1_TX_IRQHandler,
  CAN1_RX0_IRQHandler,
  CAN1_RX1_IRQHandler,
  CAN1_SCE_IRQHandler,
  EXTI9_5_IRQHandler,
  TIM1_BRK_TIM15_IRQHandler,
  TIM1_UP_TIM16_IRQHandler,
  TIM1_TRG_COM_TIM17_IRQHandler,
  TIM1_CC_IRQHandler,
  TIM2_IRQHandler,
  TIM3_IRQHandler,
  TIM4_IRQHandler,
  I2C1_EV_IRQHandler,
  I2C1_ER_IRQHandler,
  I2C2_EV_IRQHandler,
  I2C2_ER_IRQHandler,
  SPI1_IRQHandler,
  SPI2_IRQHandler,
  USART1_IRQHandler,
  USART2_IRQHandler,
  USART3_IRQHandler,
  EXTI15_10_IRQHandler,
  RTC_Alarm_IRQHandler,
  DFSDM1_FLT3_IRQHandler,
  TIM8_BRK_IRQHandler,
  TIM8_UP_IRQHandler,
  TIM8_TRG_COM_IRQHandler,
  TIM8_CC_IRQHandler,
  ADC3_IRQHandler,
  FMC_IRQHandler,
  SDMMC1_IRQHandler,
  TIM5_IRQHandler,
  SPI3_IRQHandler,
  UART4_IRQHandler,
  UART5_IRQHandler,
  TIM6_DAC_IRQHandler,
  TIM7_IRQHandler,
  DMA2_Channel1_IRQHandler,
  DMA2_Channel2_IRQHandler,
  DMA2_Channel3_IRQHandler,
  DMA2_Channel4_IRQHandler,
  DMA2_Channel5_IRQHandler,
  DFSDM1_FLT0_IRQHandler,
  DFSDM1_FLT1_IRQHandler,
  DFSDM1_FLT2_IRQHandler,
  COMP_IRQHandler,
  LPTIM1_IRQHandler,
  LPTIM2_IRQHandler,
  0,
  DMA2_Channel6_IRQHandler,
  DMA2_Channel7_IRQHandler,
  LPUART1_IRQHandler,
  QUADSPI_IRQHandler,
  I2C3_EV_IRQHandler,
  I2C3_ER_IRQHandler,
  SAI1_IRQHandler,
  SAI2_IRQHandler,
  SWPMI1_IRQHandler,
  TSC_IRQHandler,
  0,
  0,
  RNG_IRQHandler,
  FPU_IRQHandler,
};

// core
#pragma weak Reset_Handler = Default_Reset_Handler
#pragma weak NMI_Handler = Default_Handler
#pragma weak HardFault_Handler = Default_Handler
#pragma weak MemManage_Handler = Default_Handler
#pragma weak BusFault_Handler = Default_Handler
#pragma weak UsageFault_Handler = Default_Handler
#pragma weak SVC_Handler = Default_Handler
#pragma weak DebugMon_Handler = Default_Handler
#pragma weak PendSV_Handler = Default_Handler
#pragma weak SysTick_Handler = Default_Handler
// external
#pragma weak WWDG_IRQHandler = Default_Handler
#pragma weak PVD_PVM_IRQHandler = Default_Handler
#pragma weak TAMP_STAMP_IRQHandler = Default_Handler
#pragma weak RTC_WKUP_IRQHandler = Default_Handler
#pragma weak FLASH_IRQHandler = Default_Handler
#pragma weak RCC_IRQHandler = Default_Handler
#pragma weak EXTI0_IRQHandler = Default_Handler
#pragma weak EXTI1_IRQHandler = Default_Handler
#pragma weak EXTI2_IRQHandler = Default_Handler
#pragma weak EXTI3_IRQHandler = Default_Handler
#pragma weak EXTI4_IRQHandler = Default_Handler
#pragma weak DMA1_Channel1_IRQHandler = Default_Handler
#pragma weak DMA1_Channel2_IRQHandler = Default_Handler
#pragma weak DMA1_Channel3_IRQHandler = Default_Handler
#pragma weak DMA1_Channel4_IRQHandler = Default_Handler
#pragma weak DMA1_Channel5_IRQHandler = Default_Handler
#pragma weak DMA1_Channel6_IRQHandler = Default_Handler
#pragma weak DMA1_Channel7_IRQHandler = Default_Handler
#pragma weak ADC1_2_IRQHandler = Default_Handler
#pragma weak CAN1_TX_IRQHandler = Default_Handler
#pragma weak CAN1_RX0_IRQHandler = Default_Handler
#pragma weak CAN1_RX1_IRQHandler = Default_Handler
#pragma weak CAN1_SCE_IRQHandler = Default_Handler
#pragma weak EXTI9_5_IRQHandler = Default_Handler
#pragma weak TIM1_BRK_TIM15_IRQHandler = Default_Handler
#pragma weak TIM1_UP_TIM16_IRQHandler = Default_Handler
#pragma weak TIM1_TRG_COM_TIM17_IRQHandler = Default_Handler
#pragma weak TIM1_CC_IRQHandler = Default_Handler
#pragma weak TIM2_IRQHandler = Default_Handler
#pragma weak TIM3_IRQHandler = Default_Handler
#pragma weak TIM4_IRQHandler = Default_Handler
#pragma weak I2C1_EV_IRQHandler = Default_Handler
#pragma weak I2C1_ER_IRQHandler = Default_Handler
#pragma weak I2C2_EV_IRQHandler = Default_Handler
#pragma weak I2C2_ER_IRQHandler = Default_Handler
#pragma weak SPI1_IRQHandler = Default_Handler
#pragma weak SPI2_IRQHandler = Default_Handler
#pragma weak USART1_IRQHandler = Default_Handler
#pragma weak USART2_IRQHandler = Default_Handler
#pragma weak USART3_IRQHandler = Default_Handler
#pragma weak EXTI15_10_IRQHandler = Default_Handler
#pragma weak RTC_Alarm_IRQHandler = Default_Handler
#pragma weak DFSDM1_FLT3_IRQHandler = Default_Handler
#pragma weak TIM8_BRK_IRQHandler = Default_Handler
#pragma weak TIM8_UP_IRQHandler = Default_Handler
#pragma weak TIM8_TRG_COM_IRQHandler = Default_Handler
#pragma weak TIM8_CC_IRQHandler = Default_Handler
#pragma weak ADC3_IRQHandler = Default_Handler
#pragma weak FMC_IRQHandler = Default_Handler
#pragma weak SDMMC1_IRQHandler = Default_Handler
#pragma weak TIM5_IRQHandler = Default_Handler
#pragma weak SPI3_IRQHandler = Default_Handler
#pragma weak UART4_IRQHandler = Default_Handler
#pragma weak UART5_IRQHandler = Default_Handler
#pragma weak TIM6_DAC_IRQHandler = Default_Handler
#pragma weak TIM7_IRQHandler = Default_Handler
#pragma weak DMA2_Channel1_IRQHandler = Default_Handler
#pragma weak DMA2_Channel2_IRQHandler = Default_Handler
#pragma weak DMA2_Channel3_IRQHandler = Default_Handler
#pragma weak DMA2_Channel4_IRQHandler = Default_Handler
#pragma weak DMA2_Channel5_IRQHandler = Default_Handler
#pragma weak DFSDM1_FLT0_IRQHandler = Default_Handler
#pragma weak DFSDM1_FLT1_IRQHandler = Default_Handler
#pragma weak DFSDM1_FLT2_IRQHandler = Default_Handler
#pragma weak COMP_IRQHandler = Default_Handler
#pragma weak LPTIM1_IRQHandler = Default_Handler
#pragma weak LPTIM2_IRQHandler = Default_Handler
#pragma weak DMA2_Channel6_IRQHandler = Default_Handler
#pragma weak DMA2_Channel7_IRQHandler = Default_Handler
#pragma weak LPUART1_IRQHandler = Default_Handler
#pragma weak QUADSPI_IRQHandler = Default_Handler
#pragma weak I2C3_EV_IRQHandler = Default_Handler
#pragma weak I2C3_ER_IRQHandler = Default_Handler
#pragma weak SAI1_IRQHandler = Default_Handler
#pragma weak SAI2_IRQHandler = Default_Handler
#pragma weak SWPMI1_IRQHandler = Default_Handler
#pragma weak TSC_IRQHandler = Default_Handler
#pragma weak RNG_IRQHandler = Default_Handler
#pragma weak FPU_IRQHandler = Default_Handler
