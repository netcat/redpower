/*
 * hw_rcc.h
 *
 *  Created on: Feb 7, 2017
 *      Author: netcat
 */

#ifndef HW_RCC_H
#define HW_RCC_H

#include "hw_defs.h"

#define HSE_HZ MHZ_TO_HZ(10)
#define RCC_HCLK_HZ MHZ_TO_HZ(20)
#define RCC_APB1_CLK_HZ MHZ_TO_HZ(20)
#define RCC_APB2_CLK_HZ MHZ_TO_HZ(20)
#define RCC_TMRS_PRE_MUL 1

enum
{
  PLL_M = 1,
  PLL_N = 16,
  PLL_R = 2,
  PLL_Q = 2,
  PLL_P = 7
};

#define PLL_Q_FREQ (((HSE_HZ / PLL_M) * PLL_N) / PLL_Q)

void rcc_setup_lse(void);
void rcc_setup_clk(void);

#endif
