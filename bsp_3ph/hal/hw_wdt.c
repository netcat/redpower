/*
 * hw_wdt.c
 *
 *  Created on: Aug 26, 2019
 *      Author: vchumakov
 */


#include "hw_wdt.h"

#define WDT_CFG_ACCESS 0x5555
#define WDT_CFG_RELOAD 0xAAAA
#define WDT_CFG_ENABLE 0xCCCC

void wdt_reset()
{
  IWDG->KR = WDT_CFG_ACCESS;
  IWDG->PR = IWDG_PR_PR_0;
  IWDG->RLR = 1;
  IWDG->KR = WDT_CFG_RELOAD;
  IWDG->KR = WDT_CFG_ENABLE;
}
