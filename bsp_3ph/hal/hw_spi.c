/*
 * hw_spi.c
 *
 *  Created on: Apr 6, 2019
 *      Author: netcat
 */

#include "hal/hw_gpio.h"
#include "hal/hw_spi.h"
#include "hal/hw_rcc.h"

void spi_cfg(SPI_TypeDef *SPIx)
{
  SPIx->CR1 =
    SPI_CR1_BR |   // baudrate = Fpclk / 256
    SPI_CR1_SSM |  // sw slave mgmt
    SPI_CR1_SSI |  // int slave select
    SPI_CR1_MSTR | // master mode
    SPI_CR1_SPE;   // en
}

void spi_io(const spi_dev *spi, const iovec *vec, uint32_t vec_len, void *rd_buf, uint32_t rd_len)
{
  uint8_t *rdb = rd_buf;
  SPI_TypeDef *SPIx = spi->SPIx;
  uint32_t flags = spi->flags;

  spi->select(1);

  uint32_t i, j;
  for(i = 0; i < vec_len; i++)
    for(j = 0; j < vec[i].len; j++)
    {
      while(!(SPIx->SR & SPI_SR_TXE));
      SPIx->DR = ((uint8_t*)vec[i].data)[j];

      while(!(spi->SPIx->SR & SPI_SR_RXNE));
      uint32_t t = SPIx->DR;

      if(rdb && rd_len && (flags & SPI_FLAG_OVERLAP))
      {
         *rdb++ = t;
         rd_len--;
      }
    }

  uint32_t fill = (flags & SPI_FLAG_FILL_FF) ? 0xff : 0;

  while(rd_len--)
  {
    while(!(SPIx->SR & SPI_SR_TXE));
    SPIx->DR = fill;

    while(!(SPIx->SR & SPI_SR_RXNE));
    uint32_t t = SPIx->DR;
    if(rdb) *rdb++ = t;
  }

  spi->select(0);
}

