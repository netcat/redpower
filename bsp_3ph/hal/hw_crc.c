/*
 * hw_crc.c
 *
 *  Created on: Jul 19, 2019
 *      Author: netcat
 */

#include "hw_defs.h"
#include "hal/hw_crc.h"

void crc_init(uint32_t poly, uint32_t init_val)
{
  RCC->AHB1ENR |= RCC_AHB1ENR_CRCEN;

  CRC->POL = poly;
  CRC->CR = CRC_CR_RESET
      | CRC_CR_POLYSIZE_0 // 16 bit poly
      | CRC_CR_REV_IN_0
      | CRC_CR_REV_OUT;

  CRC->INIT = init_val;
  CRC->IDR = 0;
  CRC->DR = 0xffffffff;
}

uint32_t crc_calc16(const iovec *vec, uint32_t vec_len)
{
  CRC->CR |= CRC_CR_RESET;

  uint32_t i, j;
  for(i = 0; i < vec_len; i++)
    for(j = 0; j < vec[i].len; j++)
    {
      *(volatile uint8_t *)&CRC->DR = ((uint8_t *)vec[i].data)[j];
    }
  return CRC->DR;
}
