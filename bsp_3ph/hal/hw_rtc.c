/*
 * rtc.c
 *
 *  Created on: Jul 23, 2019
 *      Author: netcat
 */

#include "hw_rtc.h"

static void rtc_wp(RTC_TypeDef *RTCx, uint32_t wp)
{
  if(wp)
  {
    // init mode dis
    RTCx->ISR &= ~RTC_ISR_INIT;
    // WP en
    RTCx->WPR = RTC_WP_EN;
  }
  else
  {
    // WP dis
    RTCx->WPR = RTC_WP_DIS_KEY1;
    RTCx->WPR = RTC_WP_DIS_KEY2;

    // init mode en
    RTCx->ISR |= RTC_ISR_INIT;
    while(!(RTC->ISR & RTC_ISR_INITF));
  }
}

void rtc_init(RTC_TypeDef *RTCx)
{
  // RTC en
  RCC->BDCR |= RCC_BDCR_RTCEN;

  rtc_wp(RTCx, 0);

  // 24h, no output
  RTCx->CR = 0;
  RTCx->PRER = RTC_SYNCH_PS_DEFAULT | (RTC_ASYNCH_PS_DEFAULT << 16);

  // alarm IE every 1 sec
  RTCx->ALRMAR |= RTC_ALRMAR_MSK1 | RTC_ALRMAR_MSK2 | RTC_ALRMAR_MSK3 | RTC_ALRMAR_MSK4;
  RTCx->ALRMAR |= RTC_ALRMAR_SU_0;
  RTCx->CR |= RTC_CR_ALRAE;
  RTCx->CR |= RTC_CR_ALRAIE;

  // config EXTI for using RTC alarm
  EXTI->PR1 |= EXTI_PR1_PIF18; // clear trigger request
  EXTI->EMR1 &= ~EXTI_EMR1_EM18; // clear event mask

  EXTI->IMR1 |= EXTI_IMR1_IM18; // event mask
  EXTI->RTSR1 |= EXTI_RTSR1_RT18; // rising trigger

  rtc_wp(RTCx, 1);

  NVIC_EnableIRQ(RTC_Alarm_IRQn);
}

void rtc_set_dt(const rtc_dt_t *dt)
{
  uint32_t time_bcd = 0;
  time_bcd |= BIN_TO_BCD(dt->time.hour) << RTC_TR_HU_Pos;
  time_bcd |= BIN_TO_BCD(dt->time.min) << RTC_TR_MNU_Pos;
  time_bcd |= BIN_TO_BCD(dt->time.sec) << RTC_TR_SU_Pos;
  time_bcd &= RTC_TR_MASK;

  uint32_t date_bcd = 0;
  date_bcd |= BIN_TO_BCD(dt->date.year) << RTC_DR_YU_Pos;
  date_bcd |= dt->date.wd << RTC_DR_WDU_Pos;
  date_bcd |= BIN_TO_BCD(dt->date.month) << RTC_DR_MU_Pos;
  date_bcd |= BIN_TO_BCD(dt->date.day) << RTC_DR_DU_Pos;
  date_bcd &= RTC_DR_MASK;

  rtc_wp(RTC, 0);
  RTC->TR = time_bcd;
  RTC->DR = date_bcd;
  rtc_wp(RTC, 1);
}

void rtc_raw2date(const rtc_raw_t *raw, rtc_date_t *date)
{
  date->year = BCD_TO_BIN(RTC_RAW_YEAR(raw->date));
  date->wd = RTC_RAW_WD(raw->date);
  date->month = BCD_TO_BIN(RTC_RAW_MONTH(raw->date));
  date->day = BCD_TO_BIN(RTC_RAW_DAY(raw->date));
}

void rtc_raw2time(const rtc_raw_t *raw, rtc_time_t *time)
{
  time->hour = BCD_TO_BIN(RTC_RAW_HOUR(raw->time));
  time->min = BCD_TO_BIN(RTC_RAW_MIN(raw->time));
  time->sec = BCD_TO_BIN(RTC_RAW_SEC(raw->time));
}

void rtc_get_raw(rtc_raw_t *raw)
{
  raw->time = RTC->TR & RTC_TR_MASK;
  raw->date = RTC->DR & RTC_DR_MASK;
}

void rtc_get_dt(rtc_dt_t *dt)
{
  rtc_raw_t rtc_raw;
  rtc_get_raw(&rtc_raw);
  rtc_raw2date(&rtc_raw, &dt->date);
  rtc_raw2time(&rtc_raw, &dt->time);
}

// ret true on (st <= dt <= end)
uint32_t rtc_dt_between(const rtc_interval_t *internal, const rtc_dt_t *dt)
{
  return rtc_dt_cmp(&internal->st, dt) && rtc_dt_cmp(dt, &internal->end);
}

// ret: true on (dt1 <= dt2)
uint32_t rtc_dt_cmp(const rtc_dt_t *dt1, const rtc_dt_t *dt2)
{
  return (dt1->date.year < dt2->date.year) ||
    ((dt1->date.year == dt2->date.year) && (dt1->date.month < dt2->date.month)) ||
    ((dt1->date.month == dt2->date.month) && (dt1->date.day < dt2->date.day)) ||
    ((dt1->date.day == dt2->date.day) && (dt1->time.hour < dt2->time.hour)) ||
    ((dt1->time.hour == dt2->time.hour) && (dt1->time.min < dt2->time.min)) ||
    ((dt1->time.min == dt2->time.min) && (dt1->time.sec <= dt2->time.sec));
}

