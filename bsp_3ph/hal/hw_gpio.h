/*
 * stm_gpio.h
 *
 *  Created on: Feb 7, 2017
 *      Author: netcat
 */

#ifndef HW_GPIO_H
#define HW_GPIO_H

#include "hw_defs.h"

#define GPIO_SET(gpio, pin) (gpio)->BSRR = (1 << (pin))
#define GPIO_RESET(gpio, pin) (gpio)->BSRR = ((1 << (pin)) << 16)
#define GPIO_TOGGLE(gpio, pin) (gpio)->ODR ^= (1 << (pin))
#define GPIO_GET(gpio) ((gpio)->IDR)
#define GPIO_GET_PIN(gpio, pin) ((gpio)->IDR & (1 << pin))

typedef enum
{
  GPIO_MODE_IN = 0x00,
  GPIO_MODE_OUT = 0x01,
  GPIO_MODE_AF = 0x02,
  GPIO_MODE_AN = 0x03
} gpio_mode_t;

typedef enum
{
  GPIO_OTYPE_PP = 0x00,
  GPIO_OTYPE_OD = 0x01
} gpio_otype_t;

typedef enum
{
  GPIO_PUPD_NOPULL = 0x00,
  GPIO_PUPD_UP = 0x01,
  GPIO_PUPD_DOWN = 0x02
} gpio_pupd_t;

typedef enum
{
  GPIO_SPEED_2MHZ = 0x00,
  GPIO_SPEED_25MHZ = 0x01,
  GPIO_SPEED_50MHZ = 0x02,
  GPIO_SPEED_100MHZ = 0x03
} gpio_speed_t;

typedef enum
{
  GPIO_AF0_DEFAULT       = 0,
  GPIO_AF0_RTC_50Hz      = 0,
  GPIO_AF0_MCO           = 0,
  GPIO_AF0_SWJ           = 0,

  GPIO_AF1_TIM1          = 1,
  GPIO_AF1_TIM2          = 1,
  GPIO_AF1_TIM5          = 1,
  GPIO_AF1_TIM8          = 1,
  GPIO_AF1_LPTIM1        = 1,
  GPIO_AF1_IR            = 1,

  GPIO_AF2_TIM1          = 2,
  GPIO_AF2_TIM2          = 2,
  GPIO_AF2_TIM3          = 2,
  GPIO_AF2_TIM4          = 2,
  GPIO_AF2_TIM5          = 2,

  GPIO_AF3_TIM8          = 3,
  GPIO_AF3_TIM1_COMP2    = 3,
  GPIO_AF3_TIM1_COMP1    = 3,

  GPIO_AF4_I2C1          = 4,
  GPIO_AF4_I2C2          = 4,
  GPIO_AF4_I2C3          = 4,

  GPIO_AF5_SPI1          = 5,
  GPIO_AF5_SPI2          = 5,

  GPIO_AF6_SPI3          = 6,
  GPIO_AF6_DFSDM1        = 6,

  GPIO_AF7_USART1        = 7,
  GPIO_AF7_USART2        = 7,
  GPIO_AF7_USART3        = 7,

  GPIO_AF8_UART4         = 8,
  GPIO_AF8_UART5         = 8,
  GPIO_AF8_LPUART1       = 8,

  GPIO_AF9_CAN1          = 9,
  GPIO_AF9_TSC           = 9,

  GPIO_AF10_QUADSPI      = 10,

  GPIO_AF12_FMC          = 12,
  GPIO_AF12_SWPMI1       = 12,
  GPIO_AF12_COMP1        = 12,
  GPIO_AF12_COMP2        = 12,
  GPIO_AF12_SDMMC1       = 12,

  GPIO_AF13_SAI1         = 13,
  GPIO_AF13_SAI2         = 13,
  GPIO_AF13_TIM8_COMP2   = 13,
  GPIO_AF13_TIM8_COMP1   = 13,

  GPIO_AF14_TIM2         = 14,
  GPIO_AF14_TIM15        = 14,
  GPIO_AF14_TIM16        = 14,
  GPIO_AF14_TIM17        = 14,
  GPIO_AF14_LPTIM2       = 14,
  GPIO_AF14_TIM8_COMP1   = 14,

  GPIO_AF15_EVENTOUT     = 15,
} gpio_af_t;

typedef struct
{
  GPIO_TypeDef *GPIOx;
  uint32_t pin;
} gpio_init_t;

void gpio_init_ext(GPIO_TypeDef *GPIOx, uint32_t pin, gpio_mode_t mode, gpio_otype_t otype, gpio_speed_t speed, gpio_pupd_t pupd);
void gpio_init_af(const gpio_init_t *array, gpio_af_t af);
void gpio_deinit_af(const gpio_init_t *array);
void gpio_init(GPIO_TypeDef *GPIOx, uint32_t pin, gpio_mode_t mode);
void gpio_set_af(GPIO_TypeDef *GPIOx, uint32_t pin, gpio_af_t af);
void gpio_init_af_ext(const gpio_init_t *array, gpio_af_t af, gpio_otype_t otype, gpio_pupd_t pupd);

#endif
