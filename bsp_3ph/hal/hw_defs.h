/*
 * hw_uart.h
 *
 *  Created on: Apr 8, 2017
 *      Author: netcat
 */

#ifndef HW_DEFS_H_
#define HW_DEFS_H_

#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "stm32l471xx.h"

#define SIZE_OF_ARRAY(a) (sizeof(a) / sizeof(a[0]))

#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#define MID(a, b, c) MAX(a, MIN(b, c))
#define ABS(x) (((x) > 0) ? (x) : (-x))

#define MOD2N(m, n) ((m) & ((n) - 1))
#define IS2N(n) (!((n) & ((n) - 1)))
#define DIV_INT_ROUND(DEVIDEN, DEVIDER) (((DEVIDEN) + ((DEVIDER) >> 1)) / (DEVIDER))

#define M_RAD_TO_GRAD(rad) ((rad) * 180.0 / M_PI)
#define M_GRAD_TO_RAD(grad) ((grad) * M_PI / 180.0)

#define BCD_TO_BIN(bcd) (((bcd) & 0x0f) + ((bcd) >> 4) * 10)
#define BIN_TO_BCD(dec) ((((dec) / 10) << 4) + ((dec) % 10))

#define HZ_TO_MHZ(FREQ) ((FREQ) / 1000000)
#define MHZ_TO_HZ(FREQ) ((FREQ) * 1000000)

#define EVT_TOUT(x) \
  tout = 0xffff; \
  while(x) { if(!tout--) goto terr; }

typedef struct
{
  void *data; // pointer to data
  uint32_t len; // length of data
} iovec;

typedef struct io_if_struct
{
  void *reg;
  void (*select)(uint32_t);
  uint32_t (*transaction)(struct io_if_struct *io_if, const iovec *vec, uint32_t vec_len, void *rd_buf, uint32_t rd_len);
  uint32_t flags;
} io_if_t;

#endif /* HW_DEFS_H_ */
