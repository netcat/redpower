/*
 * hw_rcc.c
 *
 *  Created on: Feb 7, 2017
 *      Author: netcat
 */

#include <stdlib.h>
#include <stdint.h>
#include "hal/hw_rcc.h"

#include "hal/hw_gpio.h"

void rcc_setup_lse()
{
  // LSE en
  RCC->BDCR |= RCC_BDCR_LSEON;
  while(!(RCC->BDCR & RCC_BDCR_LSERDY));

  // use LSE
  RCC->BDCR &= ~RCC_BDCR_RTCSEL;
  RCC->BDCR |= RCC_BDCR_RTCSEL_0;
}

void rcc_setup_clk()
{
  // flash latency
  FLASH->ACR &= ~FLASH_ACR_LATENCY;
  FLASH->ACR |= FLASH_ACR_LATENCY_4WS;

  RCC->APB1ENR1 |= RCC_APB1ENR1_PWREN;
  // reg voltage scaling
  PWR->CR1 &= ~PWR_CR1_VOS;
  PWR->CR1 |= PWR_CR1_VOS_0;
  // enable access to backup domain
  PWR->CR1 |= PWR_CR1_DBP;

  // HSE enable
  RCC->CR |= RCC_CR_HSEON;
  while(!(RCC->CR & RCC_CR_HSERDY));

  // config pll domain
  RCC->PLLCFGR &= ~(RCC_PLLCFGR_PLLSRC | RCC_PLLCFGR_PLLM | RCC_PLLCFGR_PLLN | RCC_PLLCFGR_PLLR);
  RCC->PLLCFGR |= RCC_PLLCFGR_PLLSRC_HSE
      | ((PLL_M - 1) << RCC_PLLCFGR_PLLM_Pos)
      | (PLL_N << RCC_PLLCFGR_PLLN_Pos)
      | (((PLL_R >> 1) - 1) << RCC_PLLCFGR_PLLR_Pos);

  // en pll
  RCC->PLLCFGR |= RCC_PLLCFGR_PLLREN;
  RCC->CR |= RCC_CR_PLLON;
  while(!(RCC->CR & RCC_CR_PLLRDY));

  // set clr src, conf PS
  RCC->CFGR &= ~RCC_CFGR_SW;
  RCC->CFGR |= RCC_CFGR_SW_PLL | RCC_CFGR_HPRE_DIV4 | RCC_CFGR_PPRE1_DIV1 | RCC_CFGR_PPRE2_DIV1;
  while((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL);

  // SysTick: en, HCLK, 1ms
  SysTick->LOAD = (RCC_HCLK_HZ / 1000 - 1);
  SysTick->VAL = 0;
  SysTick->CTRL = SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_ENABLE_Msk;

  RCC->CCIPR &= ~(RCC_CCIPR_UART5SEL | RCC_CCIPR_UART4SEL);
  RCC->CCIPR &= ~(RCC_CCIPR_USART1SEL | RCC_CCIPR_USART2SEL | RCC_CCIPR_USART3SEL);
}

