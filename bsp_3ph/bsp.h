/*
 * bsp.h
 *
 *  Created on: Apr 12, 2019
 *      Author: netcat
 */

#ifndef BSP_H_
#define BSP_H_

#include "hal/hw_defs.h"
#include "sys/pkg.h"

// GPIO
// LED_ACT
#define LED_ACT_PORT GPIOD
#define LED_ACT_PIN 3

// LED_REACT
#define LED_REACT_PORT GPIOD
#define LED_REACT_PIN 1

// LED_BL
#define LED_BL_PORT GPIOE
#define LED_BL_PIN 1

// RXTX_485
#define RXTX_485_PORT GPIOD
#define RXTX_485_PIN 0

// U_GATE_OPTO_L1
#define U_GATE_OPTO_L1_PORT GPIOC
#define U_GATE_OPTO_L1_PIN 3

// REL_ON
#define REL_ON_PIN 0
#define REL_ON_PORT GPIOE

// REL_OFF
#define REL_OFF_PIN 9
#define REL_OFF_PORT GPIOB

// TAMPER1
#define TAMPER1_PIN 0
#define TAMPER1_PORT GPIOA

// TAMPER2
#define TAMPER2_PIN 2
#define TAMPER2_PORT GPIOA

// MAG
#define MAG_PIN 10
#define MAG_PORT GPIOD

// RS485
#define HAL_RS485_BUF_SIZE 1024
#define HAL_RS485_BAUDRATE 115200

enum
{
  HAL_PHASE_A_IO_IF,
  HAL_PHASE_C_IO_IF,
  HAL_PHASE_B_IO_IF,
  HAL_RS458_IO_IF,
  HAL_GATEWAY,

  HAL_IO_IF_MAX
};

enum
{
  OIF_RS485,
  OIF_GATEWAY,

  OIF_MAX
};

void bsp_init(void);
void bsp_relay_set(uint32_t st);

io_if_t *hal_io_if(uint32_t index);
void hal_pkg_free(if_msg_t *if_pkg);
if_msg_t *hal_pkg_rx(uint32_t oif_index);
if_msg_t *hal_pkg_if(uint32_t oif_index);

#endif /* BSP_H_ */
