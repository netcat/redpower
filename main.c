#include <stdlib.h>
#include <stdint.h>

#include "bsp.h"
#include "sys/device.h"
#include "pkg.h"

int main()
{
  bsp_init();
  device_init();

  while(1)
  {
    for(int i = OIF_RS485; i < OIF_MAX; i++)
    {
      if_msg_t *rx = hal_pkg_rx(i);
      if(rx)
      {
        pkg_receive(rx);
        hal_pkg_free(rx);
      }
    }

    device_loop();
  }

  return 0;
}
